# Getting Started with Create React App

### Node Js Version = 18.16.0 
### npm Version = 9.5.1 

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.


# Library Dependencies 

## Install all Dependencies 
   `npm install`
#### - Redux and Redux Toolkit 
    npm install react-redux
    npm install @reduxjs/toolkit
#### - React Router Dom 
    npm install react-router-dom
#### - React Bootstrap &  Bootstrap 5
    npm install react-bootstrap bootstrap
#### - FontAwesome
    npm install --save @fortawesome/fontawesome-svg-core
    npm install --save @fortawesome/free-solid-svg-icons
    npm install --save @fortawesome/free-regular-svg-icons
    npm install --save @fortawesome/free-brands-svg-icons
    npm install --save @fortawesome/react-fontawesome
#### - Swipper js
    npm install swiper
#### - React Data Grid
    npm install @mui/x-data-grid
#### - Rechats js
    npm install recharts

 <!-- <div className={"container mt-3"}>
        {/* <Breadcrumb title={"Dasborad "} subtitle={"Profile"}/> */}
        <div className="row mt-2 d-flex justify-content-center ">
          <div className="col-xl-8 ">
            {/*Profile picture card*/}
            <div className="card mb-xl-0 mb-4">
              <div className=" card-body text-center ">
                <div
                 style={{ backgroundImage:" linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%)"}}
                  className={`${styles["user-profile"]} img-account-profile d-flex flex-column align-items-center border bg-white`}
                >
                  <label
                    htmlFor="user-profile"
                    className={`${styles["user-profile-label"]}`}
                  >
                    <img src={userProfile.avatar} alt="" />
                    <div className={`${styles["profile-icon-box"]} bg-primary`}>
                      {/* <Import size="20" color="white" /> */}
                      { <Status
                        size="10"
                        color=""
                      /> }
                    </div>

                  </label>
                </div>
              
              </div>
            </div>
          </div>
          <div className="col-xl-12">
            {/*Account details card*/}
            <div className="card mb-4">
              <div className="card-header">Account Details</div>
              <div className="card-body">
                <form>
                  <fieldset disabled>
                    {/* Form Group (username) */}
                    <div className="row gx-3 mb-3 ">
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputUsername">
                          Username
                        </label>
                        <input
                          className="form-control"
                          id="inputUsername"
                          type="text"
                          // placeholder="Enter your username"
                          defaultValue={userProfile.username}
                          readOnly={true}
                        />
                      </div>
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputFirstName">
                          Full name
                        </label>
                        <input
                          className="form-control"
                          id="inputFirstName"
                          type="text"
                          // placeholder="Enter your first name"
                          value={userProfile.name}
                          readOnly={true}
                        />
                      </div>
                    </div>
                    <div className="row gx-3 mb-3">
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputLastName">
                          Gender :
                        </label>
                        <input
                          className="form-control"
                          id="inputLastName"
                          type="text"
                          // placeholder="Enter your last name"
                          defaultValue={userProfile.gender}
                          readOnly={true}
                        />
                      </div>
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputOrgName">
                          Role name
                        </label>
                        <input
                          className="form-control"
                          id="inputOrgName"
                          type="text"
                          // placeholder="Enter your organization name"
                          defaultValue={`${userProfile.salary}$`}
                          readOnly={true}
                        />
                      </div>
                    </div>
                    <div className="row gx-3 mb-3">
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputLocation">
                          Hire Date
                        </label>
                        <input
                          className="form-control"
                          id="inputLocation"
                          type="text"
                          // placeholder="Enter your location"
                          defaultValue={userProfile.hireDate}
                          readOnly={true}
                        />
                      </div>
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputEmailAddress">
                          Email address
                        </label>
                        <input
                          className="form-control"
                          id="inputEmailAddress"
                          type="email"
                          // placeholder="Enter your email address"
                          defaultValue={userProfile.email}
                          readOnly={true}
                        />
                      </div>
                    </div>

                    <div className="row gx-3 mb-3">
                      <div className="col-md-6">
                        <label className="small mb-1" htmlFor="inputPhone">
                          Phone number
                        </label>
                        <input
                          className="form-control"
                          id="inputPhone"
                          type="tel"
                          placeholder="Enter your phone number"
                          value={userProfile.phone}
                          readOnly={true}
                        />
                      </div>
                    </div>
                    {/* <button
                    className="btn btn-primary"
                    type="button"
                    onClick={editHandler}
                  >
                    Edit Information
                  </button> */}
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div> -->