import "react-toastify/dist/ReactToastify.css";
import { Outlet } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "bootstrap/dist/css/bootstrap.min.css";
import { useSelector } from "react-redux";
import { baselightTheme } from "./Modules/Theme/DefailtColors";
import { ThemeProvider } from "@mui/material";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Suspense, useEffect } from "react";
import useAuth from "./Modules/Auth/core/Action";
import { AuthInit } from "./Modules/Auth/utils/AuthInit";
import SplashScreen from "./Modules/Components/common/SplashScreen";

function App() {
  const { roleId } = useSelector((state) => state.login);
  const { getRoleById } = useAuth();

  const theme = baselightTheme;

  // ================================ ================================
  useEffect(() => {
    getRoleById(roleId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [roleId]);
  // ================================ ================================

  return (
    <ThemeProvider theme={theme}>
      <Suspense fallback={<SplashScreen />}>
        <AuthInit>
          <Outlet />
        </AuthInit>
      </Suspense>
      <ToastContainer
        position="top-center"
        autoClose={3000}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
    </ThemeProvider>
  );
}

export default App;
