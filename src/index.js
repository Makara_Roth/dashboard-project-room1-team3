import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { Provider } from "react-redux";
import store from "./Redux/store";
import { setUpAxios } from "./Modules/Auth/helper/authHelper";
import { AppRoutes } from "./Modules/routes/AppRoutes";

const root = ReactDOM.createRoot(document.getElementById("root"));
setUpAxios();

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <AppRoutes />
        </Provider>
    </React.StrictMode>
);
