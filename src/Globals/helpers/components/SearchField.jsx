import { Stack, TextField } from "@mui/material";

const SearchField = (props) => {
  const {params, title, onChange } = props;

  return (
    <div style={{ width: "11rem" }}>
      <Stack spacing={4}>
        <Stack direction="row" spacing={2}>
          <TextField label={title} color="primary" onChange={onChange} value={params.query}/>
        </Stack>
      </Stack>
    </div>
  );
};

export default SearchField;
