import * as React from "react";
import Skeleton from "@mui/material/Skeleton";
import Stack from "@mui/material/Stack";
import { Col, Row } from "react-bootstrap";
import DashboardCard from "../../../Modules/Components/shared/DashboardCard";
import { Box } from "@mui/system";

const SkeletonUser = () => {
  return (
    <Row>
      <Col md={6}>
        <DashboardCard>
          <Box sx={{ pt: 0.5 }}>
            <Stack spacing={1}>
              <div className="d-flex justify-content-center align-content-center">
                <Skeleton variant="circular" width={100} height={100} />
              </div>
              <Row>
                <Col md={6}>
                  <Skeleton variant="rounded" width={120} height={10} />
                </Col>
              </Row>

              <Row>
                <Col md={6}>
                  <Skeleton
                    variant="rounded"
                    width={220}
                    height={40}
                    className="mb-3"
                  />
                </Col>
                <Col md={6}>
                  <Skeleton variant="rounded" width={220} height={40} />
                </Col>
                <Col md={6}>
                  <Skeleton variant="rounded" width={220} height={40} />
                </Col>
                <Col md={6}>
                  <Skeleton variant="rounded" width={220} height={40} />
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Skeleton variant="rounded" width={120} height={10} />
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Skeleton
                    variant="rounded"
                    width={220}
                    height={40}
                    className="mb-3"
                  />
                </Col>
                <Col md={6}>
                  <Skeleton variant="rounded" width={220} height={40} />
                </Col>
              </Row>
            </Stack>
          </Box>
        </DashboardCard>
      </Col>
    </Row>
  );
};

export default SkeletonUser;
