import React from 'react'
import { Spinner } from 'react-bootstrap'

const MySpinner = () => {
  return (
    <div className="mt-5 text-center">
          <Spinner style={{ width: "3rem", height: "3rem", color: "#4085ad" }}>
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
  )
}

export default MySpinner