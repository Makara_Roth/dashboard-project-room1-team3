import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import React from "react";

const Filter = ({ title, filterlist, handleFilter, params }) => {
  const handleSort = (value) => {
    handleFilter(title, value);
  };

  const handleClear = () => {
    handleSort(""); // Call handleSort with an empty string to clear the selection
  };

  // Calculate the maximum width of the filter list options
  const maxOptionWidth = filterlist.reduce((maxWidth, item) => {
    const itemWidth = item.length * 10; // Adjust the multiplier as needed
    return maxWidth > itemWidth ? maxWidth : itemWidth;
  }, 0);

  return (
    <FormControl
      variant="outlined"
      size="small"
      sx={{
        minWidth: `${title.length * 12}px`, // Set minWidth based on title length
        marginRight: "5px",
      }}
    >
      <InputLabel id="demo-simple-select-label">{title}</InputLabel>
      <Select
        // labelId="demo-simple-select-label"
        // id="demo-simple-select"
        label={title}
        onChange={(e) => handleSort(e.target.value)}
        value={params[title]}
        defaultValue=""
        sx={{ minWidth: `${maxOptionWidth}px` }} // Set minWidth of Select based on maxOptionWidth
      >
        {filterlist.map((item, index) => (
          <MenuItem className={item} key={index} value={item}>
            {item}
          </MenuItem>
        ))}
        {/* Add a MenuItem to clear the selection */}
        <MenuItem onClick={handleClear}>Clear</MenuItem>
      </Select>
    </FormControl>
  );
};

export default Filter;
