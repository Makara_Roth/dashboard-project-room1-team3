import {
  FormControl,
  MenuItem,
  Pagination,
  Select,
} from "@mui/material";
import React from "react";

const GlobalPagination = (props) => {
  const { params, totalPage, page, handleChange, handleFilter } = props;
  
  return (
    <div className="d-flex justify-content-between align-items-center">
      <FormControl sx={{ minWidth: 60 }}>
        <Select
          name="size"
          value={params.size}
          onChange={(e) => {
            const size = e.target.value;
            handleFilter("size", size);
            handleFilter("page", 1);
          }}
        >
          <MenuItem value="12">12</MenuItem>
          <MenuItem value="24">24</MenuItem>
          <MenuItem value="48">48</MenuItem>
        </Select>
      </FormControl>
      <Pagination
        count={totalPage}
        page={page}
        onChange={handleChange}
        shape="rounded"
        variant="outlined"
        color="primary"
      />
    </div>
  );
};

export default GlobalPagination;
