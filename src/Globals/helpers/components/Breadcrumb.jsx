import { Breadcrumbs, Typography } from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";

const Breadcrumb = ({ title, subtitle }) => {
  return (
    <Breadcrumbs aria-label="breadcrumb">
      <Link style={{ textDecoration: "none", color: "black" }} to="/">
        {title}
      </Link>
      <Typography color="text.primary">{subtitle}</Typography>
    </Breadcrumbs>
  );
};

export default Breadcrumb;
