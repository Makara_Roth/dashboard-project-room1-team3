import { Box, Button, Modal, Typography } from "@mui/material";
import React from "react";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  borderRadius: "5px",
};

const DeleteModal = (props) => {
  const { title, open, onClose, confirmDelete } = props;

  return (
    <Modal open={open} onClose={onClose}>
      <Box sx={style}>
        <Typography
          id="modal-modal-title"
          variant="h6"
          component="h2"
          style={{ textAlign: "center" }}
        >
          {title}
        </Typography>
        <div className="mt-4 text-end">
          <Button
            style={{
              backgroundColor: "#4085ad",
              color: "#ffff",
              marginRight: 3,
            }}
            onClick={onClose}
          >
            Cancel
          </Button>
          <Button variant="contained" color="error" onClick={confirmDelete}>
            Delete
          </Button>
        </div>
      </Box>
    </Modal>
  );
};

export default DeleteModal;
