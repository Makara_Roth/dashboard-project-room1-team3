import { combineReducers } from "@reduxjs/toolkit";
import authSlice from "../Modules/Auth/core/Reducer";
import loginSlice from "../Modules/Auth/Login/Core/Reducer";
import userSlice from "../Modules/Views/User/core/Reducer";
import profileSlice from "../Modules/Views/ProfilePages/core/Reducer";
import foodSlide from "../Modules/Views/food/core/Reducer";
import settingsSlice from "../Modules/Views/Setting/core/Reducer";
import listOrderSlice from "../Modules/Views/Order/core/Reducer"
import roleSlice from "../Modules/Views/Roles/core/Reducer";
import tableSlice from "../Modules/Views/Table/core/Reducer";
import foodCategorySlide from "../Modules/Views/foodCategory/core/Reducer";
import dashboardSlice from "../Modules/Views/Dashboard/core/Reducer"
import incomeSlice from "../Modules/Views/Income/core/Reducer"
import foodReportSlice from "../Modules/Views/FoodReport/core/Reducer"

const rootReducer = combineReducers({
  auth: authSlice,
  login: loginSlice,
  settings: settingsSlice,
  user: userSlice,
  profile: profileSlice,
  food: foodSlide,
  combinedOrder: listOrderSlice,
  role: roleSlice,
  table: tableSlice,
  category: foodCategorySlide,
  dashboard : dashboardSlice,
  incomes : incomeSlice,
  foodReports: foodReportSlice,
})
export default rootReducer
