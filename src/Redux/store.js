import { configureStore } from "@reduxjs/toolkit";

import rootReducer from "./rootReducer";
import { getTotal } from "../Modules/Views/Order/core/Reducer";

const store = configureStore({
  reducer: rootReducer
});

store.dispatch(getTotal())

export default store;
