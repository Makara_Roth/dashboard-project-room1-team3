import axios from "axios";

const setAuth = (value) => localStorage.setItem("token", value);

const getAuth = () => localStorage.getItem("token");


const removeAuth = () => localStorage.removeItem("token");

const setRoleId = (value) => localStorage.setItem("roleId", value);
const getRoleId = () => localStorage.getItem("roleId");
const removeRoleId = () => localStorage.removeItem("roleId");


const setUpAxios = () => {
    axios.defaults.baseURL = "http://13.214.207.172:8001"
    // axios.defaults.headers = {
    //     "Content-Type": "application/json",
    //     Authorization: `Bearer ${getAuth()}`
    // }
    axios.interceptors.request.use(function (config) {
        // Do something before request is sent
        const token = getAuth();

        // Prioritize stored token, fallback to basic auth if necessary
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        } else {
            config.headers.Authorization = `Basic ${btoa('g3:123')}`;
        }


        return config;
    }, function (error) {
        // Do something with request error
        return Promise.reject(error);
    });

}

export { setAuth, getAuth, removeAuth, setUpAxios, setRoleId, getRoleId, removeRoleId };
