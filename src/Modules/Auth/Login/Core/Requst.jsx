
import axios from "axios";
// import { getAuth } from "../../Helper/authHelper";

const reqLogin = (username, password) => {
    return axios.post("/api/auth/login", {
        username: username,
        password: password,
    }
    ,{
        headers:{
            Authorization: `Basic ${btoa(`g3:123`)}`
        }
    }
)
}

const reqUserProfileById = (id) => {
    return axios.get(`/api/user/${id}` , {
    })
}
export {reqLogin , reqUserProfileById}
