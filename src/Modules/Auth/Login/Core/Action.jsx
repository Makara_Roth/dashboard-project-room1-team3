
import useAuth from "../../core/Action";
import { useDispatch } from "react-redux";
import {
  setErrorMessage,
  setPassword,
  setUsername,
  setUser,
  setCurrentUser,
} from "./Reducer";
import * as authHelper from "../../helper/authHelper";
import useProfile from "../../../Views/ProfilePages/core/Action";
import { setToken } from "../../core/Reducer";
import { reqUserProfileById , reqLogin } from "./Requst";

const useLogin = () => {
  const dispatch = useDispatch();
  const { ClearData } = useProfile();
  const { saveAuth, loginSuccess } = useAuth();


  const Login = async (username, password) => {
    try {
      const response = await reqLogin(username, password);
      dispatch(setUser(response?.data?.data?.user)); // get use to store in state

      saveAuth(response.data.data.token);
      authHelper.setRoleId(response?.data?.data?.user?.roleId)
      loginSuccess(true);
      SetErrorMessage(""); // Set ErrorMessage To empty string
    } catch (error) {
      // catch error
      if (!error?.response) {
        SetErrorMessage("No Server Response");
      } else if (error.response?.status === 400) {
        SetErrorMessage("Missing Username or Password");
      } else if (error.response?.status === 401) {
        SetErrorMessage("Unauthorized");
      } else {
        SetErrorMessage("Login Failed");
      }
    }
  };

  const getCurrentUserById = async (id) => {
    reqUserProfileById(id).then((res) => {
        setCurrentUser(res.data.user)
    }).catch (err => {
        console.log(err)
        if (err) {
            logout()
        }
    })
}
  const logout = () => {
    // SetAuth(null);
    dispatch(setUser(null)); // get use to store in state
    loginSuccess(false);
    ClearData();
    
    authHelper.removeAuth();
    dispatch(setToken(null));
    authHelper.removeRoleId()
    localStorage.removeItem('allowedRoles');
  };

  const SetUsername = (username) => {
    dispatch(setUsername(username));
  };
  const SetPassword = (password) => {
    dispatch(setPassword(password));
  };
  const SetErrorMessage = (errorMessage) => {
    dispatch(setErrorMessage(errorMessage));
  };
  return { Login, SetUsername, SetPassword, logout, SetErrorMessage  , getCurrentUserById};
};
export default useLogin;
