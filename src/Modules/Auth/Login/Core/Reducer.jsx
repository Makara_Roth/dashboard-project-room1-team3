import { createSlice } from "@reduxjs/toolkit";
import * as authHelper from "../../helper/authHelper";


const loginSlice = createSlice({
  name: "Login",
  initialState: {
    username: "",
    password: "",
    user: [],
    roleId: authHelper.getRoleId(),
    errorMessage: "",
    currentUser:{}
  },
  reducers: {
    setUser:(state,action)=>{
      state.user = action.payload;
    },
    setUsername: (state, action) => {
      state.username = action.payload;
    },
    setPassword: (state, action) => {
      state.password = action.payload;
    },
    setErrorMessage: (state, action) => {
      state.errorMessage = action.payload;
    },
    setRoleId: (state, action) => {
      state.roleId = action.payload;
    },
    setCurrentUser: (state, action) => {
      state.currentUser = action.payload;
    }
  },
});
 
export const {
  setUsername, setPassword,
  setErrorMessage, setRoleId,
  setUser,setCurrentUser
} =
  loginSlice.actions;

export default loginSlice.reducer;
