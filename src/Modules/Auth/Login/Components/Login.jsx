import { Button } from "react-bootstrap";
import logo from "../../../../assets/images/kiloit-logo.svg";
import Form from "react-bootstrap/Form";
import { useSelector } from "react-redux";
import useLogin from "../Core/Action";
import { useEffect, useState } from "react";
import useAuth from "../../core/Action";
import { Link } from "react-router-dom";
import { Icon } from "react-icons-kit";
import { eyeOff } from "react-icons-kit/feather/eyeOff";
import { eye } from "react-icons-kit/feather/eye";

const Login = () => {
  const { username, password, errorMessage } = useSelector(
    (state) => state.login
  );
  const { auth } = useSelector((state) => state.auth);
  const { SetUsername, SetPassword, Login, SetErrorMessage } = useLogin();
  const { loginSuccess } = useAuth();

  const [type, setType] = useState("password");
  const [icon, setIcon] = useState(eyeOff);

  useEffect(() => {
    if (auth) {
      loginSuccess(true);
    }
    SetErrorMessage("");
    SetUsername("");
    SetPassword("");

    // eslint-disable-next-line
  }, []);
  const handleLogin = async (e) => {
    e.preventDefault();
    if (!username || !password) {
      SetErrorMessage("Please enter username and password");
    } else {
      await Login(username, password);
    }
  };
  const handleToggle = () => {
    if (type === "password") {
      setIcon(eye);
      setType("text");
    } else {
      setIcon(eyeOff);
      setType("password");
    }
  };
  return (
    <div className="Auth-form-container">
      <form className="Auth-form" onSubmit={handleLogin}>
        <div className="Auth-form-content">
          <div className="img-logo d-flex justify-content-center align-items-center">
            <img src={logo} alt="logo" style={{ height: "120px" }} />
          </div>
          <h3 className="Auth-form-title">Log In</h3>
          <div className="form-group mt-3 flex">
            <label>Username</label>
            <Form.Control
              type="text"
              placeholder="Enter Username"
              onChange={(e) => SetUsername(e.target.value)}
              required
            />
          </div>
          <div className="form-group mt-2 position-relative">
            <label>Password</label>
            <Form.Control
              type={type}
              name="password"
              placeholder="Password"
              value={password}
              onChange={(e) => SetPassword(e.target.value)}
              required
              autoComplete="current-password"
            />
            <span
              className="flex justify-around items-center "
              onClick={handleToggle}
            >
              <Icon
                className="position-absolute  "
                icon={icon}
                size={20}
                style={{ top: "32px", right: "5%" }}
              />
            </span>
          </div>
          <div
            className={
              "fs-6 text-secondary mt-3 me-1 d-flex justify-content-end"
            }
          >
            <Link to={"/forgot_password"} className={"text-decoration-none"} style={{color: "#4085ad"}}>
              Forgot Password
            </Link>
          </div>
          <p className={"alert text-danger m-0 p-3 text-center"}>
            {errorMessage}
          </p>
          <div className="d-grid">
            <Button
              type="submit"
              // className="btn btn-primary"
              style={{ backgroundColor: "#4085ad", color: "#ffff" }}
              // onClick={handleLogin}
            >
              Log In
            </Button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Login;
