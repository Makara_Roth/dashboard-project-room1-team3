import axios from "axios"

const reqfetchUser = async () => {
    return await axios.get("/api/user/profile")
}

const reqfetchRoles = async () => {
    return await axios.get("/api/roles")
}

const reqRoleById = (id) => {
    return axios.get(`/api/roles/${id}`)
}

export { reqRoleById, reqfetchUser, reqfetchRoles } 
