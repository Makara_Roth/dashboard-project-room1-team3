import { useDispatch } from "react-redux";
import * as authHelper from "../helper/authHelper";
import { rememberAuth, setIsAuth, setMessage, setPermission, setRoles, setToken } from "./Reducer";
import { reqRoleById, reqfetchRoles, reqfetchUser } from "./Request";
import { reqFetchUserById } from "../../Views/User/core/Request";

const useAuth = () => {
    const dispatch = useDispatch()
     
    const getRoleById = async (roleId) => {
      try{
       await reqRoleById(roleId).then((res) => {
          dispatch(setRoles(res.data.data))
        })
      }catch(err){
        dispatch(setMessage(err.message))
        console.log(err.message);
      }
    }
    
    const saveAuth = (auth) => {
        dispatch(setToken(auth))
        if (auth) {
            authHelper.setAuth(auth)
        } else {
            authHelper.removeAuth();
        }
    }
  const loginSuccess = (isAuth) => {
    dispatch(setIsAuth(isAuth));
  };

  const getUser = async () => {
    try {
      const userResponse = await reqfetchUser();
      const user = userResponse?.data;
      const token = authHelper.getAuth();
      dispatch(rememberAuth({ user: user, token: token }));
      const roles = await reqfetchRoles();
      const roleNameRes = await reqFetchUserById(user?.data?.id)
      const roleName = roleNameRes.data?.data?.role?.name
      const foundRoles = roles?.data?.data?.find(r => r.name === roleName)
      reqRoleById(foundRoles?.id).then((res) => dispatch(setPermission(res?.data?.data?.permissions)))

    } catch (err) {
      authHelper.removeAuth()
    }
  }
 

  return { saveAuth, loginSuccess , getRoleById, getUser }
  };

export default useAuth;
