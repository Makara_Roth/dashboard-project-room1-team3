import { createSlice } from "@reduxjs/toolkit";
import { getAuth } from "../helper/authHelper";

const initialState = {
  token: getAuth(),
  isAuth: getAuth(),
  roles: {},
  user: {},
  userPermission: [],

  message: "",
};

const authSlice = createSlice({
  name: "Auth",
  initialState: initialState,
  reducers: {
    setToken: (state, action) => {
      state.token = action.payload;
    },

    setIsAuth: (state, action) => {
      state.isAuth = action.payload;
    },
    rememberAuth: (state, action) => {
        state.user = action.payload.user.data;
        state.isAuth = action.payload;
    },
    setPermission: (state, action) => {
      state.userPermission = action.payload;
    },
    setRoles: (state, action) => {
      state.roles = action.payload;
    },
    setMessage: (state, action) => {
      state.message = action.payload;
    },
  },
});
export const { setToken, setIsAuth, setPermission, rememberAuth, setRoles, setMessage } =
  authSlice.actions;
export default authSlice.reducer;
