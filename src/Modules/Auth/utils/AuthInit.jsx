// import {useEffect} from 'react';
import { useSelector } from "react-redux";
import useLogin from '../Login/Core/Action';
import useAuth from '../core/Action';
import { useEffect, useState } from 'react';
import useProfile from '../../Views/ProfilePages/core/Action';
import { setRoleId } from "../Login/Core/Reducer";
import { useDispatch } from 'react-redux';
import * as authHelper from '../helper/authHelper';
import useFood from "../../Views/food/core/Action";
import useTable from "../../Views/Table/core/Action";
import useRole from "../../Views/Roles/core/Action";


function AuthInit({ children }) {
  const { token } = useSelector(state => state.auth)
  const { params } = useSelector((state) => state.food);

  const { logout } = useLogin()
  const { getProfile, getAddessByToken } = useProfile();
  const { getTableData } = useTable();
  const { getFood } = useFood();
  const { getRoles } = useRole();
  const { loginSuccess, getRoleById, getUser } = useAuth();
  const dispatch = useDispatch()
  const [showSplashScreen, setShowSplashScreen] = useState(true)

  useEffect(() => {
    const initAuth = async () => {

      if (token) {
        loginSuccess(true);
        try {
          const roleId = authHelper.getRoleId();
          await getRoleById(roleId); // Assuming getRoleById returns a promise
          dispatch(setRoleId(roleId));
          getUser()
          getAddessByToken()
          getProfile();
          getFood(params);
          getRoles()
          getTableData();
        } catch (error) {
          console.error('Error fetching profile or role:', error);
          // Consider additional error handling actions (e.g., display error message)
        } finally {
          setShowSplashScreen(false);
        }
      } else {
        logout();
        setShowSplashScreen(false);
      }
    };
    initAuth();

    // eslint-disable-next-line
  }, [token]); // Optimized dependency array

  return showSplashScreen ? <>{children}</> : <>{children}</>
}

export { AuthInit };
