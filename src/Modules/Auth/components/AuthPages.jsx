import React from 'react';
import {Outlet} from "react-router-dom";

function AuthPages(props) {
    return (
       <Outlet/>
    );
}

export default AuthPages;