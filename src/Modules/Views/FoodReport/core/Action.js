import {
  setCurrentFoodReport,
  setFoodReport,
  setFoodReportPaging,
  setMessage,
  setParams,
} from "./Reducer";
import { reqFoodReport, reqFoodReportChart } from "./Request";
import { useSelector, useDispatch } from "react-redux";

export const useFoodReport = () => {
  const { params } = useSelector((state) => state.foodReports);
  const dispatch = useDispatch();

  const getFoodReport = async () => {
    try {
      const res = await reqFoodReport({ params });
      dispatch(setFoodReport(res.data.data));
      dispatch(setFoodReportPaging(res.data.data.paging));
    } catch (e) {}
  };
  const getFoodReportChart = (month) => async (dispatch) => {
    try {
      const res = await reqFoodReportChart({ month });

      const array = res.data.data; // Extract the array from the response

      // Calculate totals (same logic as in the reducer)
      const totals = array.reduce(
        (acc, element) => {
          acc.totalQuantitySold += element.totalQuantitySold;
          acc.totalPrice += element.totalPrice;
          return acc;
        },
        { totalQuantitySold: 0, totalPrice: 0 }
      );

      // Dispatch the action with both items and totals
      dispatch(
        setCurrentFoodReport({
          items: array,
          totalQuantitySold: totals.totalQuantitySold,
          totalPrice: totals.totalPrice,
        })
      );
    } catch (error) {
      // Handle errors more effectively
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        dispatch(
          setMessage(
            error.response.data.message || "Error fetching food report chart."
          )
        );
      } else if (error.request) {
        // The request was made but no response was received
        dispatch(setMessage("Network error. Please try again later."));
      } else {
        // Something happened in setting up the request that triggered an Error
        dispatch(setMessage("An error occurred while fetching the report."));
      }
      console.error(error); // Log the error for debugging
    }
  };

  const handleSearch = (text) => {
    dispatch(setParams({ page: 1, query: text }));
  };

  const handlePage = (page) => dispatch(setParams({ page: page }));

  const handleSort = (field) => {
    const newOrder = params.order === "asc" ? "desc" : "asc";
    dispatch(setParams({ order: newOrder }));
  };

  const handleFilter = (name, value) => {
    dispatch(setParams({ [name]: value }));
  };
  return {
    getFoodReport,
    handleSearch,
    handlePage,
    handleSort,
    handleFilter,
    getFoodReportChart,
  };
};
