import { createSlice } from "@reduxjs/toolkit";

const getCurrentDate = () => {
  const date = new Date();
  const year = date.getFullYear();
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  return `${year}:${month}`; // Use '-' as the separator
};

const foodReportSlice = createSlice({
  name: " foodReport",
  initialState: {
    foodReportList: [],
    message: {},
    foodReportPaging: {
      items: [],
      totalQuantitySold: 0,
      totalPrice: 0,
    },
    currentFoodReport: {},
    params: {
      day: null,
      start: null,
      end: null,
      month: getCurrentDate(),
      size: 12,
      page: 1,
      query: null,
      order: "asc",
    },
  },

  reducers: {
    setMessage: (state, action) => {
      state.message = action.payload;
    },
    setFoodReport: (state, action) => {
      state.foodReportList = action.payload;
    },
    setParams: (state, action) => {
      state.params = { ...state.params, ...action.payload };
    },
    setFoodReportPaging: (state, action) => {
      state.foodReportPaging = action.payload;
    },
    setCurrentFoodReport: (state, action) => {
      state.currentFoodReport = { ...state.currentFoodReport, ...action.payload };
    },
  },
});

export const {
  setMessage,
  setFoodReport,
  setParams,
  setFoodReportPaging,
  setCurrentFoodReport,
} = foodReportSlice.actions;

export default foodReportSlice.reducer;
