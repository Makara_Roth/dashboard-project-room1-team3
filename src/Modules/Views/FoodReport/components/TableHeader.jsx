import React from 'react';
import { useSelector } from 'react-redux';

import {
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
    Typography,
  } from "@mui/material";
import { useFoodReport } from '../core/Action';


const TableHeader = () => {
    const {params} = useSelector((state)=> state.foodReports)
    const {order} = params
    const {handleSort} = useFoodReport();

    return (
        <TableHead>
      <TableRow>
        <TableCell>
          <TableSortLabel
            active={order === "id"}
            direction={order}
            onClick={() => handleSort("id")}
          >
            <Typography variant="subtitle2" fontWeight={600}>
              No
            </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
          <TableSortLabel
            // active={sort === "name"}
            // direction={order}
            // onClick={() => handleSort("name")}
          >
            <Typography variant="subtitle2" fontWeight={600}>
              Code
            </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
          <TableSortLabel
            // active={sort === "gender"}
            // direction={order}
            // onClick={() => handleSort("gender")}
          >
            <Typography variant="subtitle2" fontWeight={600}>
              Name
            </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
        <TableSortLabel
            // active={sort === "email"}
            // direction={order}
            // onClick={() => handleSort("email")}
          >
          <Typography variant="subtitle2" fontWeight={600}>
            UnitPrice
          </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
          <Typography variant="subtitle2" fontWeight={600}>
          TotalQuantitySold
          </Typography>
        </TableCell>

        <TableCell>
          <Typography variant="subtitle2" fontWeight={600}>
           TotalPrice 
          </Typography>
        </TableCell>
        {/* {
          ((userPermission?.find((per) => per.name === "edit-role"))?.status === 1 || (userPermission?.find((per) => per.name === "delete-role"))?.status === 1) && 
        <TableCell align="center">
          <Typography variant="subtitle2" fontWeight={600}>
            Action
          </Typography>
        </TableCell>
        } */}
      </TableRow>
    </TableHead>
    );
}

export default TableHeader;
