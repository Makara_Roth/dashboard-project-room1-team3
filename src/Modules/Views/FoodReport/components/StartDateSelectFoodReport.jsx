import * as React from 'react';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Box from '@mui/material/Box';
import { useDispatch } from 'react-redux';
import { setParams } from '../core/Reducer';
import dayjs from 'dayjs';
import { TextField } from '@mui/material';
import { useSelector } from 'react-redux';

export default function StartDateSelectFoodReport() {
    const dispatch = useDispatch();
    const [inputError, setInputError] = React.useState(false);
    const { params } = useSelector(state => state.foodReports)
    const { start } = params

    const handleDateChange = (newValue) => {
      if (newValue) {
          const formattedDate = newValue.format('YYYY:MM:DD');
          console.log(formattedDate);
          if (dayjs(formattedDate).isValid()) {
              dispatch(setParams({ start: formattedDate }));
              setInputError(false);
          } else {
              setInputError(true);
          }
      } else {
          dispatch(setParams({ start: null}));
          setInputError(false);
      }
  };
    return (
      <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box
          sx={{
              width: '70%',
              display: 'flex',
              justifyContent: 'center',
              position: 'relative',
          }}
      >
          <DatePicker
              views={['year', 'month', 'day']}
              label="Start Date"
              openTo="day"
              value={start ? dayjs(start, 'YYYY:MM:DD') : null}
              onChange={handleDateChange}
              renderInput={(params) => (
                  <TextField
                      {...params}
                      size='small'
                      error={inputError}
                      helperText={inputError && 'Invalid Date Format'}
                  />
              )}
              clearable
          />
      </Box>
  </LocalizationProvider>
    );
}
