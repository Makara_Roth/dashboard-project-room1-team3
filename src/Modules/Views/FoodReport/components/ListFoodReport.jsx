
import React from 'react';
import {
    TableCell,
    TableRow,
    Typography,
} from "@mui/material";
const ListFoodReport = ({ food, totalPrice, unitPrice, totalQty }) => {

    return (
        <TableRow>
            <TableCell>
                <Typography
                    sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                    }}
                >
                    {food.id}
                </Typography>
            </TableCell>

            <TableCell>
                <Typography
                    sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                    }}
                >
                    {food.code}
                </Typography>
            </TableCell>

            <TableCell>
                <Typography
                    sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                    }}
                >
                    {food.name}
                </Typography>
            </TableCell>

            <TableCell>
                <Typography
                    sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                    }}
                >
                    $ {unitPrice}
                </Typography>
            </TableCell>

            <TableCell>
                <Typography
                    sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                        textAlign: "center"
                    }}
                >
                    {totalQty}
                </Typography>
            </TableCell>
            <TableCell>
                <Typography
                    sx={{
                        fontSize: "15px",
                        fontWeight: "500",
                    }}
                >
                    $ { Number(totalPrice.toFixed(2))}
                </Typography>
            </TableCell>
        </TableRow>
    );
}

export default ListFoodReport;
