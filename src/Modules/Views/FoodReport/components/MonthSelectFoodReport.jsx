import React from 'react';
import TextField from '@mui/material/TextField';
import Box from "@mui/material/Box"
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import dayjs  from 'dayjs';
import { useDispatch , useSelector} from 'react-redux';
import { setParams } from '../core/Reducer';



export const MonthSelectFoodReport = () => {
    const { params } = useSelector((state) => state.foodReports)   
    const {month} = params
    const [inputError, setInputError] = React.useState(false);
    const dispatch = useDispatch()

    const handleDateChange = (newValue) => {
        if (newValue) {
            const formattedDate = newValue.format('YYYY:MM');
            console.log(formattedDate);
            if (dayjs(formattedDate).isValid()) {
                dispatch(setParams({ month: formattedDate }));
                setInputError(false);
            } else {
                setInputError(true);
            }
        } else {
            dispatch(setParams({ month: null}));
            setInputError(false);
        }
    };
    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box
          sx={{
              width: '70%',
              display: 'flex',
              justifyContent: 'center',
              position: 'relative',
          }}
      >
          <DatePicker
              views={['year', 'month']}
              label="Month "
              openTo="month"
              value={month ? dayjs(month, 'YYYY:MM') : null}
              onChange={handleDateChange}
              renderInput={(params) => (
                  <TextField
                      {...params}
                      size='small'
                      error={inputError}
                      helperText={inputError && 'Invalid Date Format'}
                  />
              )}
              clearable
          />
      </Box>
  </LocalizationProvider>
    );
}

