import React, { useEffect } from 'react'
import { Grid, Box, Typography, Button, Container, TableContainer, Table, TableRow, TableBody, TableCell } from "@mui/material";
import { useSelector } from 'react-redux';
import { useFoodReport } from '../core/Action';
import SearchField from '../../../../Globals/helpers/components/SearchField';
import ListFoodReport from './ListFoodReport';
import TableHeader from './TableHeader';
import GlobalPagination from '../../../../Globals/helpers/components/Pagination';
import StartDateSelectFoodReport from './StartDateSelectFoodReport';
import { EndDateSelectFoodReport } from './EndDateSelectFoodReport';
import { MonthSelectFoodReport } from './MonthSelectFoodReport';
import Breadcrumb from '../../../../Globals/helpers/components/Breadcrumb';


const FoodReport = () => {
  const { params, foodReportList, foodReportPaging } = useSelector((state) => state.foodReports)
  const { page } = params
  const { getFoodReport, handleChange, handleFilter, handleSearch } = useFoodReport();

  useEffect(() => {

    getFoodReport()
    // eslint-disable-next-line 
  }, [params])


  return (
    <Container maxWidth="lg"
      sx={{
        marginTop: 3
      }}>
      <Typography variant="h4" className="fw-bold">List Food Report</Typography>
      <Box>
      <Breadcrumb title="Dashboard" subtitle="List Users" />
      </Box>
      <Box>
        <Grid
          container
          item
          xs={12}
          display="flex"
          justifyContent="flex-end"
          sx={{ mb: 2 }}
        >
          <Button variant="contained">Export PDF</Button>
        </Grid>
      </Box>
      <Grid container sx={{ mt: 2 }}>
        <Grid item xs={12} md={4}>
          <Box sx={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
            <SearchField
              title="Search Name..."
              params={params}
              onChange={(e) => handleSearch(e.target.value)}
              fullWidth
            />
          </Box>
        </Grid>
        <Grid item xs={12} md={8} >
          <Grid container >
            <Grid  item xs={12} md={4}>
              <MonthSelectFoodReport/>
            </Grid>
            <Grid  item xs={12} md={4} >
              <StartDateSelectFoodReport />
            </Grid>
            <Grid  item xs={12} md={4}>
              <EndDateSelectFoodReport />
            </Grid>
          </Grid>
        </Grid>

      </Grid>
      <Box sx={{ overflow: 'auto', width: { xs: '300px', sm: '500px', md: '950px', lg: '1100px' } }}>
        <TableContainer>
          <Table
            aria-label="simple table"
            sx={{
              whiteSpace: 'nowrap',
            }}
          >
            <TableHeader />
            <TableBody>
              {foodReportList.length > 0 ? (
                foodReportList.map((frl, index) => (
                  <ListFoodReport
                    key={index}
                    food={frl.food}
                    totalPrice={frl.totalPrice}
                    unitPrice={frl.unitPrice}
                    totalQty={frl.totalQuantitySold}
                  />
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan={8} className="text-center">
                    <Typography
                      sx={{
                        fontSize: '18px',
                        fontWeight: '600',
                      }}
                    >
                      User not found
                    </Typography>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <Box sx={{ marginTop: 2 }}>
          <GlobalPagination
            params={params}
            totalPage={foodReportPaging?.totalPage}
            page={page}
            handleChange={handleChange}
            handleFilter={handleFilter}
          />
        </Box>
      </Box>
    </Container>
  )
}
export default FoodReport;