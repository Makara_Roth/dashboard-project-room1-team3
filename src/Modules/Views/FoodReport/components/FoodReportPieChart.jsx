import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Card, CardContent, Typography, Box } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useFoodReport } from "../core/Action";

const FoodReportPieChart = () => {
  const { params, currentFoodReport } = useSelector(
    (state) => state.foodReports,
  );
  const { getFoodReportChart } = useFoodReport();
  const dispatch = useDispatch();
  const { month } = params;

  useEffect(() => {
    dispatch(getFoodReportChart(month));
    // eslint-disable-next-line
  }, [dispatch, month]);

  const data = {
    labels: ["Totals Price $", "Totals QTY"],
    datasets: [
      {
        data: [
          currentFoodReport?.totalPrice,
          currentFoodReport?.totalQuantitySold,
        ],
        backgroundColor: ["#3f51b5", "#e0e0e0"],
        hoverBackgroundColor: ["#303f9f", "#d5d5d5"],
        borderWidth: 1,
      },
    ],
  };
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: false,
      },
    },
  };
  return (
    <>
      <Card>
        <CardContent>
          <Typography
            variant="h6"
            component="div"
            sx={{
              mb: 2,
              display: "flex",
              justifyContent: "center",
            }}
          >
            Food Reports
          </Typography>
          {data?.datasets[0]?.data[0] === 0 ? (
            <>
              <Box
                sx={{ display: "flex", justifyContent: "center", color: "red" }}
              >
                <Typography
                  sx={{
                    fontSize: "18px",
                    fontWeight: "600",
                  }}
                >
                  Income not found
                </Typography>
              </Box>
            </>
          ) : (
            <>
              <Box sx={{ height: "150px" }}>
                <Doughnut data={data} options={options} />
              </Box>
            </>
          )}
        </CardContent>
      </Card>
    </>
  );
};

export default FoodReportPieChart;
