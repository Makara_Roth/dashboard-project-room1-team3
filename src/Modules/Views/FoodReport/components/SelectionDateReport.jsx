    import React from 'react';
import TextField from '@mui/material/TextField';
import Box from "@mui/material/Box"
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import dayjs  from 'dayjs';
import { useDispatch , useSelector} from 'react-redux';
import { setParams } from '../core/Reducer';



const SelectionDateFoodReport = () => {
    const { params } = useSelector((state) => state.foodReports)   
    const {month} = params
    const dispatch = useDispatch()

    const handleDateChange = (date) => {
        if (date) {
            const formattedDate = date.format('YYYY:MM');
            if (dayjs(formattedDate).isValid()) {
                dispatch(setParams({ month: formattedDate }));
            }
        } else {
            dispatch(setParams({ month: null }));
        }
    };

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <Box
              sx={{
                    width: '40%',
                    hight:'10%',
                    display: 'flex',
                    justifyContent: 'center',
                    position: 'relative',
                }}
            >
            <DatePicker
                label="Select Month"
                openTo="month"
                views={['year', 'month']}
                value={month ? dayjs(month, 'YYYY:MM') : null}
                onChange={handleDateChange}
                renderInput={(params) => <TextField {...params} />}
            />
            </Box>
        </LocalizationProvider>
    );
}

export default SelectionDateFoodReport;
