import axios from "axios";

// Get all Tables
const FetchTableData = (params) => {
    return axios.get(`/api/tables`, {
        params: params,
    });
};
// Create Table
const fetchCreateTable = (name, seat_Capacity, status) => {
    return axios.post(`/api/tables`, {
        name: name,
        seat_Capacity: seat_Capacity,
        status: status,
    });
};
// Delete Table
const fetchDeleteTable = (id) => {
    return axios.delete(`/api/tables/${id}`);
};
// Update Table
const fetchUpdateTable = (id, name, seatCapacity, status) => {
    return axios.put(`/api/tables/${id}`, {
        name: name,
        seatCapacity: seatCapacity,
        status: status,
    });
};
export { FetchTableData, fetchCreateTable, fetchDeleteTable, fetchUpdateTable };
