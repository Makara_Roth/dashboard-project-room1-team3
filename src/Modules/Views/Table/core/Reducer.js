import { createSlice } from "@reduxjs/toolkit";
const createTable = {
    name: "",
    seat_Capacity: "",
    status: "",
};
const updateTable = {
    id: "",
    name: "",
    seatCapacity: "",
    status: "",
};
const tableSlice = createSlice({
    name: "table",
    initialState: {
        currentTable: [],
        totalPage: 0,
        currentTableById: [],
        paging: {},
        createTable: createTable,
        updateTable: updateTable,
        params: {
            size: 12,
            page: 1,
            query: "",
            sort: "id",
            order: "desc",
        },
    },
    reducers: {
        setTables: (state, action) => {
            state.currentTable = action.payload.data;
            state.totalPage = action.payload.paging.totalPage;
        },
        setPaging: (state, action) => {
            state.paging = action.payload;
        },
        setTablesById: (state, action) => {
            state.currentTableById = action.payload;
        },
        setCreateTable: (state, action) => {
            state.createTable = action.payload;
        },
        deleteTable: (state, action) => {
            state.currentTable = state.currentTable.filter(
                (table) => table.id !== action.payload
            );
        },
        setUpdateTable: (state, action) => {
            state.updateTable = action.payload;
        },
        setParams: (state, action) => {
            state.params = { ...state.params, ...action.payload };
        },
    },
});
export const {
    setTables,
    setTablesById,
    setCreateTable,
    deleteTable,
    setUpdateTable,
    setParams,
    setPaging,
} = tableSlice.actions;
export default tableSlice.reducer;
