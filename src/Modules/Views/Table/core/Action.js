import {
    FetchTableData,
    fetchCreateTable,
    fetchDeleteTable,
    fetchUpdateTable,
} from "./Request";
import { useDispatch, useSelector } from "react-redux";
import {
    setTables,
    setCreateTable,
    deleteTable,
    setUpdateTable,
    setParams,
    setPaging,
} from "./Reducer";
import { toast } from "react-toastify";

const useTable = () => {
    const dispatch = useDispatch();

    const { params } = useSelector((state) => state.table);

    const { sort } = params;

    const getTableData = async () => {
        try {
            FetchTableData(params).then((res) => {
                dispatch(setTables(res.data));
            });
        } catch (error) {
            console.error("Failed to fetch tables:", error.response.data.message);
            throw error.response.data.message;
        }
    };

    const getPaging = () => {
        FetchTableData().then((res) => {
            dispatch(setPaging(res.data.paging));
        });
    };

    const createTable = async (name, seat_Capacity, status) => {
        try {
            const res = await fetchCreateTable(name, seat_Capacity, status);
            toast.success(`Table create ${res.data.message} `, {
                position: "top-center",
            });
            dispatch(setCreateTable(res.data.data));
        } catch (error) {
            throw error;
        }
    };
    const getDeleteTable = async (id) => {
        try {
            await fetchDeleteTable(id);
            toast.success(`Table deleted successfully`, {
                position: "top-center",
            });
            dispatch(deleteTable(id));
        } catch (error) {
            throw error;
        }
    };
    const getUpdateTable = async (id, name, seatCapacity, status) => {
        try {
            const res = await fetchUpdateTable(id, name, seatCapacity, status);
            toast.success(`Table updated ${res.data.message} `, {
                position: "top-center",
            });
            dispatch(setUpdateTable(res.data.data));
        } catch (error) {
            throw error;
        }
    };

    const handlePage = (page) => dispatch(setParams({ page: page }));

    const handleSearch = (name) => {
        dispatch(setParams({ page: 1, query: name }));
    };

    const handleSort = (field) => {
        const newOrder = params.order === "asc" ? "desc" : "asc";
        dispatch(setParams({ sort: field, order: newOrder }));
    };

    const handleFilter = (name, value) => {
        dispatch(setParams({ [name]: value }));
    };

    return {
        getTableData,
        getPaging,
        createTable,
        getDeleteTable,
        getUpdateTable,
        handlePage,
        handleSearch,
        handleSort,
        handleFilter,
    };
};
export default useTable;
