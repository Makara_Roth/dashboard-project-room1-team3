import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";

export default function AlertDialogSlide({ message, onClose }) {
  const [open, setOpen] = React.useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    // Call the onClose prop
    onClose();
    // ...
  };
  return (
    <>
      <Button onClick={handleClickOpen}></Button>
      <Dialog
        open={open}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
        style={{
          textAlign: "center",
          backgroundColor: "#fff !important",
          color: "#000 !important",
        }}
      >
        <DialogTitle style={{ paddingTop: "20px" }}>{"Warning! "}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {message}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Again</Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
