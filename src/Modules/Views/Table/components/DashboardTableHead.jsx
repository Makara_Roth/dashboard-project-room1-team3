import {
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
    Typography,
} from "@mui/material";
import React from "react";
import useTable from "../core/Action";
import { useSelector } from "react-redux";

function DashboardTableHead() {
    const { handleSort } = useTable();

    const { sort, order } = useSelector((state) => state.table.params);

    return (
        <TableHead>
            <TableRow>
                <TableCell>
                    <TableSortLabel
                        active={sort === "id"}
                        direction={order}
                        onClick={() => handleSort("id")}
                    >
                        <Typography variant="subtitle2" fontWeight={600}>
                            No
                        </Typography>
                    </TableSortLabel>
                </TableCell>
                <TableCell>
                    <TableSortLabel
                        active={sort === "name"}
                        direction={order}
                        onClick={() => handleSort("name")}
                    >
                        <Typography variant="subtitle2" fontWeight={600}>
                            Name
                        </Typography>
                    </TableSortLabel>
                </TableCell>
                <TableCell>
                    <TableSortLabel
                        active={sort === "status"}
                        direction={order}
                        onClick={() => handleSort("status")}
                    >
                        <Typography variant="subtitle2" fontWeight={600}>
                            Status
                        </Typography>
                    </TableSortLabel>
                </TableCell>
                <TableCell>
                    <TableSortLabel
                        active={sort === "seatCapacity"}
                        direction={order}
                        onClick={() => handleSort("seatCapacity")}
                    >
                        <Typography variant="subtitle2" fontWeight={600}>
                            Seat Capacity
                        </Typography>
                    </TableSortLabel>
                </TableCell>
                <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                        Action
                    </Typography>
                </TableCell>
            </TableRow>
        </TableHead>
    );
}

export default DashboardTableHead;
