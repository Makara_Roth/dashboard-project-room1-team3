import React, { useEffect } from "react";
import Table from "react-bootstrap/Table";
import useTable from "../core/Action";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";

const TableData = () => {
  const navigate = useNavigate();
  const { getTable } = useTable();
  const { table } = useSelector((state) => state.table);
  useEffect(() => {
    getTable();
  }, [getTable]);

  return (
    <>
      <Table
        className={"mt-3"}
        sx={{ "& thead th:nth-child(1)": { width: "40%" } }}
      >
        <thead align={"center"}>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Seat Capacity</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody align={"center"}>
          {table.map((row) => (
            <tr key={row.id}>
              <td>{row.id}</td>
              <td>{row.name}</td>
              <td>{row.seat_Capacity}</td>
              <td>{row.status}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};
export default TableData;

