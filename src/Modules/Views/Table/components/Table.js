import React from "react";
import DashboardCard from "../../../Components/shared/DashboardCard";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";
import DashboardTable from "./DashboardTable";

function TableBooking() {
    return (
        <>
            <h4 className="fw-bold">Table</h4>
            <div className="d-flex justify-content-between pb-3">
                <Breadcrumb title="Dashboard" subtitle="Table" />
            </div>
            <DashboardCard>
                <DashboardTable />
            </DashboardCard>
        </>
    );
}

export default TableBooking;
