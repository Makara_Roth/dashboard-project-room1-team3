import React, { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useTable from "../core/Action";
import {
  TableCell,
  TableRow,
  TextField,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
} from "@mui/material";
import { FetchTableData } from "../core/Request";
import AlertDialogSlide from "../../Roles/components/DeleteRoleDialog";

export default function AlertCreateTableDialog({ setRefresh }) {
  const [open, setOpen] = useState(false);
  const { createTable } = useTable();
  const [name, setName] = useState("");
  const [seat_Capacity, setSeatCapacity] = useState("");
  const [status, setStatus] = useState("");
  const [message, setMessage] = useState("");
  const [showDialog, setShowDialog] = useState(false);

  const handleCreateTable = async (e) => {
    e.preventDefault();
    if (name === "" || seat_Capacity === "" || status === "") {
      setMessage("Please fill all the fields");
      setShowDialog(true);
      setTimeout(() => setShowDialog(false), 2000);
      return;
    }
    if (isNaN(seat_Capacity) || seat_Capacity < 1) {
      setMessage("Please provide the number of seats for the table");
      setShowDialog(true);
      setTimeout(() => setShowDialog(false), 2000);
      return;
    }
    try {
      await createTable(name, seat_Capacity, status);
      FetchTableData();
      setOpen(false);
      setRefresh((pre) => pre + 1);
    } catch (error) {
      setMessage(error.response.data.message);
      console.log(error.response.data.message);
      setShowDialog(true);
      setTimeout(() => setShowDialog(false), 2000);
    }
  };
  const nameInputRef = React.useRef();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  React.useEffect(() => {
    if (open) {
      const timer = setTimeout(() => {
        if (nameInputRef.current) {
          nameInputRef.current.focus();
        }
      }, 100);

      return () => clearTimeout(timer); // cleanup on unmount
    }
  }, [open]);

  return (
    <>
      <Button
        onClick={handleClickOpen}
        style={{ backgroundColor: "#4085ad", color: "#ffff" }}
      >
        Add Table
      </Button>
      <Dialog
        open={open}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
        style={{
          textAlign: "center",
          backgroundColor: "#fff !important",
          color: "#000 !important",
        }}
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <DialogTitle style={{ paddingTop: "20px" }}>
              {"Create Table"}
            </DialogTitle>
            <TableRow>
              <TableCell>
                <TextField
                  id="outlined-basic"
                  label="Table Name"
                  variant="outlined"
                  size="small"
                  onChange={(e) => setName(e.target.value)}
                  inputRef={nameInputRef}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <TextField
                  id="outlined-basic"
                  label="Seat Capacity"
                  variant="outlined"
                  size="small"
                  onChange={(e) => setSeatCapacity(e.target.value)}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <FormControl
                  variant="outlined"
                  size="small"
                  style={{ width: "200px" }}
                >
                  <InputLabel id="status-label">Status</InputLabel>
                  <Select
                    labelId="status-label"
                    id="status-select"
                    label="Status"
                    onChange={(e) => setStatus(e.target.value)}
                  >
                    <MenuItem value={"Available"}>Available</MenuItem>
                    <MenuItem value={"Booked"}>Booked</MenuItem>
                  </Select>
                </FormControl>
              </TableCell>
            </TableRow>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            style={{
              backgroundColor: "#f1f1f1",
              color: "",
              marginRight: 3,
            }}
            onClick={handleClose}
          >
            Cancel
          </Button>
          <Button
            style={{
              backgroundColor: "#4085ad",
              color: "#ffff",
            }}
            onClick={handleCreateTable}
          >
            Create Table
          </Button>
        </DialogActions>
      </Dialog>
      {showDialog && (
        <AlertDialogSlide
          message={`${message} ! Please try again`}
          onClose={() => setShowDialog(false)}
        />
      )}
    </>
  );
}
