import React from "react";
import {
    Chip,
    TableBody,
    TableCell,
    TableRow,
    Typography,
} from "@mui/material";
import AlertDeleteDialog from "./AlertDeleteDialog";
import AlertUpdateDialog from "./AlertUpdateDialog";

const DashboardTableBody = ({ currentTable, setRefresh }) => {
    return (
        <>
            <TableBody>
                {currentTable.map((table) => (
                    <TableRow key={table.id}>
                        <TableCell>
                            <Typography variant="subtitle2" fontWeight={600}>
                                {table.id}
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography variant="subtitle2" fontWeight={600}>
                                {table.name}
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <Chip
                                style={{
                                    backgroundColor:
                                        table.status === "Available"
                                            ? "#4caf50"
                                            : table.status === "Booked"
                                            ? "#ff3d00"
                                            : "yellow",
                                }}
                                sx={{
                                    px: "4px",
                                }}
                                size="small"
                                label={table.status}
                                color="primary"
                            ></Chip>
                        </TableCell>
                        <TableCell>
                            <Typography variant="subtitle2" fontWeight={600}>
                                {table.seat_Capacity}
                            </Typography>
                        </TableCell>
                        <TableCell>
                            <div className="d-flex ">
                                {
                                    <AlertUpdateDialog
                                    id={table.id}
                                    name={table.name}
                                    seatCapacity={table.seat_Capacity}
                                    status={table.status}
                                    setRefresh={setRefresh}
                                    />
                                }
                                {
                                    <AlertDeleteDialog
                                        id={table.id}
                                        setRefresh={setRefresh}
                                    />
                                }
                            </div>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </>
    );
};

export default DashboardTableBody;
