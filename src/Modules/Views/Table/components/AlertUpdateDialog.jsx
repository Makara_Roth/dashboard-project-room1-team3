import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useTable from "../core/Action";
import {
    Box,
    Table,
    TableCell,
    TableRow,
    TextField,
    Select,
    MenuItem,
    FormControl,
    InputLabel,
} from "@mui/material";
import { FetchTableData } from "../core/Request";
import { ReactComponent as Edit } from "../../../../assets/images/edit.svg"

export default function AlertUpdateDialog({
    id,
    setRefresh,
    name,
    seatCapacity,
    status,
}) {
    const [open, setOpen] = React.useState(false);
    const { getUpdateTable } = useTable();
    const [tableName, setName] = React.useState(name);
    const [tableSeatCapacity, setSeatCapacity] = React.useState(seatCapacity);
    const [tableStatus, setTableStatus] = React.useState(status);
    const nameInputRef = React.useRef();
    const seatCapacityInputRef = React.useRef();
    const statusInputRef = React.useRef();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleUpdate = () => {
        getUpdateTable(id, tableName, tableSeatCapacity, tableStatus);
        FetchTableData();
        setOpen(false);
        setRefresh((pre) => pre + 1);
    };

    React.useEffect(() => {
        if (open) {
            const timer = setTimeout(() => {
                if (
                    (nameInputRef.current,
                    seatCapacityInputRef.current,
                    statusInputRef.current)
                ) {
                    nameInputRef.current.focus();
                    seatCapacityInputRef.current.focus();
                    statusInputRef.current.focus();
                }
            }, 100);

            return () => clearTimeout(timer); // cleanup on unmount
        }
    }, [open]);

    return (
        <>
            <div
                onClick={handleClickOpen}
                className="ps-2"
                style={{ cursor: "pointer" }}
            >
                <Edit style={{color: "#4085ad"}}/>
            </div>
            <Dialog
                open={open}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
                style={{
                    textAlign: "center",
                    backgroundColor: "#fff !important",
                    color: "#000 !important",
                }}
            >
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        <Box
                            sx={{
                                overflow: "auto",
                                width: { xs: "280px", sm: "auto" },
                            }}
                        >
                            <DialogTitle style={{ paddingTop: "20px" }}>
                                {"Update Table"}
                            </DialogTitle>
                            <Table
                                aria-label="simple table"
                                sx={{
                                    whiteSpace: "nowrap",
                                    mt: 2,
                                }}
                            >
                                <TableRow>
                                    <TableCell>
                                        <TextField
                                            id="outlined-basic"
                                            label="Table Name"
                                            variant="outlined"
                                            size="small"
                                            value={tableName}
                                            onChange={(e) =>
                                                setName(e.target.value)
                                            }
                                            inputRef={nameInputRef}
                                        />
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell>
                                        <TextField
                                            id="outlined-basic"
                                            label="Seat Capacity"
                                            variant="outlined"
                                            size="small"
                                            value={tableSeatCapacity}
                                            onChange={(e) =>
                                                setSeatCapacity(e.target.value)
                                            }
                                            inputRef={seatCapacityInputRef}
                                        />
                                    </TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell>
                                        <FormControl
                                            variant="outlined"
                                            size="small"
                                            style={{ width: "200px" }}
                                        >
                                            <InputLabel id="status-label">
                                                Status
                                            </InputLabel>
                                            <Select
                                                labelId="status-label"
                                                id="status-select"
                                                label="Status"
                                                value={tableStatus}
                                                onChange={(e) =>
                                                    setTableStatus(
                                                        e.target.value
                                                    )
                                                }
                                                inputRef={statusInputRef}
                                            >
                                                <MenuItem value={"All"}>
                                                    All
                                                </MenuItem>
                                                <MenuItem value={"Available"}>
                                                    Available
                                                </MenuItem>
                                                <MenuItem value={"Booked"}>
                                                    Booked
                                                </MenuItem>
                                            </Select>
                                        </FormControl>
                                    </TableCell>
                                </TableRow>
                            </Table>
                        </Box>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        style={{
                            backgroundColor: "#f1f1f1",
                            color: "",
                            marginRight: 3,
                        }}
                        onClick={handleClose}
                    >
                        Cancel
                    </Button>
                    <Button
                        style={{
                            backgroundColor: "#4085ad",
                            color: "#ffff",
                        }}
                        onClick={handleUpdate}
                    >
                        Update Table
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}
