import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useTable from "../core/Action";
import { FetchTableData } from "../core/Request";
import { ReactComponent as Delete } from "../../../../assets/images/trash.svg";

export default function AlertDeleteDialog({ id, setRefresh }) {
    const [open, setOpen] = React.useState(false);
    const { getDeleteTable } = useTable();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleDelete = () => {
        getDeleteTable(id);
        FetchTableData();
        setRefresh((pre) => pre + 1);
        setOpen(false);
    };
    return (
        <>
            <div onClick={handleClickOpen} style={{ cursor: "pointer" }}>
                <Delete className="text-danger"/>
            </div>
            <Dialog
                open={open}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
                style={{
                    textAlign: "center",
                    backgroundColor: "#fff !important",
                    color: "#000 !important",
                }}
            >
                <DialogTitle style={{ paddingTop: "20px" }}>
                    {"Warning! "}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        Do you want to delete this table?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        style={{
                            backgroundColor: "#f1f1f1",
                            color: "",
                            marginRight: 3,
                        }}
                        onClick={handleClose}
                    >
                        Cancel
                    </Button>
                    <Button
                        style={{
                            backgroundColor: "red",
                            color: "#ffff",
                        }}
                        onClick={handleDelete}
                    >
                        Delete Table
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}
