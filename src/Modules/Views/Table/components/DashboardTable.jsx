import React, { useEffect, useState } from "react";
import { Box, Table } from "@mui/material";
import DashboardTableBody from "./DashboardTableBody";
import useTable from "../core/Action";
import { useSelector } from "react-redux";
import DashboardTableHead from "./DashboardTableHead";
import AlertCreateTableDialog from "./AlertCreateTableDialog";
import { FetchTableData } from "../core/Request";
import GlobalPagination from "../../../../Globals/helpers/components/Pagination";
import SearchField from "../../../../Globals/helpers/components/SearchField";

function DashboardTable() {
  const { getTableData, getPaging } = useTable();
  const { currentTable } = useSelector((state) => state.table);
  const [refresh, setRefresh] = useState(0);

  const { handlePage, handleSearch, handleFilter } = useTable();

  const { params, totalPage } = useSelector((state) => state.table);

  const { page } = params;

  const handleChange = (value) => {
    handlePage(value);
  };

  useEffect(() => {
    FetchTableData();
    getTableData();
    getPaging();
    // eslint-disable-next-line
  }, [refresh, params]);

  return (
    <>
      <div className="d-flex justify-content-between">
        <SearchField
          title="Search Seat Capacity..."
          params={params}
          onChange={(e) => handleSearch(e.target.value)}
        />
        <div>
          <AlertCreateTableDialog setRefresh={setRefresh} />
        </div>
      </div>
      <Box sx={{ overflow: "auto", width: { xs: "280px", sm: "auto" } }}>
        <Table
          aria-label="simple table"
          sx={{
            whiteSpace: "nowrap",
            mt: 2,
          }}
        >
          <DashboardTableHead />
          <DashboardTableBody
            currentTable={currentTable}
            getTableData={getTableData}
            setRefresh={setRefresh}
            refresh={refresh}
          />
        </Table>
        <GlobalPagination
          params={params}
          totalPage={totalPage}
          page={page}
          handleChange={handleChange}
          handleFilter={handleFilter}
        />
      </Box>
    </>
  );
}

export default DashboardTable;
