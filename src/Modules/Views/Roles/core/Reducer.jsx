import { createSlice } from "@reduxjs/toolkit";
const createRole = {
    name: "",
    code: "",
};
const roleSlice = createSlice({
    name: "roles",
    initialState: {
        currentRole: [],
        totalPage: 0,
        currentRoleById: [],
        createRole: createRole,
        paging: {},
        searchTerm: "",
        params: {
            size: 12,
            page: 1,
            query: "",
            sort: "id",
            order: "desc",
        },
    },
    reducers: {
        setRoles: (state, action) => {
            state.currentRole = action.payload.data;
            state.totalPage = action.payload.paging.totalPage;
        },
        setRolesById: (state, action) => {
            state.currentRoleById = action.payload;
        },
        setCreateRole: (state, action) => {
            state.createRole = action.payload;
        },
        deleteRole: (state, action) => {
            state.currentRole = state.currentRole.filter(
                (role) => role.id !== action.payload
            );
        },
        setUpdatePermission: (state, action) => {
            state.updatePermission = action.payload;
        },
        setParams: (state, action) => {
            state.params = { ...state.params, ...action.payload };
        },
        setSearchTerm: (state, action) => {
            state.searchTerm = action.payload;
        },
        setPaging: (state, action) => {
            state.paging = action.payload;
        },
    },
});

export const {
    setRoles,
    setRolesById,
    setCreateRole,
    deleteRole,
    setUpdatePermission,
    setParams,
    setPaging,
} = roleSlice.actions;
export default roleSlice.reducer;
