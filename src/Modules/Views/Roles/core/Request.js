import axios from "axios";

// Get all users by role
const fetchRoles = async (params) => {
    return axios.get(`/api/roles`, {
        params: params,
    });
};

const fetchRolesById = (id) => {
    return axios.get(`/api/roles/${id}`);
};

// Create role
const fetchCreateRole = (name, code) => {
    return axios.post(`/api/roles`, {
        name: name,
        code: code,
    });
};
//Delete role
const fetchDeleteRole = (id) => {
    return axios.delete(`/api/roles/${id}`);
};

//Update role
const fetchUpdateRole = (id, name, code) => {
    return axios.put(`/api/role/${id}`, {
        name: name,
        code: code,
    });
};
//Update Permission
const fetchUpdatePermission = (id, updatePermission) => {
    return axios.put(`/api/roles/permission`, {
        role_id: id,
        permissions: updatePermission,
    });
};

export {
    fetchRoles,
    fetchRolesById,
    fetchCreateRole,
    fetchDeleteRole,
    fetchUpdateRole,
    fetchUpdatePermission,
};
