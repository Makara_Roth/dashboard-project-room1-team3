import {
  fetchRoles,
  fetchRolesById,
  fetchCreateRole,
  fetchDeleteRole,
  fetchUpdatePermission,
} from "./Request";
import { useDispatch, useSelector } from "react-redux";
import {
  setRoles,
  setRolesById,
  setCreateRole,
  setUpdatePermission,
  setParams,
  setPaging,
} from "./Reducer";
import { toast } from "react-toastify";

const useRole = () => {
  const dispatch = useDispatch();

  const { params } = useSelector((state) => state.role);

  const { sort } = params;

  const getRoles = () => {
    try {
      fetchRoles(params).then((res) => {
        dispatch(setRoles(res.data));
      });
    } catch (error) {
      console.error("Failed to fetch roles:", error);
    }
  };

  const getRolesById = (id) => {
    fetchRolesById(id).then((res) => {
      dispatch(setRolesById(res.data.data));
    });
  };

  const createRole = async (name, code) => {
    try {
      const res = await fetchCreateRole(name, code);
      toast.success(`Role create ${res.data.message} `, {
        position: "top-center",
      });
      dispatch(setCreateRole(res.data.data));
    } catch (error) {
      throw error; // Throw the error after logging it
    }
  };
  const getDeleteRole = async (id) => {
    try {
      const res = await fetchDeleteRole(id);
      toast.success(`Role delete ${res.data.message} `, {
        position: "top-center",
      });
      dispatch(setRoles(res.data.data));
    } catch (error) {
      throw error; // Throw the error after logging it
    }
  };

  const getUpdatePermission = (data) => {
    try {
      fetchUpdatePermission(data).then((res) => {
        toast.success(`Role update ${res.data.message} `, {
          position: "top-center",
        });
        dispatch(setUpdatePermission(res.data.data));
      });
    } catch (error) {
      console.error("Failed to fetch roles:", error);
      throw error;
    }
  };

  const getPaging = () => {
    fetchRoles().then((res) => {
      dispatch(setPaging(res.data.paging));
    });
  };

  const handlePage = (page) => dispatch(setParams({ page }));

  const handleSearch = (name) => dispatch(setParams({ query: name }));

  const handleSort = (field) => {
    const newOrder = params.order === "asc" ? "desc" : "asc";
    dispatch(setParams({ sort: field, order: newOrder }));
  };

  const handleFilter = (name, value) => {
    dispatch(setParams({ [name]: value }));
  };
  return {
    getRoles,
    getRolesById,
    createRole,
    getDeleteRole,
    getUpdatePermission,
    getPaging,
    handlePage,
    handleSearch,
    handleSort,
    handleFilter,
  };
};

export default useRole;
