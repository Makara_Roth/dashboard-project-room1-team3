import React from "react";
import DashboardCardList from "./DashboardCardList";
import DashboardCard from "../../../Components/shared/DashboardCard";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";

const Roles = () => {
  return (
    <>
      <h4 className="fw-bold">Roles List</h4>
      <div className="d-flex justify-content-between pb-3">
        <Breadcrumb title="Dashboard" subtitle="Roles" />
      </div>
      <DashboardCard>
        <DashboardCardList />
      </DashboardCard>
    </>
  );
};

export default Roles;
