import React, { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { TableCell, TableRow, TextField } from "@mui/material";
import AlertDialogSlide from ".//DeleteRoleDialog";
import useRole from "../core/Action";

export default function AlertCreateRoles({
    setRefresh,
    setMessage,
    setShowDialog,
    showDialog,
    setOpen,
    open,
    message,
}) {
    const [name, setRoleName] = useState("");
    const [code, setRoleCode] = useState("");
    const { createRole } = useRole();

    const handleCreateRole = async (e) => {
        e.preventDefault();
        if (name === "" || code === "") {
            setMessage("Please fill all the fields");
            setShowDialog(true);
            setTimeout(() => setShowDialog(false), 2000);
            return;
        }
        try {
            await createRole(name, code);
            setOpen(false);
            setRefresh((pre) => pre + 1);
            setRoleName("");
            setRoleCode("");
        } catch (error) {
            setMessage(error.response.data.message);
            console.log(error.response.data.message);
            setShowDialog(true);
            setTimeout(() => setShowDialog(false), 2000);
        }
    };

    const nameInputRef = React.useRef();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    React.useEffect(() => {
        if (open) {
            const timer = setTimeout(() => {
                if (nameInputRef.current) {
                    nameInputRef.current.focus();
                }
            }, 100);
            return () => clearTimeout(timer); // cleanup on unmount
        }
    }, [open]);

    return (
        <>
            <Button
                onClick={handleClickOpen}
                style={{ backgroundColor: "#4085ad", color: "#ffff" }}
            >
                Add Role
            </Button>
            <Dialog
                open={open}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
                style={{
                    textAlign: "center",
                    backgroundColor: "#fff !important",
                    color: "#000 !important",
                }}
            >
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        <DialogTitle style={{ paddingTop: "20px" }}>
                            {"Create Role"}
                        </DialogTitle>

                        <TextField
                            id="outlined-basic"
                            label="Role Name"
                            variant="outlined"
                            size="small"
                            value={name}
                            onChange={(e) => {
                                setRoleName(e.target.value);
                            }}
                            inputRef={nameInputRef}
                        />

                        <TableRow>
                            <TableCell>
                                <TextField
                                    id="outlined-basic"
                                    label="Code Role"
                                    variant="outlined"
                                    size="small"
                                    value={code}
                                    onChange={(e) => {
                                        setRoleCode(e.target.value);
                                    }}
                                />
                            </TableCell>
                        </TableRow>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        style={{
                            backgroundColor: "#f1f1f1",
                            color: "",
                            marginRight: 3,
                        }}
                        onClick={handleClose}
                    >
                        Cancel
                    </Button>
                    <Button
                        style={{
                            backgroundColor: "#4085ad",
                            color: "#ffff",
                        }}
                        onClick={handleCreateRole}
                    >
                        Create Role
                    </Button>
                </DialogActions>
            </Dialog>
            {showDialog && (
                <AlertDialogSlide
                    message={`${message} ! Please try again`}
                    onClose={() => setShowDialog(false)}
                />
            )}
        </>
    );
}
