import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router";
import DashboardCard from "../../../Components/shared/DashboardCard";
import {
  Box,
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import {
  fetchUpdatePermission,
  fetchRolesById,
  fetchDeleteRole,
} from "../core/Request";
import Switch from "@mui/material/Switch";
import { Delete } from "@mui/icons-material";

function PopupRoles() {
  const { id } = useParams();
  const navigation = useNavigate();
  const [fetchData, setFetchData] = useState([]);
  const [updateData, setUpdateData] = useState([]);

  useEffect(() => {
    if (updateData.length > 0) {
      fetchUpdatePermission(id, updateData);
    }
  }, [updateData, id]);

  useEffect(() => {
    setUpdateData(
      fetchData?.map((item) => {
        return {
          permission_id: item.id,
          status: item.status === 1 ? true : false,
        };
      })
    );
  }, [fetchData]);

  const handleDelete = () => {
    setTimeout(() => {
      fetchDeleteRole(id);
      navigation("/role");
    }, 2000);
  };

  useEffect(() => {
    fetchRolesById(id).then((data) => {
      setFetchData(data.data.data.permissions);
    });
  }, [id]);

  return (
    <>
      <DashboardCard>
        <Button onClick={() => navigation(`/role`)}>
          <Typography variant="subtitle2" fontWeight={600}>
            Back
          </Typography>
        </Button>

        <Button
          variant="outlined"
          style={{
            float: "right",
            padding: "6px 10px 7px 10px",
            marginLeft: "10px",
          }}
          color="error"
          onClick={() => {
            if (window.confirm("Are you sure you want to Rest?")) {
              setFetchData((prevData) => {
                return prevData.map((i) => {
                  return {
                    ...i,
                    status: 0,
                  };
                });
              });
            }
          }}
        >
          <Typography variant="subtitle2" fontWeight={600}>
            Rest All
          </Typography>
        </Button>
        <Button
          variant="outlined"
          style={{ float: "right" }}
          color="error"
          onClick={() => {
            if (window.confirm("Are you sure you want to delete?")) {
              handleDelete();
            }
          }}
        >
          <Typography variant="subtitle2" fontWeight={600}>
            <Delete /> Delete
          </Typography>
        </Button>
        <Box sx={{ overflow: "auto", width: { xs: "280px", sm: "auto" } }}>
          <Table
            aria-label="simple table"
            sx={{
              whiteSpace: "nowrap",
              mt: 1,
            }}
          >
            <TableHead>
              <TableRow>
                <TableCell
                  sx={{
                    width: "20px",
                  }}
                >
                  <Typography variant="subtitle1" fontWeight={600}>
                    ID
                  </Typography>
                </TableCell>
                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    Role Permission
                  </Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {fetchData.map((permission) => (
                <TableRow key={permission.id}>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      <Switch
                        defaultChecked
                        value={permission.id}
                        type="checkbox"
                        checked={permission.status}
                        onChange={() => {
                          setFetchData((prevData) => {
                            return prevData.map((i) => {
                              if (i.id === permission.id) {
                                return {
                                  ...i,
                                  status: i.status === 1 ? 0 : 1,
                                };
                              }
                              return i;
                            });
                          });
                        }}
                      />
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle2" fontWeight={600}>
                      {permission.name.toUpperCase()}
                    </Typography>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </DashboardCard>
    </>
  );
}

export default PopupRoles;
