import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import LockPersonOutlinedIcon from '@mui/icons-material/LockPersonOutlined';
import { Switch, Table, TableCell, TableRow, Typography } from "@mui/material";
import { fetchRolesById, fetchUpdatePermission } from "../core/Request";
import { toast } from "react-toastify";

export default function AlertUpdatePermission({ id }) {
    const [open, setOpen] = React.useState(false);
    const [fetchData, setFetchData] = React.useState([]);
    const [updateData, setUpdateData] = React.useState([]);

    const handleClickOpen = async () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    React.useEffect(() => {
        if (!open) {
            return;
        }
        const fetchRoles = async () => {
            try {
                const data = await fetchRolesById(id);
                setFetchData(data.data.data.permissions);
            } catch (error) {
                console.error("Failed to fetch roles:", error);
            }
        };
        fetchRoles();
    }, [id, open]);

    React.useEffect(() => {
        setUpdateData(
            fetchData?.map((item) => {
                return {
                    permission_id: item.id,
                    status: item.status,
                };
            })
        );
    }, [fetchData]);

    const updatePermissionStatus = (id) => {
        setFetchData((prevData) => {
            return prevData.map((i) => {
                if (i.id === id) {
                    return {
                        ...i,
                        status: i.status === 1 ? 0 : 1,
                    };
                }
                return i;
            });
        });
    };

    const handleUpdate = async () => {
        fetchUpdatePermission(id, updateData);
        setOpen(false);
        toast.success(`Role update successful`, {
            position: "top-center",
        });
    };
    return (
        <>
            <div
                onClick={handleClickOpen}
                className="ps-2"
                style={{ cursor: "pointer" }}
            >
                <LockPersonOutlinedIcon style={{color: "#4085ad"}}/>
            </div>
            <Dialog
                open={open}
                keepMounted
                onClose={handleClose}
                aria-describedby="alert-dialog-slide-description"
                style={{
                    textAlign: "center",
                    backgroundColor: "#fff !important",
                    color: "#000 !important",
                }}
            >
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                        <DialogTitle style={{ paddingTop: "20px" }}>
                            {"Update Role Permission"}
                            <Typography
                                variant="subtitle2"
                                fontWeight={600}
                            ></Typography>
                        </DialogTitle>
                        <Table
                            aria-label="simple table"
                            sx={{
                                whiteSpace: "nowrap",
                                mt: 2,
                            }}
                        >
                            {fetchData.map((permission) => (
                                <TableRow key={permission.id}>
                                    <TableCell>
                                        <Switch
                                            defaultChecked
                                            value={permission.id}
                                            type="checkbox"
                                            checked={permission.status}
                                            onChange={() =>
                                                updatePermissionStatus(
                                                    permission.id
                                                )
                                            }
                                        />
                                    </TableCell>
                                    <TableCell>
                                        <Typography
                                            variant="subtitle2"
                                            fontWeight={600}
                                        >
                                            {permission.name.toUpperCase()}
                                        </Typography>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </Table>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        style={{
                            backgroundColor: "#f1f1f1",
                            color: "",
                            marginRight: 3,
                        }}
                        onClick={handleClose}
                    >
                        Cancel
                    </Button>
                    <Button
                        style={{
                            backgroundColor: "#4085ad",
                            color: "#ffff",
                        }}
                        onClick={handleUpdate}
                    >
                        Update Permission
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}
