import { TableBody, TableCell, TableRow, Typography } from "@mui/material";
import AlertDeleteRole from "./AlertDeleteRole";
import AlertUpdatePermission from "./AlertUpdatePermission";
import dayjs from "dayjs";
import { useSelector } from "react-redux";

function DashboardBodyList({ currentRole, setRefresh }) {
  const { userPermission } = useSelector((state) => state.auth);
  return (
    <>
      <TableBody>
        {currentRole.map((role) => (
          <TableRow key={role.id}>
            <TableCell>
              <Typography variant="subtitle2" fontWeight={600}>
                {role.id}
              </Typography>
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2" fontWeight={600}>
                {role.name}
              </Typography>
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2" fontWeight={600}>
                {role.updateBy.name}
              </Typography>
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2" fontWeight={600}>
                {role.createdBy.name}
              </Typography>
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2" fontWeight={600}>
                {dayjs(role.createdDate).format("YYYY-MM-DD")}
              </Typography>
            </TableCell>
            <TableCell>
              <Typography variant="subtitle2" fontWeight={600}>
                {dayjs(role.updateDate).format("YYYY-MM-DD")}
              </Typography>
            </TableCell>

            {(userPermission?.find((per) => per.name === "edit-user")
              ?.status === 1 ||
              userPermission?.find((per) => per.name === "delete-user")
                ?.status === 1) && (
              <>
                <TableCell align="center" className="d-flex">
                  <AlertUpdatePermission id={role.id} />
                  <AlertDeleteRole id={role.id} setRefresh={setRefresh} />
                </TableCell>
              </>
            )}
          </TableRow>
        ))}
      </TableBody>
    </>
  );
}

export default DashboardBodyList;
