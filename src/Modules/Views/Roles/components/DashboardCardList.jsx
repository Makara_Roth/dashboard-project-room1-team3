import React, { useEffect, useState } from "react";
import useRole from "../core/Action";
import { Box, Table } from "@mui/material";
import DashboardHeadList from "./DashboardHeadList";
import DashboardBodyList from "./DashboardBodyList";
import AlertCreateRoles from "./AlertCreateRoles";
import { fetchRoles } from "../core/Request";
import { useSelector } from "react-redux";
import GlobalPagination from "../../../../Globals/helpers/components/Pagination";
import SearchField from "../../../../Globals/helpers/components/SearchField";

function DashboardCardList() {
  const { getRoles, handleSearch, getPaging, handleFilter } = useRole();
  const { currentRole } = useSelector((state) => state.role);
  const { userPermission } = useSelector((state) => state.auth);
  const [refresh, setRefresh] = useState(0);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [showDialog, setShowDialog] = useState(false);

  const { handlePage } = useRole();

  const { params, totalPage } = useSelector((state) => state.role);

  const { page } = params;

  const handleChange = (value) => {
    handlePage(value);
  };

  useEffect(() => {
    fetchRoles();
    getRoles();
    getPaging();
    // eslint-disable-next-line
  }, [refresh, params]);

  return (
    <>
      <div className="d-flex justify-content-between">
        <SearchField
          title="Search Name..."
          params={params}
          onChange={(e) => handleSearch(e.target.value)}
        />
        <div>
          {userPermission?.find((per) => per.name === "create-role")?.status ===
            1 && (
            <AlertCreateRoles
              setRefresh={setRefresh}
              setMessage={setMessage}
              setOpen={setOpen}
              setShowDialog={setShowDialog}
              open={open}
              message={message}
              showDialog={showDialog}
            />
          )}
        </div>
      </div>
      <Box sx={{ overflow: "auto", width: { xs: "280px", sm: "auto" } }}>
      <Table
        aria-label="simple table"
        sx={{
          whiteSpace: "nowrap",
          mt: 2,
        }}
      >
        <DashboardHeadList />
        <DashboardBodyList currentRole={currentRole} setRefresh={setRefresh} />
      </Table>
      <GlobalPagination
        params={params}
        totalPage={totalPage}
        page={page}
        handleChange={handleChange}
        handleFilter={handleFilter}
      />
    </Box>
    </>
  );
}

export default DashboardCardList;
