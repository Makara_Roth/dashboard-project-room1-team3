import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Typography,
} from "@mui/material";
import useRole from "../core/Action";
import { useSelector } from "react-redux";

function DashboardHeadList() {
  const { handleSort } = useRole();
  const {userPermission} = useSelector((state) => state.auth)
  const { sort, order } = useSelector((state) => state.role.params);

  return (
    <>
      <TableHead>
        <TableRow>
          <TableCell>
            <TableSortLabel
              active={sort === "id"}
              direction={order}
              onClick={() => handleSort("id")}
            >
              <Typography variant="subtitle2" fontWeight={600}>
                No
              </Typography>
            </TableSortLabel>
          </TableCell>
          <TableCell>
            <TableSortLabel
              active={sort === "name"}
              direction={order}
              onClick={() => handleSort("name")}
            >
              <Typography variant="subtitle2" fontWeight={600}>
                Roles
              </Typography>
            </TableSortLabel>
          </TableCell>
          <TableCell>
            <TableSortLabel
              active={sort === "createdBy"}
              direction={order}
              onClick={() => handleSort("createdBy")}
            >
              <Typography variant="subtitle2" fontWeight={600}>
                Create By
              </Typography>
            </TableSortLabel>
          </TableCell>
          <TableCell>
            <TableSortLabel
              active={sort === "updateBy"}
              direction={order}
              onClick={() => handleSort("updateBy")}
            >
              <Typography variant="subtitle2" fontWeight={600}>
                Update By
              </Typography>
            </TableSortLabel>
          </TableCell>
          <TableCell>
            <TableSortLabel
              active={sort === "createdDate"}
              direction={order}
              onClick={() => handleSort("createdDate")}
            >
              <Typography variant="subtitle2" fontWeight={600}>
                Created Date
              </Typography>
            </TableSortLabel>
          </TableCell>
          <TableCell>
            <TableSortLabel
              active={sort === "updateDate"}
              direction={order}
              onClick={() => handleSort("updateDate")}
            >
              <Typography variant="subtitle2" fontWeight={600}>
                Update Date
              </Typography>
            </TableSortLabel>
          </TableCell>
          {(userPermission?.find((per) => per.name === "edit-role")?.status ===
            1 ||
            userPermission?.find((per) => per.name === "delete-role")
              ?.status === 1) && (
            <TableCell align="center">
              <Typography variant="subtitle2" fontWeight={600}>
                Action
              </Typography>
            </TableCell>
          )}
        </TableRow>
      </TableHead>
    </>
  );
}

export default DashboardHeadList;
