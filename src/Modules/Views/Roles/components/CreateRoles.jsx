import React, { useEffect, useState } from "react";
import { Form, Spinner } from "react-bootstrap";
import DashboardCard from "../../../Components/shared/DashboardCard";
import { Button } from "@mui/material";
import useRole from "../core/Action";
import DeleteRoleDialog from "./DeleteRoleDialog";

const CreateRoles = () => {
  const [name, setRoleName] = useState("");
  const [code, setRoleCode] = useState("");
  const { createRole } = useRole();
  const [loading, setLoading] = useState(true);
  const [showDialog, setShowDialog] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (name === "" || code === "") {
      alert("Please fill all the fields");
      return;
    }
    try {
      await createRole(name, code);
    } catch (error) {
      setErrorMessage(error.response.data.message);
      setShowDialog(true);
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoading(false);
    }, 500);
    return () => clearTimeout(timer);
  }, []);

  return (
    <>
      {loading ? (
        <div className="text-center">
          <Spinner
            className="text-primary"
            style={{ width: "3rem", height: "3rem" }}
          >
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      ) : (
        <>
        <Typography
            id="modal-modal-title"
            variant="h4"
            component="h2"
            style={{ textAlign: "center" }}
          >
            Create Roles
          </Typography>
          <DashboardCard>
            <Button className="mb-3" variant="outlined">
              Back
            </Button>
            <Form onSubmit={handleSubmit}>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label> Name : </Form.Label>
                <Form.Control
                  type="text"
                  autoFocus
                  value={name}
                  onChange={(e) => setRoleName(e.target.value)}
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlTextarea1"
              >
                <Form.Label>Code : </Form.Label>
                <Form.Control
                  type="text"
                  rows={3}
                  value={code}
                  onChange={(e) => setRoleCode(e.target.value)}
                />
              </Form.Group>
              <Button variant="contained" type="submit">
                Submit
              </Button>
            </Form>
          </DashboardCard>
        </>
      )}
      {/* ... other components ... */}
      {showDialog && (
        <DeleteRoleDialog message={`${errorMessage} ! Please try again`} />
      )}
    </>
  );
};

export default CreateRoles;
