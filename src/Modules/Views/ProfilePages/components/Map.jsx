// src/EmbeddedMap.js
import React from 'react';
import { Box } from '@mui/material';

const EmbeddedMap = ({ latitude, longitude }) => {

  const mapUrl = `https://maps.google.com/maps?q=${latitude},${longitude}&z=15&output=embed`;

  console.log(mapUrl);

  return (
    <Box>
      <iframe
        title="Google Map"
        width="600"
        height="450"
        frameBorder="0"
        style={{ border: 0 }}
        src={mapUrl}
        allowFullScreen
      ></iframe>
    </Box>
  );
};

export default EmbeddedMap;
