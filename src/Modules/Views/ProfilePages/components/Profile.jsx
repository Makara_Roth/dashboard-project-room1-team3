import "../../../../Globals/Profile/Profile.css";

import { Grid, Box, } from '@mui/material';
import { useEffect } from "react";
import useProfile from "../core/Action";
import { useSelector } from "react-redux";
import ProfileCard from './ProfileCard';
import PerformanceChart from "./PerformanceChart";
import { makeStyles } from '@mui/styles';
import CreateAddress from "./CreateAddress";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));
const Profile = () => {
  const {
    userProfile,
    addresses,
    message,
  } = useSelector((state) => state.profile);
  const { getProfile } = useProfile();
  const classes = useStyles();


  useEffect(() => {
    getProfile();

    // eslint-disable-next-line
  }, [message]);

  useEffect(() => {
    if (!addresses) {
      console.log("Not exiting");
    } else {
      console.log("exiting");
    }
  }, [addresses])

  return (
    <>
      <Box className={classes.root}>
        <Grid container spacing={2}>
          {/* Profile Card */}
          <Grid item xs={12} lg={4}>
            <ProfileCard userProfile={userProfile} address={addresses} />
          </Grid>

          {/* Performance Chart */}
          <Grid item xs={12} lg={8}>
            <PerformanceChart userData={userProfile} />
          </Grid>

          {/* Social Traffic Table */}
          <Grid item xs={12} lg={8}>
            {/* <CreateAddress/> */}
          </Grid>

          {/* Stats Cards */}
          {/* <Grid item xs={12} lg={4}>
            <StatsCard title="Followers" value="2,356" icon={<i className="fa fa-chart-pie"></i>} percentChange="3.48" since="month" />
            <StatsCard title="Likes" value="10,442" icon={<i className="fa fa-check"></i>} percentChange="30" since="month" />
            <StatsCard title="Messages" value="140" icon={<i className="fa fa-inbox"></i>} percentChange="5.8" since="week" />
          </Grid> */}
        </Grid>
      </Box>
    </>
  );
};
export default Profile;
