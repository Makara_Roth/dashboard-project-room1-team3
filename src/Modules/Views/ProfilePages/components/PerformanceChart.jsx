import React, { useRef, useEffect } from 'react';
import { Chart, BarElement , CategoryScale , LinearScale } from 'chart.js'

import { Bar } from "react-chartjs-2";

Chart.register(BarElement , CategoryScale, LinearScale);

const PerformanceChart = ({ userData }) => {

  const chartRef = useRef(null); // Reference to the chart canvas element

  const status = userData?.orders?.map(item => item?.status) ?? [];
  
  // Counting the number of each status
  const statusCounts = status?.reduce((acc, curr) => {
    acc[curr] ? acc[curr]++ : (acc[curr] = 1);
    return acc;
  }, {});

  // Extracting status labels and counts for the chart
  const labels = Object?.keys(statusCounts);
  const counts = Object?.values(statusCounts);

  const data = {
    labels: labels,
    datasets: [{
      label: "Totals Ordered",
      data: counts,
      backgroundColor: [
        'rgba(255, 99, 132, 0.2)',
        'rgba(255, 159, 64, 0.2)',
        'rgba(255, 205, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(201, 203, 207, 0.2)'
      ],
      borderColor: [
        'rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(255, 205, 86)',
        'rgb(75, 192, 192)',
        'rgb(54, 162, 235)',
        'rgb(153, 102, 255)',
        'rgb(201, 203, 207)'
      ],
      borderWidth: 1
    }]
  };

  // Chart options
  const options = {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  };

  useEffect(() => {
    // Destroy existing chart instance before rendering a new chart
    if (chartRef && chartRef?.current) {
      const chartInstance = chartRef?.current?.chartInstance;
      if (chartInstance) {
        chartInstance?.destroy();
      }
    }
  }, [userData]); // Re-run effect when userData changes

  return <Bar ref={chartRef} data={data} options={options} />;
};

export default PerformanceChart;
