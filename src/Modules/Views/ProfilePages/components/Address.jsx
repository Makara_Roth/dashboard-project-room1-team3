import React from 'react'
import { Box } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';


function Address({show}) {

  return (
  <Box sx={{
    display: 'inline-block',
    cursor: 'pointer',
  }}  
>
    <EditIcon sx={{
                    fontSize: 20,
                    marginLeft: 2,
                    color: 'primary.main',
                    cursor: 'pointer',
                    '&:hover': {
                      color: 'primary.dark',
                    },
                  }}
     /> 
  </Box>
  )
}

export default Address