
import { Card, CardContent, Grid, Typography } from '@mui/material';
const StatsCard = ({ title, value, icon, percentChange, since }) => {
    return (
      <Card>
        <CardContent>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <Typography variant="body1">{title}</Typography>
              <Typography variant="h4">{value}</Typography>
            </Grid>
            <Grid item>
              <div>
                {icon}
              </div>
            </Grid>
          </Grid>
          <Typography variant="body2">
            <span style={{ color: 'green' }}><i className="fa fa-arrow-up"></i> {percentChange}%</span>
            <span>Since last {since}</span>
          </Typography>
        </CardContent>
      </Card>
    );
  }
export default StatsCard;