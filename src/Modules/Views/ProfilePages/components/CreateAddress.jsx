
import React from 'react'
import { Box, TextField, Button, Typography, Container } from '@mui/material';
import { useState } from 'react';
import EmbeddedMap from './Map';


function CreateAddress() {

  const [location, setLocation] = useState({ latitude: '11.5591256', longitude: '104.8811755' });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setLocation({
      ...location,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <>
      <Container>
      <Typography variant="h1" component="h1" gutterBottom>
      Google Maps 
      </Typography>
      <Box component="form" onSubmit={handleSubmit} noValidate autoComplete="off">
        <Box mb={2}>
          <TextField
            fullWidth
            label="Latitude"
            variant="outlined"
            name="latitude"
            value={location.latitude}
            onChange={handleInputChange}
          />
        </Box>
        <Box mb={2}>
          <TextField
            fullWidth
            label="Longitude"
            variant="outlined"
            name="longitude"
            value={location.longitude}
            onChange={handleInputChange}
          />
        </Box>
        <Button variant="contained" color="primary" type="submit">
          Show Location
        </Button>
      </Box>
      {location.latitude && location.longitude && (
        <EmbeddedMap latitude={location.latitude} longitude={location.longitude} />
      )}
    </Container>
    </>
  )
}

export default CreateAddress