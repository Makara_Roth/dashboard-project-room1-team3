import {TableHead, Card, CardHeader, CardContent, Button,
     Table, TableRow, TableCell, TableBody, CircularProgress, Typography } from '@mui/material';


const  SocialTrafficTable = () => {
    return (
      <Card>
        <CardHeader
          title="Social Traffic"
          action={
            <Button size="small" variant="contained" color="primary">See all</Button>
          }
        />
        <CardContent>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Referral</TableCell>
                <TableCell>Visitors</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>Facebook</TableCell>
                <TableCell>1,480</TableCell>
                <TableCell>
                  <div>
                    <CircularProgress variant="determinate" value={60} />
                    <Typography variant="body2">60%</Typography>
                  </div>
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </CardContent>
      </Card>
    );
  }

  export default SocialTrafficTable;