import { Card, CardHeader, CardContent, Typography, Avatar, Box } from '@mui/material';
import EmailIcon from '@mui/icons-material/Email';
import DateRangeIcon from '@mui/icons-material/DateRange';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import MyLocationIcon from '@mui/icons-material/MyLocation';
import './ProfileCard.css';
import Address from './Address';

function ProfileCard({ userProfile , address }) {

  return (
      <Card className={"card"}>
          <Box className={'mt-3'}>
              <Avatar
                  src={userProfile?.avatar || "https://conflictresolutionmn.org/wp-content/uploads/2020/01/flat-business-man-user-profile-avatar-icon-vector-4333097-600x648.jpg"}
                  alt={userProfile?.avatar || "User Avatar"}
                  sx={{width: 100, height: 100}}
              />
          </Box>
          <CardHeader
              title={userProfile.name}
              subheader={userProfile.email}
              className={"cardheader"}
          />
              {/*<div className={'d-flex justify-content-around mb-0 '}>*/}
              {/*    <Button variant="contained" color="primary" size="small">*/}
              {/*        Contact*/}
              {/*    </Button>*/}
              {/*    <Button variant="contained" color="primary" size="small">*/}
              {/*        Message*/}
              {/*    </Button>*/}
              {/*</div>*/}
             <Box className={'mt-2'}>
                 <Typography variant="body1"><EmailIcon/> : { userProfile.email}</Typography>
                 <Typography variant="body1"><DateRangeIcon/> : {userProfile.hireDate} </Typography>
                 <Typography variant="body1"><LocalPhoneIcon/>: { userProfile.phone} </Typography>
                 <Typography variant="body1" ><MyLocationIcon/> : {address?.location} , {address?.name} 
                    <Address />
                 </Typography>
             </Box>
      </Card>
  );
}

export default ProfileCard;
