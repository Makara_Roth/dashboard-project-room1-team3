import { reqUserProfile , reqAddressByToken } from "./Request";
import { useDispatch } from "react-redux";


import {
  setUserProfile,
  setAddresses,
  setMessage
} from "./Reducer";

const useProfile =  () => {
  const dispatch = useDispatch();

  const getProfile = async () => {
   await reqUserProfile().then((response) => {
      SetUserProfile(response.data.data);
    });
  };

  const SetUserProfile = (user) => {
    dispatch(setUserProfile(user));
  };

  const ClearData = () => {
    SetUserProfile();
    dispatch(setAddresses({}));
  };

  // const UpdateStatus = async ( id ) => {
  //   await fetchUserProfile(id).then((response) => {
  //     SetUserProfile(response.data.data);
  //   });
  // };

  const getAddessByToken =  async () =>{
    try{
      const response = await reqAddressByToken()
      dispatch(setAddresses(response?.data?.data));
    }catch(e){

      if (!e?.response) {
        dispatch(setMessage("No Server Response"));
      } else if (e.response?.status === 400) {
        dispatch(setMessage(e.response.data.message));
      } else if (e.response?.status === 401) {
        dispatch(setMessage("Unauthorized"));
      }
    }
  }
  const createAddressByToken = async({value}) =>{
    try{
      const response = await reqAddressByToken({value})
      console.log(response.message);
    }catch(e){

      if (!e?.response) {
        dispatch(setMessage("No Server Response"));
      } else if (e.response?.status === 400) {
        dispatch(setMessage(e.response.data.message));
      } else if (e.response?.status === 401) {
        dispatch(setMessage("Unauthorized"));
      }
    }
  }

  return {
    getProfile,
    ClearData,
    SetUserProfile,
    // UpdateStatus,
    getAddessByToken,
    createAddressByToken
  };
};
export default useProfile;

