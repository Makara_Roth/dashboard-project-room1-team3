import axios from "axios";
import { getAuth } from "../../../Auth/helper/authHelper";

const reqUserProfile = () => {
  return axios.get("/api/user/profile", {
    headers: {
      Authorization: `Bearer ${getAuth()}`,
    },
  });
};
const reqUpdateStatus = (status, id) => {
  return axios.patch(`/api/user/${id}/profile`, {
  });
};
const reqAddressByToken = () => {
  return axios.get("/api/address/current")
}
const reqCreateAddressByToken = ({ value }) => {
  return axios.post("/api/address", value)
}


export { reqUserProfile, reqUpdateStatus, reqAddressByToken, reqCreateAddressByToken };
