import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userProfile: {},
  addresses: undefined,
  status: true,
  message: "",
  newAddress: {
    name: "",
    location: "",
    googleMapUrl: "",
  },
};

const profileSlice = createSlice({
  name: "profile",
  initialState: initialState,
  reducers: {
    setUserProfile: (state, action) => {
      state.userProfile = action.payload;
    },
    setAddresses: (state, action) => {
      state.addresses = action.payload;
    },
    setStatus: (state, action) => {
      state.status = action.payload;
    },
    setMessage: (state, action) => {
      state.message = action.payload;
    },
  },
});
export const { setUserProfile, setAddresses, setStatus, setMessage } =
  profileSlice.actions;

export default profileSlice.reducer;
