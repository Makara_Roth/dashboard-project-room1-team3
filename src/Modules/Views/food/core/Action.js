import {
    reqCreateFood, reqFood, reqFoodById, reqCategories, reqUpdateFood, reqDeleteFoodById,
    reqUploadFoodImages, reqDeleteFoodImage, reqFoodByCategory, reqEditFoodImage
} from "./Request";
import {
    setErrorMessage, setFood, setFoodCategory, setPaging,
    setFoodById, setParams, setSupportState
} from "./Reducer";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

// import { setFood, setFoodCategory, setErrorMessage } from "./Reducer";

const useFood = () => {
    const dispatch = useDispatch()
    const { params, sortByCategory } = useSelector((state) => state.food);


    const { sortBy } = params;

    // ===============  Fetch Food  ================
    const getFood = async (params) => {
        try {
            await reqFood(params).then((response) => {
                const food = response.data.data
                const paging = response.data.paging
                dispatch(setFood(food))
                dispatch(setPaging(paging))

            });
        } catch (error) {
            console.error('Error adding object:', error);
        }
    };
    // ===============  Fetch Food by Id     ===================

    const getFoodById = async (id) => {
        try {
            await reqFoodById(id).then((response) => {
                dispatch(setFoodById(response.data.data));
            });
        } catch (error) {
            console.error('Error adding object:', error);
        }
    };
    // =============== Fetch Food By  Categories ================
    const getFoodByCategory = async () => {
        try {
            const response = await reqFoodByCategory(sortByCategory)
            dispatch(setFood(response.data.data))
        } catch (error) {
            console.error('Error fetching food by')
        }
    }
    // =============  Edit Food   =================

    const foodEdit = async (id, editFood) => {
        try {
            const response = await reqUpdateFood(id, editFood)
            toast.success(`Food updated ${response.data.message} `, {
                position: "top-center",
            })
        } catch (error) {
            toast.error(`error.data.message`, {
                position: "top-center",
            });
        }
    }

    // ========  Add Food   ===================

    const addFood = async ({ ...newFood }) => {
        try {
            await reqCreateFood({ ...newFood }).then((response) => {
                toast.success(`Food added ${response.data.message}`)
                dispatch(setErrorMessage(response.data.message))
            });
        } catch (error) {
            console.error("Error adding object:", error);
        }
    };
    const deleteFood = async (id) => {
        try {
            await reqDeleteFoodById(id).then((res) => {
                dispatch(setErrorMessage(res.data.data))
                toast.success(`${res.data.data}`);
            })
        } catch (error) {
            dispatch(setErrorMessage(error.response.data))
            toast.error(`error.data.message`, {
                position: "top-center",
            });
        }

    }
    // upload foodImage to server only create new food
    const uploadFoodImage = async (formData) => {
        try {
            await reqUploadFoodImages(formData).then((res) => {
                toast.success(`Image uploaded  ${res.data.message}`);
                dispatch(setErrorMessage(res.data.data))
            })
        } catch (error) {
            console.log(error.response);
            toast.error(`error.data.message`);
        }
    }
    const editFoodImage = async ( foodId , formData) => {
        try {
            await reqEditFoodImage( foodId ,formData).then((res) => {
                toast.success(` Update Food Image ${res.data.message}`);
                dispatch(setErrorMessage(res.data.data))
            })
        } catch (error) {
            console.log(error.response);
            toast.error(`error.data.message`);
        }
    }
    // Delete Food Image By Id Food 
    const deleteFoodImage = async (foodId) => {
        try {
            await reqDeleteFoodImage(foodId).then((res) => {
                dispatch(setErrorMessage(res.data.data))
                toast.success(`Delete Food Image ${res.data.message}`);
            })
        } catch (error) {
            console.log(error.response);
            toast.error(`error.data.message`, {
                position: "top-center",
            });
        }
    }
    const getFoodCategories = () => {
        try {
            reqCategories().then((res) => {
                dispatch(setFoodCategory(res.data.data));
            });
        } catch (err) {
            dispatch(setErrorMessage(err));
        }
    };
    const handleSearch = (text) => {
        dispatch(setParams({ page: 1, query: text }));
    };
    const handleSort = (field) => {
        if (sortBy === field) {
            dispatch(setParams({ order: sortBy === "asc" }));
        } else {
            dispatch(setParams({ sort: field, order: "asc" }));
        }
    };
    const handleFilter = (name, value) => {
        dispatch(setParams({ [name]: value }));
      }
    
    const handleSortByCategory = (field) => {
        dispatch(setSupportState({ sortByCategory: field }));
    };
    const handlePageChange = (page) => {
        dispatch(setParams({ page: page }));
    };


    return {
        getFood, addFood,
        foodEdit, getFoodCategories,
        deleteFood, uploadFoodImage,
        getFoodById, handleSearch,
        handleSort, handlePageChange,
        handleSortByCategory, deleteFoodImage,
        getFoodByCategory, editFoodImage ,
        handleFilter
    };
};

export default useFood;
