import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    foodCategory: [],
    food: [] ,
    foodById: {},
    errorMessage: "",
    imageEnties: [],
    params:{
        query:"",
        sortBy:"name",
        size:12,
        page:1,
        order:"asc",
    },
    supportState:{
        newFood: {},
        deleteId: null,
        loading : false,
        showModleCreateFood : false,
        showModleUpdateFoodImage : false,
        deleteImageFoodId : null,
        editImage:false,
        category: "All",
        errors:{
            name: "",
            code: "",
            price: "",
        },
    },
    sortByCategory: {
        filters : "categories",
        nameType: "Cambodian"
    },
    foodPaging: 0,
}

const foodSlide = createSlice({
    name: "food",
    initialState,
    reducers: {
        setFoodCategory: (state, action) => {
            state.foodCategory = action.payload;
        },
        setFood: (state, action) => {
            state.food = action.payload;
        },
        setErrorMessage: (state, action) => {
            state.errorMessage = action.payload;
        },
        setPaging:(state, action) => {
            state.foodPaging = action.payload;
        },
        setImageEnties: (state, action) => {
            state.imageEnties = action.payload;
        },
        // setPage: (state, action) => {
        //     state.page = action.payload;
        // },
        setFoodById: (state, action) => {
            state.foodById = action.payload;
        },
        setParams: (state, action) => {
            state.params = {
               ...state.params,
               ...action.payload
            }
        },
        setSupportState: (state, action) => {
            return {
                ...state,
                supportState: {
                  ...state.supportState,
                  ...action.payload
                }
            };
        },
        setSortByCategory: (state, action) => {
         return {
            ...state,
            sortByCategory: {
              ...state.sortByCategory,
              ...action.payload
            }
        };
        }
    },
});
export const {
    setFoodCategory, 
    setFood, 
    setErrorMessage , 
    setPaging ,
    setPage,
    setSupportState,
    setFoodById,
    setParams,
    setSortByCategory,
    } = foodSlide.actions;

export default foodSlide.reducer;
