import axios from "axios";

// get Food list
const reqFood = (params) => {
  return axios.get(`/api/foods`, {
    params : params })
};

// get food by id
const reqFoodById = (id) => {
  return axios.get(`/api/foods/${id}`);
};
const reqFoodByCategory = (params) => {
  return axios.get("/api/foods/list" , {
    params : params
  });
}
// create a new food
const reqCreateFood = ({
  ...newFood
}) => {
  return axios.post(
    "/api/foods",
    {
      ...newFood
    }
  );
};

// update Food by id
const reqUpdateFood = ( editFood ) => {
  return axios.put(
    `/api/foods/${editFood.id}`, editFood
  );
};

// delete food by id
const reqDeleteFoodById = (id) => {
  return axios.delete(
    `/api/foods/${id}`,
  );
};

// upload foodImage to server 
const reqUploadFoodImages = (fromData) => {
  return axios.post("/api/food/images", fromData
  );
}
const reqEditFoodImage = ( foodId , fromData) => {
  return axios.put(
    `/api/food/images/${foodId}`, fromData
  );
}
const reqDeleteFoodImage = ( foodId ) => {
  return axios.delete(
    `/api/food/images/${foodId}`,
  );
}
// get Food Category
const reqCategories = () => {
  return axios.get("/api/categories");
};

export {
  reqFood,
  reqFoodById,
  reqFoodByCategory,
  reqCreateFood,
  reqUpdateFood,
  reqDeleteFoodById,
  reqCategories,
  reqUploadFoodImages,
  reqDeleteFoodImage,
  reqEditFoodImage
};
