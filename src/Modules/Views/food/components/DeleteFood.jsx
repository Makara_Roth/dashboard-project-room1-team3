import React from 'react'
import { Box, Button, Modal, Typography } from "@mui/material";
import { setErrorMessage, setSupportState } from "../core/Reducer";

import useFood from '../core/Action';
import { useDispatch } from 'react-redux';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};


function DeleteFood({ handleClose, foodId}) {
    const dispatch = useDispatch()
    const { deleteFood } = useFood()


    const confirmDelete = async (e) => {
        e.preventDefault();
            await deleteFood(foodId)
            dispatch(setErrorMessage(""))
            dispatch(setSupportState({ deleteId: "" }))
    }

    return (
        <>
            <Modal
                open={true}
                onClose={handleClose}
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2" style={{ textAlign: "center" }}>
                        Are you sure want to delete this user?
                    </Typography>
                    <div className="mt-4 text-end">
                        <Button style={{ backgroundColor: "#4085ad", color: "#ffff", marginRight: 3 }} onClick={() => dispatch(setSupportState({deleteId: null }))}>Cancel</Button>
                        <Button variant="contained" color="error" onClick={confirmDelete}>Delete</Button>
                    </div>
                </Box>
            </Modal>
        </>
    )
}

export default DeleteFood