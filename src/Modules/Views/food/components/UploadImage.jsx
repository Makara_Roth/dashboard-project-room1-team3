import React, { useState } from 'react'
import '../../../../Globals/Food/uploader.css'
import useFood from '../core/Action';
import { Box, Typography, Dialog, DialogContent, DialogActions, Button  } from '@mui/material';
import { MdCloudUpload, MdDelete } from 'react-icons/md';
import { toast } from 'react-toastify';


function UploadImage({ history , foodData , handleClose , show }) {
  const { uploadFoodImage } = useFood()
  const [fileImages, setFileImages] = useState([])
  const MAX_FILE_LIMIT = 1;

  const handleImageChange = async (e) => {
    if (e.target.files) {
      const newFiles = Array.from(e.target.files);
      if (fileImages.length + newFiles.length > MAX_FILE_LIMIT) {
        alert(`You can upload a maximum of ${MAX_FILE_LIMIT} files.`);
        return;
      }

      // Add Modify File Type 
      const modifyFilesType = await Promise.all(
        newFiles.map(async (file) => {
          const fileUrl = await new Promise((resolve) => {
            const reader = new FileReader();
            reader.onload = (e) => resolve(e.target.result);
            reader.readAsDataURL(file);
          });
          return {
            file,
            fileType: 'image/jpeg',
            previewUrl: fileUrl,
          };
        })
      );

      setFileImages([...modifyFilesType]);
      const newImages = [...newFiles];
      const formData = new FormData();

      // Add images to form data
      newImages.forEach((fileData, index) => {
        formData.append(`files`, fileData);
      });
      formData.append('foodId', foodData.id);
        uploadFoodImage(formData)
        handleClose();
    }
  }

  const handleDeleteImage = (index) => {
    setFileImages(fileImages.filter((_, i) => i !== index));
  };

  return (
    <>
       <Dialog open={show} onClose={handleClose} fullWidth maxWidth="sm">
            <Box sx={{p:3}}>
                <Typography variant="h4" component="h2" sx={{ textAlign: "left" }}>
                    Upload image
                </Typography>
            </Box>
            <DialogContent>
                <main>
                    <div className="container">
                        <div className="d-flex justify-content-center">
                            <div className=" fileUpload bg-white ">
                                <label
                                    htmlFor="user-profile"
                                    className="d-flex align-items-center rounded-pill border border-secondary px-4 py-2 bg-white text-secondary"
                                >
                                    <MdCloudUpload color='#1475cf' size={60} className='me-2' />
                                    <span className="mt-2">Browse Files to upload</span>
                                </label>
                                <input type="file" className="d-none" id="user-profile"
                                    onChange={(e) => handleImageChange(e)} multiple />
                            </div>
                        </div>
                    </div>
                    <section className='uploaded-row d-flex justify-content-center '>
                        {fileImages.map((fileData, index) => (
                            <div key={index} className='align-items-center '>
                                <div className='me-2 my-2 ms-2 upload-content  '>
                                    <img src={fileData.previewUrl} alt="" style={{ width: '50px', height: '50px' }} className='me-2' />
                                    {fileData.file.name} - {/* Display file name */}
                                    <MdDelete onClick={() => handleDeleteImage(index)} className='me-2 fs-3' />
                                </div>
                            </div>
                        ))}
                    </section>
                </main>
            </DialogContent>
            <DialogActions>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    </>
  )
}

export default UploadImage 