import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AddIcon from "@mui/icons-material/Add";
import { IoMenu } from "react-icons/io5";
import { setParams, setSupportState, setSortByCategory } from "../core/Reducer";
import {
  Tooltip,
  IconButton,
  FormControl,
  Select,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import { Menu, MenuItem, Checkbox, FormControlLabel, Box } from "@mui/material";
// Component

import useFood from "../core/Action";
import AddFoodForm from "./AddFoodForm";
import DeleteFoodImage from "./DeleteFoodImage";
import UploadImage from "./UploadImage";
import DeleteFood from "./DeleteFood";
import EditFoodForm from "./EditFoodForm";
import CardFood from "./CardFood";
import EditFoodImage from "./EditFoodImage";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";
import MySpinner from "../../../../Globals/helpers/components/MySpinner";
import GlobalPagination from "../../../../Globals/helpers/components/Pagination";
import SearchField from "../../../../Globals/helpers/components/SearchField";
import useOrder from "../../Order/core/Action";

const Food = () => {
  const dispatch = useDispatch();
  const {
    getFood,
    getFoodCategories,
    handlePageChange,
    getFoodByCategory,
    handleSearch,
    handleFilter,
  } = useFood();
  const { handleAddToCart } = useOrder();
  const { cartTotalQuantity } = useSelector(
    (state) => state.combinedOrder.cart
  );
  const { food, params, foodPaging, errorMessage, foodCategory } = useSelector(
    (state) => state.food
  );
  const {
    deleteId,
    loading,
    showModleCreateFood,
    showModleUpdateFoodImage,
    deleteImageFoodId,
    category,
    editImage,
  } = useSelector((state) => state.food.supportState); // Extract from supportState
  const { sortByCategory } = useSelector((state) => state.food);
  const { page, order, sortBy } = params;
  const [foodData, setFoodData] = useState(null);
  const [foodDataForUploadImage, setFoodDataForUploadImage] = useState(null);
  const [foodDataForEditImage, setFoodDataForEditImage] = useState(null);

  useEffect(() => {
    getFoodCategories();
    category === "All" ? getFood(params) : getFoodByCategory(sortByCategory);

    // eslint-disable-next-line
  }, [category, sortByCategory, params, foodData, dispatch, errorMessage]);

  const [anchorEl, setAnchorEl] = useState(null);

  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleCreateFood = (e) => {
    e.preventDefault();
    dispatch(setSupportState({ showModleCreateFood: true }));
  };

  // console.log(showModleCreateFood);

  const handleDelete = (foodId) => {
    dispatch(setSupportState({ deleteId: foodId }));
  };

  const onEditHandler = (foodData) => {
    setFoodData(foodData);
  };

  const onHandleUploadImage = (foodData) => {
    setFoodDataForUploadImage(foodData);
    dispatch(setSupportState({ showModleUpdateFoodImage: true }));
  };

  const onHandleEditImage = (foodId) => {
    dispatch(setSupportState({ editImage: true }));
    setFoodDataForEditImage(foodId);
  };

  const onHandleDeleteImage = (foodId) => {
    dispatch(setSupportState({ deleteImageFoodId: foodId }));
  };

  const handleCategoryChange = (e) => {
    const selectedCategory = e.target.value;
    dispatch(setSupportState({ category: selectedCategory }));
    dispatch(setSortByCategory({ nameType: selectedCategory }));
  };
  const handleChange = (event, value) => {
    handlePageChange(value);
  };

  return (
    <>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Box>
              <Typography variant="h2" component="h2" className="fw-bold">
                Foods
              </Typography>
              <Breadcrumb title="Dashboard" subtitle="food" />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "start",
                mt: 2,
              }}
            >
              <Box sx={{ display: "inline-block", mr: 2 }}>
                <IconButton onClick={handleMenuClick} color="secondary">
                  <IoMenu className="bag-icon" />{" "}
                  <Typography
                    variant="h2"
                    component="h2"
                    sx={{ fontSize: "16px" }}
                  >
                    Sort
                  </Typography>
                </IconButton>
                <Menu
                  id="dropdown-basic-button"
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={handleMenuClose}
                >
                  <MenuItem>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={sortBy === "name"}
                          onChange={(e) =>
                            dispatch(
                              setParams({
                                sortBy: e.target.checked ? "name" : "",
                              })
                            )
                          }
                        />
                      }
                      label="Sort By Name"
                    />
                  </MenuItem>
                  <MenuItem>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={sortBy === "code"}
                          onChange={(e) =>
                            dispatch(
                              setParams({
                                sortBy: e.target.checked ? "code" : "",
                              })
                            )
                          }
                        />
                      }
                      label="Sort By Code"
                    />
                  </MenuItem>
                  <MenuItem>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={order === "desc"}
                          onChange={(e) =>
                            dispatch(
                              setParams({
                                order: e.target.checked ? "desc" : "asc",
                              })
                            )
                          }
                        />
                      }
                      label="Sort By Order"
                    />
                  </MenuItem>
                </Menu>
              </Box>
              <Box sx={{ display: "inline-block", mr: 5 }}>
                <FormControl variant="outlined" size="small">
                  <Select
                    defaultValue="All"
                    onChange={handleCategoryChange}
                    sx={{ width: 200 }} // Adjust the width as needed
                  >
                    <MenuItem value="All">Show All</MenuItem>
                    {foodCategory.map((item, index) => (
                      <MenuItem
                        value={item.name}
                        key={index}
                        sx={{
                          overflow: "hidden",
                          textOverflow: "ellipsis",
                          whiteSpace: "nowrap",
                        }}
                      >
                        {item.name.length > 20
                          ? `${item.name.substring(0, 20)}...`
                          : item.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
              <Box sx={{ display: "inline-block", mr: 2 }}>
                <Tooltip title="Create New Food">
                  <IconButton
                    sx={{
                      bgcolor: "primary.main",
                      borderRadius: 2,
                      color: "common.white",
                      "&:hover": {
                        bgcolor: "primary.dark", // Maintain the hover background color
                        color: "common.white", // Maintain the hover text color
                      },
                    }}
                    onClick={handleCreateFood}
                  >
                    <AddIcon />{" "}
                    <Typography sx={{ fontSize: "10" }}> Add Food </Typography>
                  </IconButton>
                </Tooltip>
              </Box>
              <AddFoodForm
                show={showModleCreateFood}
                handleClose={() => {
                  dispatch(setSupportState({ showModleCreateFood: false }));
                  dispatch(
                    setSupportState({
                      errors: { name: "", code: "", price: "" },
                    })
                  );
                }}
              />
            </Box>
          </Grid>
        </Grid>
      </Container>
      <Container>
        <Box mt={3}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Box display="flex">
                <SearchField
                  title="Search Food..."
                  params={params}
                  onChange={(e) => handleSearch(e.target.value)}
                />
              </Box>
              <Box
                display="flex"
                justifyContent="flex-end"
                onClick={() => dispatch(setParams({ query: "" }))}
                sx={{ cursor: "pointer" }}
              >
                <Typography variant="body1" className="fw-bold" px={3}>
                  Totals Food : {foodPaging.totals}
                </Typography>
              </Box>
            </Grid>
            {loading ? (
              <Grid item xs={12}>
                <MySpinner />
              </Grid>
            ) : (
              food.map((item, index) => (
                <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
                  <CardFood
                    id={item.id}
                    name={item.name}
                    price={item.price}
                    discount={item.discount}
                    foodCategoryEntity={item.foodCategoryEntity.name}
                    foodImage={item.foodImage}
                    onEdit={() => onEditHandler(item)}
                    onDelete={() => handleDelete(item.id)}
                    onUploadImage={() => onHandleUploadImage(item)}
                    onDeleteFoodImage={() => onHandleDeleteImage(item.id)}
                    onEditImage={() => onHandleEditImage(item.id)}
                    handleAddToCart={() => handleAddToCart(item)}
                  />
                </Grid>
              ))
            )}
          </Grid>
        </Box>

        {foodData && (
          <EditFoodForm
            handleClose={() => setFoodData(null)}
            foodData={foodData}
          />
        )}

        {showModleUpdateFoodImage && (
          <UploadImage
            show={showModleUpdateFoodImage}
            handleClose={() =>
              dispatch(setSupportState({ showModleUpdateFoodImage: false }))
            }
            foodData={foodDataForUploadImage}
          />
        )}

        {editImage && (
          <EditFoodImage
            handleClose={() => dispatch(setSupportState({ editImage: null }))}
            foodId={foodDataForEditImage}
          />
        )}

        {deleteImageFoodId && (
          <DeleteFoodImage
            handleClose={() =>
              dispatch(setSupportState({ deleteImageFoodId: null }))
            }
            foodId={deleteImageFoodId}
          />
        )}

        <Box mt={4}>
          <GlobalPagination
            params={params}
            totalPage={foodPaging?.totalPage}
            page={page}
            handleChange={handleChange}
            handleFilter={handleFilter}
          />
        </Box>
      </Container>
      {/* // Delete Food By Id  */}
      {deleteId && (
        <DeleteFood
          foodId={deleteId}
          handleClose={() => dispatch(setSupportState({ deleteId: null }))}
        />
      )}
    </>
  );
};

export default Food;
