const validateForm = (formData) => {
  const errors = {
    name: "",
    code: "",
    price: "",
  };

  let isValid = true;

  if (!formData.name.trim()) {
    errors.name = "Name is required";
    isValid = false;
  }

  if (!formData.code.trim()) {
    errors.code = "Code is required";
    isValid = false;
  }

  if (!formData.price) {
    errors.price = "Price is required";
    isValid = false;
  } else if (isNaN(formData.price)) {
    errors.price = "Price must be a number";
    isValid = false;
  }

  if (!formData.food_categoryId) {
    errors.food_categoryId = "food_categoryId is required";
    isValid = false;
  }

  return { isValid, errors };
};

export default validateForm;
