import React, { useState } from "react";
import "../../../../Globals/Profile/Profile.css";
import "../../../../Globals/Food/Food.css";
import useFood from "../core/Action";
import { useSelector, useDispatch } from "react-redux";
import { Box, Button, Grid, InputLabel, MenuItem, Modal, Select, TextField, Typography, FormControl } from "@mui/material";
import { setSupportState } from "../core/Reducer";
import validateForm from "./validateForm";

const initialData = {
  name: "",
  code: "",
  price: undefined,
  discount: undefined,
  description: "",
  food_categoryId: ''
}

const AddFoodForm = ({ handleClose, show }) => {
  const dispatch = useDispatch();
  const { foodCategory } = useSelector(state => state.food)
  const { errors } = useSelector((state) => state.food.supportState); // Extract from supportState

  const { addFood } = useFood()
  const [newFood, setNewFood] = useState({ ...initialData });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewFood({ ...newFood, [name]: value });

    // Clear validation error when input changes
    dispatch(setSupportState({ errors: { ...errors, [name]: "" } }));

  };

  const handleSave = (e) => {
    e.preventDefault();
    // Form validation using the validation function
    const { isValid, errors: validationErrors } = validateForm(newFood);
    // Update errors state
    // setErrors(validationErrors);
    dispatch(setSupportState({ errors: validationErrors }));

    if (isValid) {
      // Form submission logic
      addFood({ ...newFood });
      setNewFood({ ...initialData });
      dispatch(setSupportState({ showModleCreateFood: false }));
    }
  };
const onCancel = () => {
  setNewFood({ ...initialData });
  handleClose()
  dispatch(setSupportState({ errors : { name:"" , code:"" , price:"" } }))
}

  return (
    <>
      <Modal open={show} onClose={handleClose}>
        <Box
          autoComplete="off"
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: '90%', // Adjusted width for smaller screens
            maxWidth: 430, // Maximum width for larger screens
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            borderRadius: "5px",
            // Responsive styles
            '@media (max-width: 600px)': { // xs and sm screens
              width: '90%', // Adjusted width
              maxWidth: 'none', // No maximum width
            },
            '@media (min-width: 600px) and (max-width: 960px)': { // md screens
              width: '80%', // Adjusted width
              maxWidth: 500, // Maximum width
            },
            '@media (min-width: 960px)': { // lg screens
              width: 600, // Fixed width
              maxWidth: 'none', // No maximum width
            },
          }}>
          <Typography
            // id="modal-modal-title"
            variant="h4"
            component="h2"
            style={{ textAlign: "left"}}
          >
            Add New Food
          </Typography>
          <div className="mt-4">
            <Grid container spacing={2}>
              <Grid item xs={12} md={8}>
                <TextField
                  fullWidth
                  label="Name"
                  name="name"
                  // id="outlined-textfield"
                  value={newFood.name}
                  onChange={handleInputChange}
                  error={Boolean(errors.name)}
                  helperText={errors.name}
                  required
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <FormControl fullWidth >
                  <InputLabel id="Food_Category" >Food Category</InputLabel>
                  <Select
                    labelId="Food_Category"
                    value={newFood.food_categoryId}
                    name="food_categoryId"
                    label="Food Category"
                    onChange={handleInputChange}
                    required
                  >
                    {foodCategory.map(category => (
                      <MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} md={12}>
                <TextField
                  fullWidth
                  label="Code"
                  name="code"
                  value={newFood.code}
                  onChange={handleInputChange}
                  error={Boolean(errors.code)}
                  helperText={errors.code}
                  required
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Price"
                  name="price"
                  value={newFood.price}
                  onChange={handleInputChange}
                  error={Boolean(errors.price)}
                  helperText={errors.price}
                  required
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  label="Discount"
                  name="discount"
                  value={newFood.discount}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  multiline
                  name="description"
                  rows={4}
                  label="Description"
                  value={newFood.description}
                  onChange={handleInputChange}
                />
              </Grid>
            </Grid>
           
            <div className="mt-4 text-end">
              <Button
                variant="outlined"
                onClick={onCancel}
                sx={{ mr: 2 }}
              >
                Close
              </Button>
              <Button
                variant="contained"
                onClick={handleSave }
                sx={{ backgroundColor: "#4085ad", color: "#fff" }}
              >
                Submit
              </Button>
            </div>
          </div>
        </Box>
      </Modal>
    </>
  );
};

export default AddFoodForm;
