
import "../../../../Globals/Profile/Profile.css";
import "../../../../Globals/Food/Food.css";
import { useState, useEffect } from "react";
import { Modal, Box, Typography, Button, TextField, Select, MenuItem, FormControl, InputLabel, Grid } from '@mui/material';
import { useSelector } from "react-redux";
import useFood from "../core/Action";

const initialData = {
    name: "",
    code: "",
    price: "",
    discount: "",
    description: "",
    food_categoryId: "",

};
const EditFoodForm = ({ handleClose, foodData }) => {
    const { foodCategory } = useSelector(state => state.food)

    const { foodEdit } = useFood()
    const [editFood, setEditFood] = useState({ ...initialData })

    useEffect(() => {
        setEditFood((prevData) => ({
            ...prevData,
            ...foodData,
            food_categoryId: foodData?.foodCategoryEntity?.id
        }))
    }, [foodData]);
    console.log(foodData);
    const handleChange = (e) => {
        const { name, value } = e.target;
        setEditFood({ ...editFood, [name]: value });
    };

    const handleSave = (e) => {
        e.preventDefault();
        foodEdit(editFood).then(() => {
            handleClose()
        })

        setEditFood({ ...initialData })
    };
    return (
        <>
            <Modal open={true} onClose={handleClose}>
                <Box
                    sx={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width: '90%', // Adjusted width for smaller screens
                        maxWidth: 600, // Maximum width for larger screens
                        bgcolor: 'background.paper',
                        boxShadow: 24,
                        borderRadius: "5px",
                        p: 4,
                        // Responsive styles
                        '@media (max-width: 600px)': { // xs and sm screens
                            width: '90%', // Adjusted width
                            maxWidth: 'none', // No maximum width
                        },
                        '@media (min-width: 600px) and (max-width: 960px)': { // md screens
                            width: '80%', // Adjusted width
                            maxWidth: 500, // Maximum width
                        },
                        '@media (min-width: 960px)': { // lg screens
                            width: 600, // Fixed width
                            maxWidth: 'none', // No maximum width
                        },
                    }}
                >
                    <Typography variant="h4" component="h2" style={{ textAlign: "left" }}>
                        Edit Food
                    </Typography>
                    <Grid container spacing={2}>
                        <Grid item xs={12} md={8}>
                            <TextField
                                fullWidth
                                label="Name Food"
                                value={editFood.name}
                                name="name"
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <FormControl fullWidth >
                                <InputLabel>Food Category</InputLabel>
                                <Select
                                    name="food_categoryId"
                                    value={foodData?.foodCategoryEntity?.id}
                                    onChange={handleChange}
                                    label="Food Category"
                                    placeholder="Category"
                                >
                                    {foodCategory.map((foodCategories) => (
                                        <MenuItem key={foodCategories.id} value={foodCategories.id}>
                                            {foodCategories.name}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                label="Code"
                                value={editFood?.code}
                                name="code"
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField
                                fullWidth
                                label="Price"
                                value={editFood?.price}
                                name="price"
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField
                                fullWidth
                                label="Discount"
                                value={editFood?.discount}
                                placeholder="Discount"
                                name="discount"
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                multiline
                                label="Description"
                                value={editFood?.description}
                                placeholder="Description"
                                name="description"
                                onChange={handleChange}
                            />
                        </Grid>
                    </Grid>
                    <div className="mt-4 text-end">
          <Button
            style={{
              backgroundColor: "#f1f1f1",
              color: "",
              marginRight: 3,
            }}
            onClick={handleClose}
          >
            Cancel
          </Button>
          <Button
            style={{
              backgroundColor: "#4085ad",
              color: "#ffff",
            }}
            onClick={handleSave}
          >
            Save
          </Button>
        </div>
                </Box>
            </Modal>
        </>
    );
};

export default EditFoodForm;
