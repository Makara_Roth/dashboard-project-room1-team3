
import React from 'react'
import { useSelector } from 'react-redux';
import { Badge } from '@mui/material';
import { Typography } from '@mui/material';

function ListCategoryForFillterFoods({ active, setActive }) {
    const { foodCategory } = useSelector(state => state.food)

    return (
        <div className="px-1">
            <div className={' flex-wrap mt-3 py-1 pb-3'}>
                <Badge onClick={() => setActive("All")} className="px-2 py-3 my-3 fs-6 mx-2  "
                    bg={active === "All" ? "primary" : "secondary"}>All</Badge>
                {
                    foodCategory.map((item, index) =>
                        <Badge onClick={() => setActive(item)} key={index} className="px-2 py-3 fs-6 mx-2"
                            bg={active === item ? "primary" : "secondary"}> <Typography
                            >
                                {item}
                            </Typography>
                            </Badge>)
                            
                }
            </div>
        </div>
    )
}

export default ListCategoryForFillterFoods