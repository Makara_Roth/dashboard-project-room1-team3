import React, { useState } from "react";
import {
  IconButton,
  Popover,
  Button,
  Card,
  CardContent,
  CardActions,
  Typography,
  Tooltip,
  CardMedia,
} from "@mui/material";
import { MoreVert } from "@mui/icons-material";
import AddPhotoAlternateIcon from "@mui/icons-material/AddPhotoAlternate";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import { ReactComponent as Delete } from "../../../../assets/images/trash.svg";
import { ReactComponent as Edit } from "../../../../assets/images/edit.svg";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import { ReactComponent as Cart } from "../../../../assets/images/shopping-cart.svg";
import { Link } from "react-router-dom";

const CardFood = ({
  id,
  name,
  code,
  discount,
  price,
  foodCategoryEntity,
  foodImage,
  handleShow,
  onEdit,
  onDelete,
  onUploadImage,
  onDeleteFoodImage,
  onEditImage,
  handleAddToCart,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const DU = open ? "simple-popover" : undefined;

  return (
    <div className="mt-4 d-flex justify-content-sm-center w-100">
      <Card className="w-100 min-vh-25 min-vw-25 bg-body-tertiary">
        <div className="position-relative">
          <div className="position-absolute top-0 w-100 h-100 ">
            <IconButton
              style={{ left: "85%" }}
              className=" text-secondary"
              aria-describedby={DU}
              onClick={handleClick}
            >
              <MoreVert />
            </IconButton>
            <Popover
              id={DU}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
            >
              <Tooltip title="Upload Image">
                <Button onClick={handleClose}>
                  <AddPhotoAlternateIcon onClick={onUploadImage} />
                </Button>
              </Tooltip>
              <Tooltip title="Edit Image">
                <Button onClick={handleClose}>
                  <ModeEditIcon onClick={onEditImage} />
                </Button>
              </Tooltip>
              <Tooltip title="Delete Image">
                <Button onClick={handleClose}>
                  <Delete onClick={onDeleteFoodImage} />
                </Button>
              </Tooltip>
            </Popover>
          </div>
          {foodImage ? (
            <>
              <CardMedia
                component="img"
                height="194"
                image={foodImage}
                alt="Paella dish"
              />
              {/* <img
              src={foodImage}
              style={{ height: "250px", width: "100%" }}
              className="rounded-2"
              alt="food"
            />*/}
            </>
          ) : (
            <img
              src="https://st3.depositphotos.com/23594922/31822/v/1600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
              style={{ height: "194px", width: "100%" }}
              className="rounded-2"
              alt="food"
            />
          )}
        </div>
        <CardContent>
          <div className="d-flex justify-content-between">
            <Typography
              variant="h5"
              component="div"
              noWrap
              sx={{
                textOverflow: "ellipsis",
                overflow: "hidden",
                whiteSpace: "nowrap",
              }}
            >
              {name}
            </Typography>
            <button className="p-1 food" size="small" onClick={handleAddToCart}>
              <Cart />
            </button>
          </div>
          <Typography variant="h6" component="div" color="text.secondary">
            {foodCategoryEntity}
          </Typography>
          <Typography sx={{ fontSize: 17 }} className="mt-2 fw-medium">
            <span>Price: </span>
            <span className="text-danger">${price}</span>
            <sup className="me-1">%{discount}off</sup>
          </Typography>
        </CardContent>
        <CardActions
          sx={{
            display: "flex",
            justifyContent: "end",
            alignItems: "center",
            gap: "1rem",
            mt: -1,
          }}
        >
          <Link to={`/food/detail/${id}`} className=" text-decoration-none">
            <button className="p-1 food">
              <VisibilityOutlinedIcon />
            </button>
          </Link>
          <button className="p-1 food" onClick={onEdit}>
            <Edit />
          </button>
          <button className="p-1 food" onClick={onDelete}>
            <Delete />
          </button>
        </CardActions>
      </Card>
    </div>
  );
};

export default CardFood;
