import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { Close } from "@mui/icons-material";
import {
  Rating,
  Card,
  CardContent,
  Typography,
} from "@mui/material";

import Box from '@mui/material/Box';
import CardMedia from '@mui/material/CardMedia';

import useFood from "../core/Action";
import { useMediaQuery } from "@mui/material";
import MySpinner from "../../../../Globals/helpers/components/MySpinner";
import useOrder from "../../Order/core/Action";

const FoodDetail = () => {
  const id = useParams().id;
  const { getFoodById } = useFood();
  const { handleAddToCart } = useOrder();
  const { foodById } = useSelector((state) => state.food);
  const [loading, setLoading] = useState(true);
  const [value, setValue] = useState(0);
  const isMobile = useMediaQuery("(max-width:600px)");

  useEffect(() => {
    getFoodById(id);
    setTimeout(() => {
      setLoading(false);
    }, 300);
    // eslint-disable-next-line
  }, [id]);

  // Handle change when user clicks on a star
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      {loading ? (
        <MySpinner />
      ) : (
        <Card
          className="card"
          sx={{
            hight: "10rem",
            maxWidth: isMobile ? "100%" : "90%",
            display: "flex",
            flexDirection: isMobile ? "column" : "row",
          }}
        >
          <CardContent>
            {/* <Box
              display="flex"
              justifyContent={isMobile ? "flex-end" : "flex-end"}
              mb={isMobile ? 2 : 0}
            >
          </Box> */}
            <div className="d-flex justify-content-end">
              <Link to="/food" style={{color: "black"}}>
                <Close />
              </Link>
            </div>
            <Box display="flex" flexDirection={isMobile ? "column" : "row"}>
              <Box
                flex={isMobile ? "auto" : 5}
                sx={{
                  height: isMobile ? "40vh" : "22rem",
                  width: "100%",
                  objectFit: "cover"
                }}
              >
                <CardMedia
                  component="img"
                  src={
                    foodById.foodImage || "https://st3.depositphotos.com/23594922/31822/v/1600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
                  }
                  alt={foodById.foodImage ? "Food Image" : "Placeholder Image"}
                  sx={{
                    height: "100%",
                    width: "100%",
                  }}
                />
              </Box>
              <Box
                flex={isMobile ? "auto" : 4}
                ml={isMobile ? 0 : 2}
                mr={isMobile ? 0 : 2}
                mt={isMobile ? 2 : 0}
              >
                <Typography variant="h6" component="div" color="text.secondary">
                  {foodById.foodCategoryEntity.name}
                </Typography>
                <Typography variant="h2" component="h1" gutterBottom>
                  {foodById.name}
                </Typography>
                <Box display="flex" alignItems="center" mb={2}>
                  <Rating
                    name="reviews"
                    value={value}
                    precision={1} // Set precision to 1 for half-star increments
                    onChange={handleChange}
                  />
                  <Typography variant="subtitle1" ml={1}>
                    ({value} Reviews)
                  </Typography>
                </Box>

                <Box mt={1}>
                  <Typography variant="h5" color="textSecondary" component="h6">
                    Code : {foodById.code}
                  </Typography>
                  <Typography
                    variant="h3"
                    color="textSecondary"
                    className="my-2 fw-medium"
                  >
                    {/* <span>Price: </span> */}
                    <span className="text-danger">${foodById.price}</span>
                    <sup className="me-1" style={{fontSize: "15px"}}>%{foodById.discount}off</sup>
                  </Typography>
                  <button
                    className="p-2 mb-1 food"
                    size="small"
                    onClick={() => handleAddToCart(foodById)}
                  >
                    Add to cart
                  </button>
                  <hr className="one" />
                  <Typography
                    variant="h5"
                    color="textSecondary"
                    component="h6"
                    className="mt-3"
                  >
                    Description : {foodById.description}
                  </Typography>
                </Box>
              </Box>
            </Box>
          </CardContent>
        </Card>
      )}
    </>
  );
};

export default FoodDetail;
