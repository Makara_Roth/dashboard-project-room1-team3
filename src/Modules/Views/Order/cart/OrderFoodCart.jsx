import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { Col, Row } from "react-bootstrap";
import Card from "@mui/material/Card";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";
import { ReactComponent as HandRight } from "../../../../assets/images/right-finger.svg";
import { ReactComponent as HandClick } from "../../../../assets/images/click.svg";
import { ReactComponent as EmptyCart } from "../../../../assets/images/empty-shopping-cart-95276f54.svg";
import useOrder from "../core/Action";
import { Typography } from "@mui/material";
import { getTotal, setItems, setTable, setUser } from "../core/Reducer";

const OrderFoodCart = () => {
  const cart = useSelector((state) => state.combinedOrder.cart);
  const currentTable = useSelector((state) => state.table);
  const { userProfile } = useSelector((state) => state.profile);
  const {
    cartTotalQuantity,
    totalDiscount,
    cartSubtotalAmount,
    cartTotalAmount,
  } = useSelector((state) => state.combinedOrder.cart);
  const dispatch = useDispatch();
  const orderData = useSelector((state) => state.combinedOrder.order); // Get order data from Redux
  const {
    createOrder,
    handleDecreaseCart,
    handleIncreaseCart,
    handleClearCart,
    handleRemoveFromCart,
    handleTableChange,
    handlePaymentMethod,
  } = useOrder();

  useEffect(() => {
    dispatch(setUser(userProfile.id));
    dispatch(setTable(currentTable.id));
    dispatch(
      setItems(
        cart.items.map((food) => ({
          food_Id: food.id,
          quantity: food.quantity,
        }))
      )
    );
  }, [cart, userProfile, currentTable, dispatch]);
  
  useEffect(() => {
    dispatch(getTotal());
  }, [cart, dispatch]);

  const handleCheckOut = async () => {
    createOrder(orderData);
  };
  
  return (
    <div className="container">
      <h4 className="fw-bold">View Cart</h4>
      <div className="d-flex justify-content-between mb-3">
        <Breadcrumb title="Dashboard" subtitle="View Cart" />
      </div>

      {cart.items.length === 0 ? (
        <div className="text-center" style={{ marginTop: "4rem" }}>
          <div style={{ color: "#dee2e6" }}>
            {/* <img src={Empty} alt="404" style={{ width: '100%', maxWidth: '200px' }} /> */}
            <EmptyCart style={{width: "100%", maxWidth: "170px"}}/>
            <h4 className="fw-bold">Cart is Empty</h4>
            <p>Fill Your Cart With Our Products Now !</p>
          </div>
          <div>
            <Link to="/food" style={{ textDecoration: "none", color: "black" }}>
              <span className="fw-medium me-1" style={{ fontSize: "18px" }}>
                Start order your food
              </span>
              <span>
                <HandClick style={{ width: "25px", height: "25px" }} />
              </span>
            </Link>
          </div>
        </div>
      ) : (
        <>
          <div>
            <Row>
              <Col md={6} className="mb-3">
                <div
                  style={{
                    border: "1px solid #dee2e6",
                    borderRadius: "5px",
                    padding: "0 20px",
                  }}
                >
                  <FormControl
                    sx={{
                      minWidth: 110,
                      marginTop: "10px",
                      marginRight: "5px",
                    }}
                  >
                    <InputLabel id="demo-simple-select-label">Table</InputLabel>
                    <Select
                      className="mb-4"
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="table"
                      onChange={handleTableChange}
                      defaultValue=""
                    >
                      {currentTable.currentTable?.map((table) => (
                        <MenuItem key={table.id} value={table.id}>
                          {table.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <FormControl sx={{ minWidth: 110, marginTop: "10px" }}>
                    <InputLabel id="demo-simple-select-label">
                      Payment
                    </InputLabel>
                    <Select
                      className="mb-4"
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Payment"
                      onChange={handlePaymentMethod}
                      defaultValue=""
                    >
                      <MenuItem value="Cash">Cash</MenuItem>
                      <MenuItem value="Bank">Bank</MenuItem>
                    </Select>
                  </FormControl>
                  {cart.items?.map((cartItem) => (
                    <Card
                      variant="outlined"
                      key={cartItem}
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        // marginBottom: "2px",
                      }}
                    >
                      {cartItem.foodImage ? (
                        <img
                          className="rounded-3"
                          width={90}
                          height={80}
                          src={cartItem.foodImage}
                          alt=""
                        />
                      ) : (
                        <img
                          className="rounded-3"
                          width={90}
                          height={80}
                          src="https://st3.depositphotos.com/23594922/31822/v/1600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
                          alt=""
                        />
                      )}
                      <Typography
                        sx={{ fontSize: 15 }}
                        className="fw-medium px-3"
                      >
                        {cartItem.name}
                      </Typography>
                      {/* <h6 className="px-3 fw-semibold">{cartItem.name}</h6> */}
                      <Button onClick={() => handleRemoveFromCart(cartItem)}>
                        Remove
                      </Button>
                      <Button onClick={() => handleDecreaseCart(cartItem)}>
                        -
                      </Button>
                      {cartItem.quantity}

                      <Button onClick={() => handleIncreaseCart(cartItem)}>
                        +
                      </Button>
                      <span className="pe-2">
                        <span>%{cartItem.discount * cartItem.quantity}</span>off
                        <div className="text-danger">
                          ${cartItem.price * cartItem.quantity}
                        </div>
                      </span>
                    </Card>
                  ))}
                  <div className="d-flex justify-content-between my-3">
                    <Button
                      className="m-0 p-0"
                      onClick={() => handleClearCart()}
                    >
                      Clear Cart
                    </Button>
                    <span>
                      <span>Total: </span>
                      <span className="text-danger">${cartTotalAmount}</span>
                    </span>
                  </div>
                </div>
              </Col>
              <Col md={6}>
                <div
                  style={{
                    border: "1px solid #dee2e6",
                    borderRadius: "5px",
                    padding: "10px 20px",
                  }}
                >
                  <Typography sx={{ fontSize: 17 }} className="fw-medium">
                    Payment Summary
                  </Typography>
                  <div className="d-flex justify-content-between py-3">
                    <Typography sx={{ fontSize: 17 }} className="fw-medium">
                      Subtotal
                    </Typography>
                    <Typography sx={{ fontSize: 17 }} className="fw-medium">
                      ${cartTotalAmount}
                    </Typography>
                  </div>
                  <div className="d-flex justify-content-between">
                    <Typography sx={{ fontSize: 17 }} className="fw-medium">
                      Discount
                    </Typography>
                    <Typography sx={{ fontSize: 17 }} className="fw-medium">
                      ${totalDiscount}
                    </Typography>
                  </div>
                  <div className="d-flex justify-content-between py-3">
                    <Typography sx={{ fontSize: 17 }} className="fw-medium">
                      Total ({cartTotalQuantity})
                    </Typography>
                    <Typography
                      sx={{ fontSize: 17 }}
                      className="fw-medium text-danger"
                    >
                      ${cartSubtotalAmount}
                    </Typography>
                  </div>
                  <Button
                    style={{
                      backgroundColor: "#4085ad",
                      color: "#ffff",
                      width: "100%",
                    }}
                    onClick={handleCheckOut}
                  >
                    Check out
                  </Button>
                </div>
                <div className="d-flex justify-content-end mt-2">
                  <Link to="/food" style={{ textDecoration: "none", color: "black" }}>
                    <span>
                      <HandRight style={{ width: "25px", height: "25px" }} />
                    </span>
                    <span className="fw-medium mx-1">Continue Order</span>
                  </Link>
                </div>
              </Col>
            </Row>
          </div>
        </>
      )}
    </div>
  );
};

export default OrderFoodCart;
