import axios from "axios";

const reqFetchOrders = async (params) => {
    return await axios.get("/api/orders", {
        params: params
    });
};

const reqFetchOrderById = async (id) => {
    return await axios.get(`/api/orders/${id}`);
};

const reqAddOrder = async (orderData) => {
    return await axios.post(`/api/orders`, orderData);
};

const reqUpdateOrderById = async (updatedOrder, id) => {
    try {
        const response = await axios.put(`/api/orders/${id}`, updatedOrder);
        return response.data;
    } catch (error) {
        console.error("Failed to update order", error);
        return null;
    }
};

const reqDeleteOrderById = async (id) => {
    return await axios.delete(`/api/orders/${id}`);
};

const reqUpdatePaymentMethod = async (data, id) => {
    return await axios.put(`/api/orders/payment/${id}`, { paymentMethod: data })
}

const reqUpdateOrderStatus = async (data, id) => {
    return await axios.put(`/api/orders/status/${id}`, { status: data })
}

export { reqFetchOrders, reqFetchOrderById, reqAddOrder, reqUpdateOrderById, reqDeleteOrderById, reqUpdateOrderStatus, reqUpdatePaymentMethod };
