import { useDispatch, useSelector } from "react-redux";
import { reqFetchOrders, reqAddOrder, reqDeleteOrderById, reqFetchOrderById, reqUpdateOrderStatus, reqUpdatePaymentMethod, reqUpdateOrderById } from "./Request";
import { toast } from "react-toastify";
import { useNavigate } from "react-router";
import { addToCart, clearCart, decreaseCart, removeFromCart, setLoading, setOrderDetail, setOrders, setParams, setPaymentMethod, setTable } from "./Reducer";

const useOrder = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { params } = useSelector(state => state.combinedOrder.orderList)

  const getOrders = () => {
    dispatch(setLoading(true));
    reqFetchOrders(params).then((res) => {
      dispatch(setOrders(res.data));
    });
  };

  const getOrderById = (id) => {
    reqFetchOrderById(id).then((res) => {
      dispatch(setOrderDetail(res.data.data))
    }).catch((err) => {
      toast.error(err.response.data.message)
    })
  }

  const createOrder = async (payload) => {
    const payloadValues = Object.values(payload);
    const isAllFilled = payloadValues.every((value) => value !== "");
    const isTableSelected = payload.table !== "";
    if (isAllFilled) {
      try {
        await reqAddOrder(payload).then(() => {
          dispatch(clearCart())
          toast.success("Order has been created");
          navigate("/food");
        })
      } catch (err) {
        toast.error(err.response.data.message)
      }
    } else {
      if (!isAllFilled) {
        toast.error("Please selete table and payment method");
      } else if (!isTableSelected) {
        toast.error("Please selete table");
      }
    }
  }

  const deleteOrderById = (id) => {
    return reqDeleteOrderById(id).then((res) => {
      toast.success(res.data.message);
    })
      .catch((err) => {
        toast.error(err.response.data.message);
      })
  }

  const updateOrder = async (updatedOrder, id) => {
    const response = await reqUpdateOrderById(updatedOrder, id);
    if (response) {
      getOrderById(id);
      toast.success("Order successfully updated");
    } else {
      toast.error("Failed to update order");
    }
  };


  const updateOrderStatus = async (data, id) => {
    try {
      await reqUpdateOrderStatus(data, id)
      getOrderById(id)
    } catch (err) {
      toast.error(err.response.data.message);
    }
  }

  const updateOrderPayment = async (data, id) => {
    try {
      await reqUpdatePaymentMethod(data, id)
      getOrderById(id)
    } catch (err) {
      toast.error(err.response.data.message);
    }
  }

  const handleAddToCart = (item) => {
    dispatch(addToCart(item));
  };

  const handleSearch = (text) => {
    dispatch(setParams({ page: 1, query: text }));
  }

  const handlePage = (page) => dispatch(setParams({ page: page }));

  const handleSort = (field) => {
    const newOrder = params.order === "asc" ? "desc" : "asc";
    dispatch(setParams({ sort: field, order: newOrder }));
  }

  const handleFilter = (name, value) => {
    dispatch(setParams({ [name]: value }));
  }

  const handleDecreaseCart = (cartItem) => {
    dispatch(decreaseCart(cartItem));
  };

  const handleIncreaseCart = (order) => {
    dispatch(addToCart(order));
  };

  const handleClearCart = () => {
    dispatch(clearCart());
  };

  const handleRemoveFromCart = (cartItem) => {
    dispatch(removeFromCart(cartItem));
  };

  const handleTableChange = (e) => {
    dispatch(setTable(parseInt(e.target.value)));
  };

  const handlePaymentMethod = (e) => {
    dispatch(setPaymentMethod(e.target.value));
  };

  return {
    getOrders,
    getOrderById,
    createOrder,
    updateOrder,
    updateOrderStatus,
    updateOrderPayment,
    deleteOrderById,
    handleAddToCart,
    handleSearch,
    handlePage,
    handleSort,
    handleFilter,
    handleDecreaseCart,
    handleIncreaseCart,
    handleClearCart,
    handleRemoveFromCart,
    handleTableChange,
    handlePaymentMethod
  };
};

export default useOrder;
