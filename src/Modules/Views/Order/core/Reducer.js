import { createSlice } from "@reduxjs/toolkit";
import { toast } from "react-toastify";

const initialState = {
  orderList: {
    orders: [],
    order: [],
    orderIdToDelete: null,
    addFoodToggle: false,
    totalPage: 0,
    totals: 0,
    loading: true,
    selectedItems: [],
    selectAll: false,
    orderTableId: null,
    params: {
      page: 1,
      size: 12,
      query: "",
      sort: "",
      order: "desc",
    },
  },
  order: {
    user_Id: null,
    table_Id: null,
    paymentMethod: "",
    items: []
  },
  cart: {
    items: localStorage.getItem("items")
      ? JSON.parse(localStorage.getItem("items"))
      : [],
    cartTotalQuantity: 0,
    cartSubtotalAmount: 0,
    cartTotalAmount: 0,
    totalDiscount: 0,
  },
  paging: {}
};

const listOrderSlice = createSlice({
  name: "combinedOrder",
  initialState,
  reducers: {
    setOrders: (state, action) => {
      state.orderList.orders = action.payload.data;
      state.paging = action.payload.paging
      state.totals = action.payload.paging.totals
      state.paging = action.payload.paging;
      state.orderList.totalPage = action.payload.paging.totalPage;
      state.orderList.loading = false;
    },
    setOrderDetail: (state, action) => {
      state.orderList.order = action.payload;
    },
    setOrderIdToDelete: (state, action) => {
      state.orderList.orderIdToDelete = action.payload;
    },
    setDeleteOrder: (state, action) => {
      const deletedId = action.payload;
      state.orderList.orders = state.orderList.orders.filter(
        (order) => order.id !== deletedId
      );
    },
    setAddFoodToggle: (state, action) => {
      state.orderList.addFoodToggle = action.payload;
    },
    setOrderTableId: (state, action) => {
      console.log("orderTableId:", action.payload);
      state.orderList.orderTableId = action.payload
    },
    setLoading: (state, action) => {
      state.orderList.loading = action.payload;
    },
    setParams: (state, action) => {
      state.orderList.params = {
        ...state.orderList.params,
        ...action.payload,
      };
    },
    setUser: (state, action) => {
      state.order.user_Id = action.payload;
    },
    setTable: (state, action) => {
      state.order.table_Id = action.payload;
    },
    setPaymentMethod: (state, action) => {
      state.order.paymentMethod = action.payload;
    },
    setItems: (state, action) => {
      state.order.items = action.payload;
    },
    addToCart(state, action) {
      const itemIndex = state.cart.items.findIndex(
        (item) => item.id === action.payload.id
      );

      if (itemIndex >= 0) {
        state.cart.items[itemIndex].quantity += 1;
      } else {
        const tempFood = { ...action.payload, quantity: 1 };
        state.cart.items.push(tempFood);
        toast.success(`${action.payload.name} added to cart`);
      }
      listOrderSlice.caseReducers.getTotal(state);
      localStorage.setItem("items", JSON.stringify(state.cart.items));
    },
    removeFromCart(state, action) {
      const nextCartItems = state.cart.items.filter(
        (cartItem) => cartItem.id !== action.payload.id
      );

      state.cart.items = nextCartItems;
      localStorage.setItem("items", JSON.stringify(state.cart.items));
      toast.error(`${action.payload.name} removed from order`);
    },
    decreaseCart(state, action) {
      const itemIndex = state.cart.items.findIndex(
        (cartItem) => cartItem.id === action.payload.id
      );
      if (state.cart.items[itemIndex].quantity > 1) {
        state.cart.items[itemIndex].quantity -= 1;
      } else if (state.cart.items[itemIndex].quantity === 1) {
        const nextCartItems = state.cart.items.filter(
          (cartItem) => cartItem.id !== action.payload.id
        );

        state.cart.items = nextCartItems;

        toast.error(`${action.payload.name} removed from order`);
      }
      localStorage.setItem("items", JSON.stringify(state.cart.items));
    },
    clearCart(state, action) {
      state.cart.items = [];
      localStorage.setItem("items", JSON.stringify(state.cart.items));
    },
    getTotal(state, action) {
      let { total, cartQuantity, cartDiscount, subTotal } =
        state.cart.items.reduce(
          (cartTotal, cartItem) => {
            const { price, quantity, discount } = cartItem;
            const itemTotal = price * quantity;
            const itemDiscount = (price - discount) * quantity;
            const itemSubTotal = discount * quantity;

            cartTotal.total += itemTotal;
            cartTotal.cartQuantity += quantity;
            cartTotal.cartDiscount += itemDiscount;
            cartTotal.subTotal += itemSubTotal;

            return cartTotal;
          },
          {
            total: 0,
            cartQuantity: 0,
            cartDiscount: 0,
            subTotal: 0,
          }
        );

      state.cart.cartTotalQuantity = cartQuantity;
      state.cart.cartSubtotalAmount = subTotal;
      state.cart.cartTotalAmount = total;
      state.cart.totalDiscount = cartDiscount;
    },
    toggleSelectAll(state) {
      if (state.orderList.selectAll) {
        state.orderList.selectedItems = [];
      } else {
        state.orderList.selectedItems = state.orderList.order.map(
          (orderItem) => orderItem.id
        );
      }
      state.orderList.selectAll = !state.orderList.selectAll;
    },
    toggleSelectItem(state, action) {
      const orderId = action.payload;
      const selectedItems = state.orderList.selectedItems.includes(orderId)
        ? state.orderList.selectedItems.filter((id) => id !== orderId)
        : [...state.orderList.selectedItems, orderId];
      state.orderList.selectedItems = selectedItems;
      state.orderList.selectAll =
        selectedItems.length === state.orderList.order.length;
    },
    deleteSelectedItems(state) {
      const selectedIds = state.orderList.selectedItems;
      state.orderList.order = state.orderList.order.filter(
        (orderItem) => !selectedIds.includes(orderItem.id)
      );
      state.orderList.selectedItems = [];
      state.orderList.selectAll = false;
    },
  },
});

export const {
  setOrders,
  setOrderDetail,
  setOrderIdToDelete,
  setAddFoodToggle,
  setDeleteOrder,
  setLoading,
  setParams,
  setOrderTableId,
  setUser,
  setTable,
  setPaymentMethod,
  setItems,
  addToCart,
  removeFromCart,
  decreaseCart,
  clearCart,
  getTotal,
  toggleSelectAll,
  toggleSelectItem,
  deleteSelectedItems,
} = listOrderSlice.actions;

export default listOrderSlice.reducer;
