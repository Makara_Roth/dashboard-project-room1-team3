import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import useOrder from "../core/Action";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";
import { Link } from "react-router-dom";
import { CloseRounded } from "@mui/icons-material";
import { ReactComponent as Delete } from "../../../../assets/images/trash.svg";
import AddNewFood from "./AddNewFood";
import {
  Button,
  Card,
  Checkbox,
  Chip,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import {
  deleteSelectedItems,
  setAddFoodToggle,
  toggleSelectAll,
  toggleSelectItem,
} from "../core/Reducer";

const OrderDetail = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const {
    order,
    orders,
    orderTableId,
    addFoodToggle,
    selectAll,
    selectedItems,
  } = useSelector((state) => state.combinedOrder.orderList);
  const { userProfile } = useSelector((state) => state.profile);
  const {
    getOrderById,
    updateOrderStatus,
    updateOrderPayment,
    updateOrder,
    handleAddToCart,
  } = useOrder();

  useEffect(() => {
    if (id) {
      getOrderById(id);
    }
    // eslint-disable-next-line
  }, [id]);

  const statusList = [
    { id: 0, name: "Prepare" },
    { id: 1, name: "Cooking" },
    { id: 2, name: "Complete" },
    { id: 3, name: "Cancel" },
  ];

  const paymentList = [
    { id: 0, name: "Bank" },
    { id: 1, name: "Cash" },
  ];

  const handleStatus = (status) => {
    updateOrderStatus(status, parseInt(id));
  };

  const handlePayment = (payment) => {
    updateOrderPayment(payment, parseInt(id));
  };

  const handleCheckboxToggle = (orderId) => {
    dispatch(toggleSelectItem(orderId));
  };

  const handleSelectAllToggle = () => {
    dispatch(toggleSelectAll());
  };

  const handleDeleteSelected = () => {
    dispatch(deleteSelectedItems());
  };

  const handleSave = () => {
    const updatedOrderItems = order.filter(
      (orderItem) => !selectedItems.includes(orderItem.id),
    );
    const orderTableBysUserID = orders.find((item) => item.id === parseInt(id));

    const updatedOrder = {
      user_Id: userProfile.id,
      table_Id: orderTableBysUserID?.table?.id,
      items: updatedOrderItems.map((item) => ({
        food_Id: item.id,
        quantity: item.quantity,
      })),
    };
    updateOrder(updatedOrder, id);
  };

  return (
    <>
      {addFoodToggle && (
        <AddNewFood
          handleClose={() => dispatch(setAddFoodToggle())}
          handleAddToCart={handleAddToCart}
        />
      )}
      <h4 className="fw-bold">Order</h4>
      <div className="d-flex justify-content-between mb-3">
        <Breadcrumb key="uniqueKey" title="Dashboard" subtitle="Order Detail" />
      </div>
      <Container maxWidth="sm" className="p-0">
        <div className="d-flex justify-content-between mt-5">
          <h6 className="fw-bold">Order ID: {id}</h6>
          <Link to="/order" className="mt-2" style={{ color: "black" }}>
            <CloseRounded />
          </Link>
        </div>
        <div
          style={{
            border: "1px solid #dee2e6",
            borderRadius: "5px",
            padding: "5px",
          }}
        >
          <div className="d-flex justify-content-between">
            <span className="align-content-center">
              <Checkbox checked={selectAll} onChange={handleSelectAllToggle} />
              {selectedItems.length > 0 && (
                <Button
                  startIcon={<Delete />}
                  variant="contained"
                  color="error"
                  className="p-1 ms-2"
                  onClick={handleDeleteSelected}
                >
                  Delete
                </Button>
              )}
            </span>
            <div>
              <FormControl
                sx={{
                  minWidth: 80,
                  marginRight: "5px",
                }}
              >
                <InputLabel id="status-select-label">Status</InputLabel>
                <Select
                  labelId="status-select-label"
                  id="status-select"
                  label="Status"
                  onChange={(e) => handleStatus(e.target.value)}
                  value={order?.[0]?.order.status || ""}
                >
                  {statusList.map((item) => (
                    <MenuItem key={item.id} value={item.name}>
                      {item.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl
                sx={{
                  minWidth: 150,
                }}
              >
                <InputLabel id="payment-select-label">
                  Payment Method
                </InputLabel>
                <Select
                  labelId="payment-select-label"
                  id="payment-select"
                  label="Payment Method"
                  onChange={(e) => handlePayment(e.target.value)}
                  value={order?.[0]?.order.paymentMethod || ""}
                >
                  {paymentList.map((item) => (
                    <MenuItem key={item.id} value={item.name}>
                      {item.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
          </div>
          {order && order.length > 0 ? (
            order.map((orderItem) => (
              <Card
                key={orderItem.id}
                variant="outlined"
                sx={{
                  display: "flex",
                  alignItems: "center",
                  marginBottom: "2px",
                  marginTop: "2px",
                }}
              >
                <Checkbox
                  checked={selectedItems.includes(orderItem.id)}
                  onChange={() => handleCheckboxToggle(orderItem.id)}
                />

                <img
                  className="rounded-3"
                  width={90}
                  height={80}
                  src={
                    orderItem.food.food_image?.[0]?.url ||
                    "https://st3.depositphotos.com/23594922/31822/v/1600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
                  }
                  alt={orderItem.food.name || "Default Image"}
                />
                <div className="mx-3">{orderItem.food.name || "No Name"}</div>
                <Chip
                  className="mx-2 my-2"
                  sx={{
                    px: "4px",
                    backgroundColor:
                      orderItem.order.status === "Complete"
                        ? "complete.main"
                        : orderItem.order.status === "Prepare"
                          ? "primary.dark"
                          : orderItem.order.status === "Cooking"
                            ? "warning.main"
                            : "error.main",
                    color: "warning.contrastText",
                  }}
                  size="small"
                  label={orderItem.order.status}
                />
                <Chip
                  className="mx-2 my-2"
                  sx={{
                    px: "4px",
                    backgroundColor:
                      orderItem.order.paymentMethod === "Bank"
                        ? "text.primary"
                        : orderItem.order.paymentMethod === "Cash"
                          ? "text.secondary"
                          : "error.main",
                    color: "warning.contrastText",
                  }}
                  size="small"
                  label={orderItem.order.paymentMethod}
                />
                <div className="mx-4">{orderItem.quantity || 0}</div>
                <div>${orderItem.total_Price || 0}</div>
              </Card>
            ))
          ) : (
            <div className="d-flex justify-content-center fw-medium">
              No order
            </div>
          )}

          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              padding: "10px 0",
              fontWeight: 500,
            }}
          >
            Total:
            <div className="text-danger ps-1">
              $
              {order
                ?.reduce((acc, orderItem) => acc + orderItem.total_Price, 0)
                .toFixed(2)}
            </div>
          </div>
        </div>
        <div className="d-flex justify-content-end mt-2">
          <button
            className="food p-1"
            onClick={() => dispatch(setAddFoodToggle(true))}
          >
            Add Food
          </button>
          <button className="food p-1 ms-1" onClick={handleSave}>
            Save
          </button>
        </div>
      </Container>
    </>
  );
};

export default OrderDetail;
