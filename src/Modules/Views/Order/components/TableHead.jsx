import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Typography,
} from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import useOrder from "../core/Action";

const TableHeadOrder = () => {
  const { params } = useSelector((state) => state.combinedOrder.orderList);
  const { handleSort } = useOrder();
  const { sort, order } = params;

  return (
    <TableHead>
      <TableRow>
        
        <TableCell>
          <TableSortLabel
            active={sort === "id"}
            direction={order}
            onClick={() => handleSort("id")}
          >
            <Typography variant="subtitle2" fontWeight={600}>
              No
            </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
            <Typography variant="subtitle2" fontWeight={600}>
              Table
            </Typography>
        </TableCell>

        <TableCell>
          <Typography variant="subtitle2" fontWeight={600}>
            Total
          </Typography>
        </TableCell>

        <TableCell>
          <Typography variant="subtitle2" fontWeight={600}>
            User
          </Typography>
        </TableCell>

        <TableCell align="center">
          <Typography variant="subtitle2" fontWeight={600}>
            Action
          </Typography>
        </TableCell>

      </TableRow>
    </TableHead>
  );
};

export default TableHeadOrder;
