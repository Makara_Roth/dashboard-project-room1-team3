import {
  IconButton,
  TableCell,
  TableRow,
  Typography,
} from "@mui/material";
import { ReactComponent as Delete } from "../../../../assets/images/trash.svg"
import VisibilityOutlinedIcon from '@mui/icons-material/VisibilityOutlined';
import React from "react";
import { Link } from "react-router-dom";

const PostAllOrder = ({ id, table, user, total_Price, onDelete }) => {
  return (
      <TableRow>
        <TableCell>
          <Typography
            variant="subtitle2" fontWeight={600}
          >
            {id}
          </Typography>
        </TableCell>

        <TableCell>
          <Typography
            variant="subtitle2" fontWeight={600}
          >
            {table}
          </Typography>
        </TableCell>

        <TableCell>
          <Typography
            variant="subtitle2" fontWeight={600}
          >
            ${total_Price}
          </Typography>
        </TableCell>

        <TableCell>
          <Typography
            variant="subtitle2" fontWeight={600}
          >
            {user}
          </Typography>
        </TableCell>

        <TableCell align="center">
          <Link
            style={{ textDecoration: "none" }}
            to={`/order/orderdetail/${id}`}
          >
            <IconButton aria-label="view" color="primary">
              <VisibilityOutlinedIcon />
            </IconButton>
          </Link>
          <IconButton onClick={onDelete} aria-label="delete" color="error">
            <Delete />
          </IconButton>
        </TableCell>
      </TableRow>
  );
};

export default PostAllOrder;
