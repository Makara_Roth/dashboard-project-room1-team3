import React, { useEffect } from "react";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";
import useOrder from "../core/Action";
import { useDispatch, useSelector } from "react-redux";
import DashboardCard from "../../../Components/shared/DashboardCard";
import {
  Box,
  Container,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from "@mui/material";
import TableHeadOrder from "./TableHead";
import PostAllOrder from "./PostAllOrder";
import SearchField from "../../../../Globals/helpers/components/SearchField";
import { setOrderIdToDelete } from "../core/Reducer";
import GlobalPagination from "../../../../Globals/helpers/components/Pagination";
import GlobalDeleteModal from "../../../../Globals/helpers/alert/DeleteModal";
import Filter from "../../../../Globals/helpers/components/Filter";

const Order = () => {
  const { getOrders, handleSearch, handlePage, deleteOrderById, handleFilter } =
    useOrder();
  const { orders, params, totalPage, orderIdToDelete } = useSelector(
    (state) => state.combinedOrder.orderList
  );
  const { paging } = useSelector((state) => state.combinedOrder);
  const dispatch = useDispatch();
  const { page } = params;
  useEffect(() => {
    getOrders();
    // eslint-disable-next-line
  }, [params, orderIdToDelete]);

  const onDeleteHandler = (id) => {
    dispatch(setOrderIdToDelete(id));
  };

  const confirmDelete = () => {
    deleteOrderById(orderIdToDelete).then(() => {
      dispatch(setOrderIdToDelete(null));
    });
  };

  const handleChange = (event, value) => {
    handlePage(value);
  };

  const renderedOrderList = orders.map((order, index) => {
    return (
      <PostAllOrder
        key={index}
        id={order.id}
        table={order.table.name}
        total_Price={order.total_Price}
        user={order.user.name}
        onDelete={() => onDeleteHandler(order.id)}
      />
    );
  });

  return (
    <Container maxWidth="lg" className="p-0">
      <h4 className="fw-bold">Orders</h4>
      <div className="d-flex justify-content-between">
        <Breadcrumb title="Dashboard" subtitle="List Order" />
        <p className="fw-bold">Total Orders: {paging.totals}</p>
      </div>

      <DashboardCard>
        <div className="d-flex justify-content-between">
          <div style={{ width: "10rem" }}>
            <SearchField
              title="Search Table..."
              params={params}
              onChange={(e) => handleSearch(e.target.value)}
            />
          </div>
          <span className="d-flex justify-content-end align-items-center">
            <Filter
              title="paymentMethod"
              filterlist={["Bank", "Cash"]}
              handleFilter={handleFilter}
              params={params}
            />
            <Filter
              title="status"
              filterlist={["Prepare", "Cooking", "Complete", "Cancel"]}
              handleFilter={handleFilter}
              params={params}
            />
          </span>
        </div>
        <Box sx={{ overflow: "auto", width: { xs: "280px", sm: "auto" } }}>
          <Table
            aria-label="simple table"
            sx={{
              whiteSpace: "nowrap",
              mt: 2,
            }}
          >
            <TableHeadOrder />
            <TableBody>
              {orders.length > 0 ? (
                renderedOrderList
              ) : (
                <TableRow>
                  <TableCell colSpan={8} className="text-center ">
                    <Typography
                      sx={{
                        fontSize: "18px",
                        fontWeight: "600",
                      }}
                    >
                      Order not found
                    </Typography>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </Box>
        {/* Pagination */}
        <GlobalPagination
          params={params}
          totalPage={totalPage}
          page={page}
          handleChange={handleChange}
          handleFilter={handleFilter}
        />
        {/* Delete Modal */}
        <GlobalDeleteModal
          title="Are you sure want to delete this Order?"
          open={orderIdToDelete != null}
          onClose={() => dispatch(setOrderIdToDelete(null))}
          confirmDelete={confirmDelete}
        />
      </DashboardCard>
    </Container>
  );
};

export default Order;
