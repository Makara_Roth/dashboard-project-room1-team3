import { Box, Card, Grid, Modal, Typography } from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import { ReactComponent as Cart } from "../../../../assets/images/shopping-cart.svg";
import useOrder from "../core/Action";

const AddNewFood = ({ handleClose, handleAddToCart }) => {
  const { food } = useSelector((state) => state.food);

  return (
    <Modal open={true} onClose={handleClose}>
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: 1000,
          maxWidth: 1000,
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          borderRadius: "5px",
          // Responsive styles
          "@media (max-width: 600px)": {
            // xs and sm screens
            width: "90%", // Adjusted width
            maxWidth: "none", // No maximum width
          },
          "@media (min-width: 600px) and (max-width: 960px)": {
            // md screens
            width: "80%", // Adjusted width
            maxWidth: 1000, // Maximum width
          },
          "@media (min-width: 960px)": {
            // lg screens
            width: 1000, // Fixed width
            maxWidth: "none", // No maximum width
          },
        }}
      >
        <Typography
          id="modal-modal-title"
          variant="h4"
          component="h2"
          style={{ textAlign: "left" }}
        >
          Order New Food
        </Typography>
        <main
          className="row overflow-scroll "
          style={{ maxHeight: "80vh", overflowY: "scroll" }}
        >
          {food.length === 0 ? (
            <div className="text-center">No Result.</div>
          ) : (
            <Grid container spacing={1}>
              {food?.map((cartItem, index) => (
                <Grid item xs={12} sm={6} md={6} lg={6} key={index}>
                  <Card
                    variant="outlined"
                    key={cartItem}
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      // marginBottom: "2px",
                    }}
                  >
                    <img
                      className="rounded-3"
                      width={90}
                      height={80}
                      src={
                        cartItem.foodImage ||
                        "https://st3.depositphotos.com/23594922/31822/v/1600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
                      }
                      alt={cartItem.name || "No image available"}
                    />
                    <div>
                      <Typography
                        sx={{ fontSize: 15 }}
                        className="fw-medium px-3"
                      >
                        {cartItem.name}
                      </Typography>
                      <button
                        className="p-1 food ms-3"
                        size="small"
                        onClick={handleAddToCart(cartItem)}
                      >
                        <Cart /> {""}
                      </button>
                    </div>

                    <span className="pe-2">
                      <span>%{cartItem.discount}</span>off
                      <div className="text-danger">${cartItem.price}</div>
                    </span>
                  </Card>
                </Grid>
              ))}
            </Grid>
          )}
        </main>
      </Box>
    </Modal>
  );
};

export default AddNewFood;
