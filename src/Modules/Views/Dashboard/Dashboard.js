import React, { useEffect } from "react";
import { Grid, Box, Typography, Button, Avatar } from "@mui/material";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import DashboardCard from "./components/DashboardCard"; // Ensure this path is correct
import restaurantPng from "../../../assets/images/backgrounds/catering-logo-801a6a945086a633fa349077537a05ab.png";
import logo from "../../../assets/images/kiloit-logo.svg";
import { useSelector, useDispatch } from "react-redux";
import useUser from "../User/core/Action";
import useOrder from "../Order/core/Action";
import CountUp from "react-countup";
import { useIncome } from "../Income/core/Action";
import IncomeChart from "../Income/components/IncomeChart";
import SelectionDate from "../Income/components/selection";
import FoodReportPieChart from "../FoodReport/components/FoodReportPieChart";
import SelectionDateReport from "../FoodReport/components/SelectionDateReport";
import UserPerformance from "./components/UserPerformence";

const Dashboard = () => {
  const { paging } = useSelector((state) => state.user);
  const { totals } = useSelector((state) => state?.combinedOrder);
  const dailyIncome = useSelector((state) => state?.incomes?.dailyIncome);
  const monthlyIncome = useSelector((state) => state?.incomes?.monthlyIncome);
  const { params } = useSelector((state) => state.incomes);
  const { foodPaging } = useSelector((state) => state.food);
  const { currentTable } = useSelector((state) => state.table);

  const { fetchDailyIncome, fetchMonthlyIncome } = useIncome();
  const { getOrders } = useOrder();
  const { getCurrentUser } = useUser();
  const dispatch = useDispatch();
  const { day, month } = params;

  useEffect(() => {
    getCurrentUser();
    getOrders();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    dispatch(fetchDailyIncome(day));
    // eslint-disable-next-line
  }, [dispatch, day]);
  useEffect(() => {
    dispatch(fetchMonthlyIncome(month));
    // eslint-disable-next-line
  }, [dispatch, month]);

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    border: "node",
  }));

  return (
    <Box sx={{ flexGrow: 1, p: 2 }}>
      <Grid container spacing={2}>
        {/* Dashboard Title */}
        <Grid item xs={12}>
          <Typography variant="h4" className="mb-3">
            Dashboard
          </Typography>
        </Grid>

        {/* Export PDF Button */}
        <Grid
          item
          xs={12}
          display="flex"
          justifyContent="flex-end"
          sx={{ mb: 2 }}
        >
          <Button variant="contained">Export PDF</Button>
        </Grid>

        {/* Main Content */}
        <Grid item xs={12}>
          <Grid container item spacing={2}>
            <Grid item xs={12} sm={6} md={8}>
              <Paper
                elevation={3}
                sx={{
                  p: 3,
                  borderRadius: 2,
                  display: "flex",
                  flexDirection: { xs: "column", md: "row" }, // Stack content vertically on xs screen
                  alignItems: "center",
                  gap: 3,
                  backgroundColor: "#f0f4ff",
                }}
              >
                <Box sx={{ flex: 1 }}>
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      gap: 2,
                      mt: {
                        md: 0,
                      },
                    }}
                  >
                    <Avatar alt="Mathew Anderson" src={logo} />
                    <Typography variant="h6">
                      Welcome back To My Restaurant!
                    </Typography>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      marginLeft: {
                        md: 5,
                      },
                      flexDirection: { xs: "column", md: "row" },
                      mt: { xs: 2, md: 2 },
                      alignItems: { xs: "center", md: "flex-start" },
                      gap: 2,
                    }}
                  >
                    <Box sx={{ textAlign: { xs: "center", md: "center" } }}>
                      <Typography
                        variant="h4"
                        sx={{
                          fontWeight: "bold",
                          fontSize: { xs: "28px", md: "24px" },
                        }}
                      >
                        $<CountUp end={dailyIncome[0]?.totalPrice} />
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        sx={{ fontSize: { xs: "20px", md: "16px" } }}
                      >
                        Today's Sales
                      </Typography>
                    </Box>
                    <Box sx={{ textAlign: { xs: "center", md: "center" } }}>
                      <Typography
                        variant="h4"
                        sx={{
                          fontWeight: "bold",
                          fontSize: { xs: "28px", md: "24px" },
                        }}
                      >
                        <CountUp end={35} />%
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        sx={{ fontSize: { xs: "20px", md: "16px" } }}
                      >
                        Performance
                      </Typography>
                    </Box>
                  </Box>
                </Box>
                <img
                  src={restaurantPng}
                  alt="Restaurant Logo"
                  style={{ maxWidth: 150, height: "auto" }}
                />
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={2}>
              <Item>
                <DashboardCard title="Totals Orders" data={totals} />
              </Item>
            </Grid>
            <Grid item xs={12} sm={6} md={2}>
              <Item>
                <DashboardCard title="Totals Foods" data={foodPaging?.totals} />
              </Item>
            </Grid>
            {/* </Grid> */}
            <Grid item xs={12} sm={6} md={4}>
              <Item
                sx={{
                  display: "flex",
                  justifyContent: "end",
                }}
              >
                <SelectionDate />
              </Item>
              <IncomeChart value={monthlyIncome[0]?.totalPrice} />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <Item
                sx={{
                  display: "flex",
                  justifyContent: "end",
                }}
              >
                <SelectionDateReport />
              </Item>
              <FoodReportPieChart />
            </Grid>
            <Grid item xs={12} sm={6} md={2}>
              <Item>
                <DashboardCard title="Total Users" data={paging.totals} />
              </Item>
            </Grid>
            <Grid item xs={12} sm={6} md={2}>
              <Item>
                <DashboardCard
                  title="Total Tables"
                  data={currentTable.length}
                />
              </Item>
            </Grid>
          </Grid>
        </Grid>
        {/* User Performance Component */}
        <Grid item>
          <UserPerformance />
        </Grid>
      </Grid>
    </Box>
  );
};

export default Dashboard;
