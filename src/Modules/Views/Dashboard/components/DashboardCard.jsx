
import { Card, Typography } from '@mui/material';
import { CardContent } from '@mui/material';
import CountUp from 'react-countup';
import LineCharts from './LineChart';

const DashboardCard = ({ title, data }) => {

    return ( <Card sx={{border: "none"}}>
                <CardContent>
                            <Typography variant="body1" fontSize={{ xs:12, sm: 13, md: 20 }} >{title}</Typography>
                            <Typography variant="h4" mt={2} mb={2} fontSize={{ xs: 12, sm: 14, md: 20 }} align="center">
                                <CountUp end={data}/>
                            </Typography>
                            <LineCharts Data={data}/>
                </CardContent>
            </Card>
    );

}

export default DashboardCard;