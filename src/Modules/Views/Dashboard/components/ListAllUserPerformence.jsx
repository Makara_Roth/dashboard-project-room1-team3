
import React from 'react'
import {
    TableCell,
    TableRow,
    Typography,
  } from "@mui/material";


function ListAllUserPerformance({ cashier, paymentMethod, totalPrice}) {

// console.log(cashier);

  return (
          <TableRow>
            <TableCell>
              <Typography
                sx={{
                  fontSize: "15px",
                  fontWeight: "500",
                }}
              >
                {cashier?.name}
              </Typography>
            </TableCell>
         
    
            <TableCell>
              <Typography
                sx={{
                  fontSize: "15px",
                  fontWeight: "500",
                }}
              >
                {paymentMethod === null ? " Null" : paymentMethod  }
              </Typography>
            </TableCell>
    
            <TableCell>
              <Typography
                sx={{
                  fontSize: "15px",
                  fontWeight: "500",
                }}
              >
                {totalPrice}
              </Typography>
            </TableCell>
          </TableRow>
  )
}

export default ListAllUserPerformance