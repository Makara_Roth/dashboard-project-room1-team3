import * as React from 'react';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Box from '@mui/material/Box';
import {  useDispatch } from 'react-redux';
import { setParams } from '../core/Reducer';
import dayjs from 'dayjs';
import { TextField } from '@mui/material';
import { useSelector } from 'react-redux';

export default function EndDateSelecter() {
    const dispatch = useDispatch();
    const [inputError, setInputError] = React.useState(false);
    const { params } = useSelector(state => state.dashboard)
    const { end  } = params

    const handleDateChange = (newValue) => {
        if (newValue) {
            const formattedDate = newValue.format('YYYY:MM:DD');
            console.log(formattedDate);
            if (dayjs(formattedDate).isValid()) {
                dispatch(setParams({ end: formattedDate }));
                setInputError(false);
            } else {
                setInputError(true);
            }
        } else {
            dispatch(setParams({ end: null }));
            setInputError(false);
        }
    };

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <Box
              sx={{
                   width: '100%',
                    display: 'flex',
                    justifyContent: 'center',
                    position: 'relative',
                }}
                md={{
                    width: '100%',
                    justifyContent: 'end',
                    position: 'relative',
                }}
            >
                <DatePicker
                    views={['year', 'month', 'day']}
                    label="End date"
                    value={end ? dayjs(end, 'YYYY:MM:DD') : null}
                    onChange={handleDateChange}
                    renderInput={(props) => <TextField  {...props} size='small' helperText={inputError && 'Invalid Date Format'} />}
                />
            </Box>
        </LocalizationProvider>
    );
}
