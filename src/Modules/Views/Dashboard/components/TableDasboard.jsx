import {
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
    Typography,
  } from "@mui/material";
  import React from "react";
  import { useSelector } from "react-redux";
import useDashboard from "../core/Action";

  
  const TableDashboard = () => {
    const { params } = useSelector((state) => state.dashboard);
    const { handleSort } = useDashboard();
    const { sortby, order } = params;

    return (
      <TableHead>
        <TableRow>
          <TableCell>
            <TableSortLabel
            
              active={sortby === "id"}
              direction={order}
              onClick={() => handleSort("id")}
            >
              <Typography variant="subtitle2" fontWeight={600}>
              cashier
              </Typography>
            </TableSortLabel>
          </TableCell>
  
          <TableCell>
            <TableSortLabel
            >
              <Typography variant="subtitle2" fontWeight={600}>
                Payment Methods
              </Typography>
            </TableSortLabel>
          </TableCell>
  
          <TableCell>
            <TableSortLabel
            >
              <Typography variant="subtitle2" fontWeight={600}>
              totalPrice
              </Typography>
            </TableSortLabel>
          </TableCell>
  
          {/* <TableCell>
          <TableSortLabel
            //   active={sort === "email"}
            //   direction={order}
            //   onClick={() => handleSort("email")}
            >
            <Typography variant="subtitle2" fontWeight={600}>
             totalPrice
            </Typography>
            </TableSortLabel>
          </TableCell> */}
        </TableRow>
      </TableHead>
    );
  };
  
  export default TableDashboard;
  