import React from 'react';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import { SparkLineChart } from '@mui/x-charts/SparkLineChart';


const LineCharts = ({Data}) => {
  return ( 
    <Stack  sx={{ width: '100%'}} >
      <Box sx={{ flexGrow: 1 }} className='d-flex justify-content-center'>
        <SparkLineChart
          data={[1, 4, 2, 5, 7, 2, 4, 6]}
          height={80}
          width={200}
          showHighlight={true}
          showTooltip={true}
        />
      </Box>
    </Stack>
  )
}
export default LineCharts;
