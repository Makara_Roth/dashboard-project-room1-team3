import React from 'react'
import { Box, Grid, Typography, Button } from '@mui/material';
import { Container } from '@mui/material';
import SearchField from '../../../../Globals/helpers/components/SearchField';
import { Table, TableContainer } from '@mui/material';
import TableDashboard from './TableDasboard';
import useDashboard from '../core/Action';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { TableCell } from '@mui/material';
import ListAllUserPerformance from './ListAllUserPerformence';
import { useDispatch } from 'react-redux';
import { setParams } from '../core/Reducer';
import GlobalPagination from '../../../../Globals/helpers/components/Pagination';
import StartDateSelector from './StartDateSelector';
import EndDateSelector from './EndDateSelector';
import DateMonthAndYearSelector from './DateMonthAndYearSelector';
import { TableBody } from '@mui/material';
import { TableRow } from '@mui/material';



function UserPerformance() {
  const { getSoldStaffs, handlePage, handleFilter } = useDashboard()
  const dispatch = useDispatch()
  const { params, reportStaff, paging } = useSelector(state => state.dashboard)
  const { page } = params;

  useEffect(() => {

    getSoldStaffs()

    // eslint-disable-next-line
  }, [params, dispatch])

  const handleSearch = (value) => {
    dispatch(setParams({ query: value }))
  }
  const handleChange = (event, value) => {
    handlePage(value);
  };

  return (
    <Container maxWidth="lg"
      sx={{
        marginTop: 3
      }}>
      <Typography variant="h4" className="fw-bold">User Performance</Typography>
      <Box>
        <Grid
          container
          item
          xs={12}
          display="flex"
          justifyContent="flex-end"
          sx={{ mb: 2 }}
        >
          <Button variant="contained">Export PDF</Button>
        </Grid>
      </Box>
      <Grid container sx={{ mt: 2 }}>
        <Grid item xs={12} md={6}>
          <Box sx={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
            <SearchField
              title="Search Name..."
              params={params}
              onChange={(e) => handleSearch(e.target.value)}
              fullWidth
            />
          </Box>
        </Grid>
        <Grid
          container
          item xs={12} md={6}>
          <Grid

            sx={{
              display: 'flex',
              mt: 3,
              flexDirection: { xs: 'column', md: 'row' },
              justifyContent: {
                xs: 'start',
              },
              gap: 2,
            }} >
            <Grid item xs={3} >
              <DateMonthAndYearSelector />
            </Grid>
            <Grid item xs={3} >
              <StartDateSelector />
            </Grid>
            <Grid item xs={3} >
              <EndDateSelector />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Box sx={{ overflow: 'auto', width: { xs: '300px', sm: '500px', md: '950px', lg: '1100px' } }}>
        <TableContainer>
          <Table
            aria-label="simple table"
            sx={{
              whiteSpace: 'nowrap',
            }}
          >
            <TableDashboard />
            <TableBody>
              {reportStaff.length > 0 ? (
                reportStaff.map((rf, index) => (
                  <ListAllUserPerformance
                    key={index}
                    cashier={rf.cashier}
                    paymentMethod={rf.paymentMethod}
                    totalPrice={rf.totalPrice}
                  />
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan={8} className="text-center">
                    <Typography
                      sx={{
                        fontSize: '18px',
                        fontWeight: '600',
                      }}
                    >
                      User not found
                    </Typography>
                  </TableCell>
                  </TableRow>
              )}
              </TableBody>
          </Table>
        </TableContainer>
        <Box sx={{ marginTop: 2 }}>
          <GlobalPagination
            params={params}
            totalPage={paging?.totalPage}
            page={page}
            handleChange={handleChange}
            handleFilter={handleFilter}
          />
        </Box>
      </Box>
    </Container>
  )
}

export default UserPerformance
