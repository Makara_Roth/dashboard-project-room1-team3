
import { createSlice } from '@reduxjs/toolkit';


const getCurrentDate = () => {
  const date = new Date();
  const year = date.getFullYear();
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  return `${year}:${month}`;
}

const dashboardSlice = createSlice({
    name: "dashboard",
    initialState: {
        message: {},
        reportStaff:{},
        paging:{},
        params:{
            start: undefined, 
            end: undefined,
            month: getCurrentDate(),
            size: 12,
            page: 1,
            query: undefined,
            order: "desc",
        },
        
    },
    reducers: {
        setMessage: (state, action) => {
            state.message = action.payload;
        },
        setParams: (state, action) => {
            state.params = { ...state.params, ...action.payload };
        },
        setReportStaff: (state, action) => {
            state.reportStaff = action.payload;
        },
        setPaging: (state, action) => {
            state.paging = action.payload;
        },
    }
})

export const { setMessage , setParams , setReportStaff , setPaging , setIncomeToday} = dashboardSlice.actions;

export default dashboardSlice.reducer; 