import { useDispatch, useSelector } from "react-redux";
import { reqReportStaff } from "./Request";
import { setReportStaff, setParams , setPaging } from "./Reducer";
import { toast } from 'react-toastify';

const useDashboard = () => {
  const dispatch = useDispatch();
  const { params  } = useSelector((state) => state.dashboard);
  const { sortBy } = params;

  const getSoldStaffs = async () => {
   try{
    await reqReportStaff({ params }).then((res) =>{
      dispatch(setReportStaff(res.data.data))
        dispatch(setPaging(res.data.paging))}
    );
   }catch(e) {
    toast.error(e.message)
   }
  };

  const handleSearch = (text) => {
    dispatch(setParams({ page: 1, query: text }));
  };
  const handleSort = (field) => {
    if (sortBy === field) {
      dispatch(setParams({ order: sortBy === "asc" }));
    } else {
      dispatch(setParams({ sort: field, order: "asc" }));
    }
  }
  const handlePage = (page) => dispatch(setParams({ page: page }));

  const handleFilter = (name, value) => {
    dispatch(setParams({ [name]: value }));
  }

  return { getSoldStaffs, handleSearch , handleSort  , handlePage , handleFilter };
};

export default useDashboard;
