import axios from "axios";
import { getAuth } from "../../../Auth/helper/authHelper";
// get Food Category
const fetchCategories = (params) => {
  return axios.get("/api/categories", {
    params: params
  });
};
const postCategory = (newCategory) => {
  return axios.post(
    "/api/categories",
    {
      name: newCategory
    },
    {
      headers: {
        Authorization: `Bearer ${getAuth()}`,
      },
    },
  );
};
const reqDeleteCategoryById = (id) => {
  return axios.delete(`/api/categories/${id}`)
}


const reqUpdatefoodCategory = (updatedCategoryData) => {
  return axios.put(`/api/categories/${updatedCategoryData.id}`, updatedCategoryData)
};

export {

  fetchCategories,
  postCategory,
  reqDeleteCategoryById,
  reqUpdatefoodCategory,

};