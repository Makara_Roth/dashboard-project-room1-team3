import {
    fetchCategories,
    postCategory,
    reqUpdatefoodCategory,
    reqDeleteCategoryById,
} from "./request";
import { setErrorMessage, setFoodCategory, setParams } from "./Reducer";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

// import { setFood, setFoodCategory, setErrorMessage } from "./Reducer";

const useFoodCategory = () => {
    const dispatch = useDispatch();
    const { params } = useSelector((state) => state.category);

    // ========  fetch Food Categories    ===================

    const getFoodCategories = () => {
        try {
            fetchCategories(params).then((res) => {
                dispatch(setFoodCategory(res.data));
            });
        } catch (err) {
            dispatch(setErrorMessage(err));
        }
    };

    const AddCategory = async (newCategory) => {
        try {
            await postCategory(newCategory);
        } catch (error) {
            // console.error("Error adding object:", error);
        }
    };

    const deleteCategory = (id) => {
        return reqDeleteCategoryById(id)
            .then((res) => {
                toast.success("Category deleted successfully");
            })
            .catch((error) => {
                toast.error("Failed to delete category");
            });
    };

    const editFoodCategory = (updatedCategoryData) => {
        return reqUpdatefoodCategory(updatedCategoryData)
            .then((res) => {
                toast.success("Category updated successfully");
            })
            .catch(() => {
                toast.error("Failed to update category");
            });
    };
    const handleSearch = (text) => {
        dispatch(setParams({ page: 1, query: text }));
        console.log(text);
    };

    const handlePage = (page) => dispatch(setParams({ page: page }));

    const handleSort = (field) => {
        // if (sort === field) {
        //     dispatch(setParams({ order: sort === "asc" ? "asc" : "desc" }));
        // } else {
        //     dispatch(setParams({ sort: field, order: "asc" }));
        // }
        const newOrder = params.order === "asc" ? "desc" : "asc";
        dispatch(setParams({ sort: field, order: newOrder }));
    };

    const handleFilter = (name, value) => {
        dispatch(setParams({ [name]: value }));
    };

    return {
        getFoodCategories,
        AddCategory,
        deleteCategory,
        editFoodCategory,
        handleSearch,
        handlePage,
        handleSort,
        handleFilter,
    };
};

export default useFoodCategory;
