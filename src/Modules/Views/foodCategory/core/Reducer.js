import { createSlice } from "@reduxjs/toolkit";
const postCategory = {
    name: "",
};
const initialState = {
    foodCategory: [],
    errorMessage: "",
    totalPage: 0,
    postCategory: postCategory,
    addFoodCategoryData: null,
    params: {
        size: 12,
        page: 1,
        query: "",
        sort: "id",
        order: "desc",
    }
}


const foodCategorySlide = createSlice({
    name: "foodCategory",
    initialState,
    reducers: {
        setFoodCategory: (state, action) => {
            state.foodCategory = action.payload.data;
            state.totalPage = action.payload.paging.totalPage;
        },
        AddCategory: (state, action) => {
            state.postCategory = action.payload;
        },
        editCategory: (state, action) => {
            const { categoryId, updatedCategoryData } = action.payload;
            state.foodCategory = state.foodCategory.map(category => category.id === categoryId ? { ...category, ...updatedCategoryData } : category);
        },
        deleteCategory: (state, action) => {
            const deletedId = action.payload
            state.foodCategory = state.foodCategory.filter(category => category.id !== deletedId);
        },
        setErrorMessage: (state, action) => {
            state.errorMessage = action.payload;
        },
        setAddFoodCategoryData: (state, action) => {
            state.addFoodCategoryData = action.payload;
        },
        setParams: (state, action) => {
            state.params = { ...state.params, ...action.payload }
        }
    },
});

export const { setFoodCategory, AddCategory, setErrorMessage, editCategory, setAddFoodCategoryData, setParams } = foodCategorySlide.actions;

export default foodCategorySlide.reducer;
