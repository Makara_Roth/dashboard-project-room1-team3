import { Col, Form, Row } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import useFoodCategory from "../core/Action";
import { Box, Button, Modal, Typography } from "@mui/material";
import { setAddFoodCategoryData } from '../core/Reducer';

// import Modal from "react-bootstrap/Modal";


const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};


const AddfoodCategoryModal = ({ handleClose, addCategoryData }) => {
    const { AddCategory } = useFoodCategory()
    const [newCategory, setnewCategory] = useState({});
const dispatach = useDispatch()
    useEffect(() => {
        setnewCategory((prevData) => ({
            ...prevData,
            ...addCategoryData,
        }));
    }, [addCategoryData]);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setnewCategory({ ...newCategory, [name]: value });
    };

    const handleSave = async (e) => {
        e.preventDefault();
        try {
            await AddCategory(newCategory).then(() => {
                dispatach(setAddFoodCategoryData(null))
            })
            setnewCategory("");
            

        } catch (e) {
            console.log(e);
        }   

        setnewCategory({
           
        });
    };


    return (
        <Modal open={true} onClose={handleClose}>
            <Box sx={style}>
                <Typography
                    id="modal-modal-title"
                    variant="h4"
                    component="h2"
                    style={{ textAlign: "left", marginBottom: 5 }}
                >
                    Add Category
                </Typography>
                <Row>
                    <Col md={12}>
                        <Form.Group className="mb-3" controlId="formBasicName">
                            <Form.Label>
                                Name:
                            </Form.Label>
                            <Form.Control
                                type="text"
                                name="name"
                                onChange={(e) => setnewCategory(e.target.value)}
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <div className="mt-4 text-end">
                    <Button
                        style={{
                            backgroundColor: "#f1f1f1",
                            color: "",
                            marginRight: 3,
                        }}
                        onClick={handleClose}
                    >
                        Cancel
                    </Button>
                    <Button style={{
                        backgroundColor: "#4085ad",
                        color: "#ffff",
                    }} onClick={handleSave}>Save</Button>
                </div>
            </Box>  
        </Modal>
    );
}

export default AddfoodCategoryModal