import React, { useEffect, useState } from "react";
import {
  Box,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Button,
  Table,
  TableSortLabel,
  Container,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";
import SearchField from "../../../../Globals/helpers/components/SearchField";
import AddfoodCategoryModal from "./AddfoodCategoryModal";
import { setAddFoodCategoryData } from "../core/Reducer";
import { IconButton, TableBody, Tooltip } from "@mui/material";
import { ReactComponent as Detele } from "../../../../assets/images/trash.svg"
import { ReactComponent as Edit } from "../../../../assets/images/edit.svg"
import DashboardCard from "../../../Components/shared/DashboardCard";
import useFoodCategory from "../core/Action";
import EditfoodCategoryModal from "./EditfoodCategoryModal";
import GlobalPagination from "../../../../Globals/helpers/components/Pagination";
import DeleteModal from "../../../../Globals/helpers/alert/DeleteModal";

const FoodsCategorise = () => {
  const {
    foodCategory,
    addFoodCategoryData = null,
    params,
    totalPage,
  } = useSelector((state) => state.category);
  const {
    getFoodCategories,
    editFoodCategory,
    deleteCategory,
    handleSearch,
    handlePage,
    handleSort,
    handleFilter,
  } = useFoodCategory();
  const [foodCategoryData, setFoodCategoryData] = useState(null);
  const [categoryIdToDelete, setCategoryIdToDelete] = useState(null);

  const dispatch = useDispatch();
  const { page, sort, order } = params;
  // const navigate = useNavigate()

  useEffect(() => {
    getFoodCategories();
    // eslint-disable-next-line
  }, [
    foodCategoryData,
    categoryIdToDelete,
    addFoodCategoryData,
    dispatch,
    params,
  ]);
  
  const onAddHandler = (addCategoryData) => {
    dispatch(setAddFoodCategoryData(addCategoryData));
  };

  const onEditHandler = (categoryId, foodCategoryData) => {
    setFoodCategoryData(foodCategoryData);
  };

  const handleEditSubmit = (updatedCategoryData) => {
    return editFoodCategory(updatedCategoryData).then(() => {
      setFoodCategoryData(null);
    });
  };
  const handleDelete = (id) => {
    setCategoryIdToDelete(id);
  };
  const confirmDelete = () => {
    deleteCategory(categoryIdToDelete).then(() => {
      setCategoryIdToDelete(null);
    });
  };

  const handleChange = (event, value) => {
    handlePage(value);
  };

  return (
    <Container maxWidth="lg" className="p-0">
      <div className="col-xl-4 mb-3">
        <h4 className="fw-bold">FoodCategory</h4>
        <Breadcrumb title="Dashboard" subtitle="FoodsCategorise" />
      </div>
      <DashboardCard>
        <div className="d-flex  justify-content-between">
          <div style={{ width: "10rem" }}>
            <SearchField
              title="Search Name..."
              params={params}
              onChange={(e) => handleSearch(e.target.value)}
            />
          </div>

          <div>
            <Button
              style={{ backgroundColor: "#4085ad", color: "#ffff" }}
              onClick={() => onAddHandler(foodCategory)}
            >
              Add Category
            </Button>
          </div>
          {addFoodCategoryData && (
            <AddfoodCategoryModal
              // show={}
              handleClose={() => dispatch(setAddFoodCategoryData(null))}
              addCategoryData={addFoodCategoryData}
              // onSubmit={handleAddSubmit}
            />
          )}
        </div>
        <Box sx={{ overflow: "auto", width: { xs: "280px", sm: "auto" } }}>
          <Table
            aria-label="simple table"
            sx={{
              whiteSpace: "nowrap",
              mt: 2,
            }}
          >
            <TableHead>
              <TableRow>
                <TableCell>
                  <TableSortLabel
                    active={sort === "id"}
                    direction={order}
                    onClick={() => handleSort("id")}
                  >
                    <Typography variant="subtitle2" fontWeight={600}>
                      ID
                    </Typography>
                  </TableSortLabel>
                </TableCell>

                <TableCell>
                  <TableSortLabel
                    active={sort === "name"}
                    direction={order}
                    onClick={() => handleSort("name")}
                  >
                    <Typography variant="subtitle2" fontWeight={600}>
                      Name
                    </Typography>
                  </TableSortLabel>
                </TableCell>

                <TableCell>
                  <Typography variant="subtitle2" fontWeight={600}>
                    CreatedBy
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography variant="subtitle2" fontWeight={600}>
                    Action
                  </Typography>
                </TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {foodCategory.length > 0 ? (
                foodCategory && foodCategory.map((foodCategory) => (
                  <TableRow key={foodCategory.id}>
                    <TableCell>
                      <Typography
                        variant="subtitle2" fontWeight={600}
                      >
                        {foodCategory.id}
                      </Typography>
                    </TableCell>

                    <TableCell>
                      <Typography
                        variant="subtitle2" fontWeight={600}
                      >
                        {foodCategory.name}
                      </Typography>
                    </TableCell>

                    <TableCell>
                      <Typography
                        variant="subtitle2" fontWeight={600}
                      >
                        {foodCategory.createdBy.username}
                      </Typography>
                    </TableCell>

                    <TableCell align="center">
                      <Tooltip title="Edit" placement="bottom">
                        <IconButton
                          aria-label="edit"
                          color="primary"
                          onClick={() =>
                            onEditHandler(foodCategory.id, foodCategory)
                          }
                        >
                          <Edit />
                        </IconButton>
                      </Tooltip>

                      <Tooltip title="Delete" placement="bottom">
                        <IconButton
                          onClick={() => handleDelete(foodCategory.id)}
                          aria-label="delete"
                          color="error"
                        >
                          <Detele />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan={4} className="text-center">
                    <Typography
                      sx={{
                        fontSize: "18px",
                        fontWeight: "600",
                      }}
                    >
                      Food category not found
                    </Typography>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>

            <EditfoodCategoryModal
              show={foodCategoryData != null}
              handleClose={() => setFoodCategoryData(null)}
              foodCategoryData={foodCategoryData}
              onSubmit={handleEditSubmit}
              handleFilter={handleFilter}
            />
          </Table>
        </Box>
        {/* Pagination */}
        <GlobalPagination
          params={params}
          totalPage={totalPage}
          page={page}
          handleChange={handleChange}
          handleFilter={handleFilter}
        />
      </DashboardCard>
      
      <DeleteModal
        title="Are you sure want to delete this category?"
        open={categoryIdToDelete != null}
        onClose={() => setCategoryIdToDelete(null)}
        confirmDelete={confirmDelete}
      />
    </Container>
  );
};

export default FoodsCategorise;
