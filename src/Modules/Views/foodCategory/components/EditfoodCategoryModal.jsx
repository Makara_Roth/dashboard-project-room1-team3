import { Col, Form, Row } from "react-bootstrap";
import { useEffect, useState } from "react";
import { Box, Button, Modal, Typography } from "@mui/material";
// import Modal from "react-bootstrap/Modal";

const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    borderRadius: "5px",
};

const initialData = {
    name: "",
};

const EditfoodCategoryModal = ({ show, handleClose, foodCategoryData, onSubmit }) => {
    const [editCategory, setEditCategory] = useState({
        ...initialData,
    });

    useEffect(() => {
        setEditCategory((prevData) => ({
            ...prevData,
            ...foodCategoryData,
        }));
    }, [foodCategoryData]);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setEditCategory({ ...editCategory, [name]: value });
    };

    const handleEdit = (e) => {
        e.preventDefault();
        onSubmit(editCategory);

        setEditCategory({
            ...initialData,
        });
    };

    return (
        <Modal open={show} onClose={handleClose}>
            <Box sx={style}>
                <Typography
                    id="modal-modal-title"
                    variant="h4"
                    component="h2"
                    style={{ textAlign: "left", marginBottom: 5 }}
                >
                    Edit Category
                </Typography>

                <Row>
                    <Col md={12}>
                        <Form.Group className="mb-3" controlId="formBasicName">
                            <Form.Label>
                                Name:
                            </Form.Label>
                            <Form.Control
                                type="text"
                                name="name"
                                value={editCategory.name || ""}
                                onChange={handleChange}
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <div className="mt-4 text-end">
                    <Button
                        style={{
                            backgroundColor: "#f1f1f1",
                            color: "",
                            marginRight: 3,
                        }}
                        onClick={handleClose}
                    >
                        Cancel
                    </Button>
                    <Button style={{
                        backgroundColor: "#4085ad",
                        color: "#ffff",
                    }} onClick={handleEdit}>Save</Button>
                </div>
            </Box>
        </Modal>
    );


};

export default EditfoodCategoryModal;