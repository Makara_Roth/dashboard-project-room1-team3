import { createSlice } from "@reduxjs/toolkit";

const getCurrentDate = () => {
  const date = new Date();
  const year = date.getFullYear();
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const day = date.getDate().toString().padStart(2, "0"); // Get and pad the day
  return `${year}-${month}-${day}`; // Use '-' as the separator
};


const incomeSlice = createSlice({
  name: "incomes",
  initialState: {
    message: {},
   dailyIncome : [],
   monthlyIncome:[],
    params: {
      day: getCurrentDate(),
      start: null,
      end: null,
      month: "2024:05",
      size: 12,
      page: 1,
      query: null,
      order: null,
    },
  },
  reducers: {
    setMessage: (state, action) => {
      state.message = action.payload;
    },
    setParams: (state, action) => {
      state.params = { ...state.params, ...action.payload };
    },
    setDailyIncome: (state, action) => {
      state.dailyIncome = action.payload
    },
    setMonthlyIncome: (state, action) => {
        state.monthlyIncome = action.payload
      },
  },
});

export const { setMessage, setParams, setDailyIncome , setMonthlyIncome } = incomeSlice.actions;

export default incomeSlice.reducer;
