import { reqIncomeReports } from "./Request";
import { setDailyIncome, setMessage, setMonthlyIncome } from "./Reducer";

const useIncome = () => {
  // const {params} = useSelector(state => state.incomes)

  const fetchDailyIncome = (day) => async (dispatch) => {
    try {
     const res =  await reqIncomeReports({day})
        dispatch(setDailyIncome(res.data.data));
    } catch (e) {
      
    }
  };
  const fetchMonthlyIncome = (month) => async (dispatch) => {
    try {
      const res =  await reqIncomeReports({month})
        dispatch(setMonthlyIncome(res.data.data));
    } catch (e) {}
  };
  
  return { fetchDailyIncome, fetchMonthlyIncome };
};
export { useIncome };
