import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import { Card, CardContent, Typography, Box } from '@mui/material';
import { ArcElement, Chart as ChartJS, Tooltip, Legend } from "chart.js";


ChartJS.register(ArcElement, Tooltip, Legend);

const IncomeChart = ({ value }) => {

    const data = {
        labels: ['Totals Price $'],
        datasets: [
            {
                data: [value],
                backgroundColor: ['#3f51b5', '#e0e0e0'],
                hoverBackgroundColor: ['#303f9f', '#d5d5d5'],
                borderWidth: 1,
            },
        ],
    };
    const options = {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            legend: {
                display: false,
            },
        },
    };
    return (<>

        <Card  >
            <CardContent>
                <Typography variant="h6" component="div"
                    sx={{
                        mb: 2,
                        display: 'flex',
                        justifyContent: 'center',
                    }}
                >
                    Monthly Income
                </Typography>
                {data?.datasets[0]?.data[0] === undefined ? <>
                    <Box sx={{ display: 'flex', justifyContent: 'center', color: 'red' }}>
                        <Typography
                            sx={{
                                fontSize: '18px',
                                fontWeight: '600',
                            }}
                        >
                            Income not found
                        </Typography>
                    </Box>
                </> :
                    <>
                        <Box sx={{height: '150px'}}>
                            <Doughnut data={data} options={options} />
                        </Box>
                    </>}
            </CardContent>
        </Card>
    </>
    );
}
export default IncomeChart;