import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Table,
  TableContainer,
  Typography,
  TableCell,
  Container,
  TableRow,
  TableBody,
} from "@mui/material";
import DashboardCard from "../../../Components/shared/DashboardCard";
import { useDispatch, useSelector } from "react-redux";
import useUser from "../core/Action";
import PostAllUser from "./PostAllUser";
import TableHeadUser from "./TableHead";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";
import SearchField from "../../../../Globals/helpers/components/SearchField";
import EditUserModal from "./EditUserModal";
import AddUserModal from "./AddUserModal";
import { setUserIdToDelete } from "../core/Reducer";
import GlobalPagination from "../../../../Globals/helpers/components/Pagination";
import DeleteModal from "../../../../Globals/helpers/alert/DeleteModal";
import Reset from "../../../../Globals/helpers/components/Reset";

const User = () => {
  const {
    getCurrentUser,
    editUserById,
    deleteUserById,
    getRole,
    handlePage,
    handleSearch,
    handleFilter,
  } = useUser();
  const { currentUser, paging, params, totalPage, userIdToDelete } =
    useSelector((state) => state.user);
    const { userPermission } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const { page } = params;
  const [userData, setUserData] = useState(null);
  const [showAddUserModal, setShowAddUserModal] = useState(false);

  useEffect(() => {
    getCurrentUser();
    getRole();
    // eslint-disable-next-line
  }, [params, userData, userIdToDelete]);

  const onEditHandler = (userData) => {
    setUserData(userData);
  };

  const handleEditSubmit = (updatedUserData) => {
    return editUserById(updatedUserData).then(() => {
      setUserData(null);
    });
  };

  const onDeleteHandler = (id) => {
    dispatch(setUserIdToDelete(id));
    dispatch(setUserIdToDelete(id));
  };

  const confirmDelete = () => {
    deleteUserById(userIdToDelete).then(() => {
      dispatch(setUserIdToDelete(null));
    });
  };

  const handleChange = (event, value) => {
    handlePage(value);
  };

  const renderedUserList = currentUser.map((user) => {
    return (
      <PostAllUser
        key={user.id}
        id={user.id}
        name={user.name}
        gender={user.gender}
        email={user.email}
        role={user.role}
        createdDate={user.createdDate}
        status={user.status}
        onEdit={() => onEditHandler(user)}
        onDelete={() => onDeleteHandler(user.id)}
      />
    );
  });

  return (
    <Container maxWidth="lg" className="p-0">
      <h4 className="fw-bold">Users</h4>
      <div className="d-flex justify-content-between">
        <Breadcrumb title="Dashboard" subtitle="List Users" />
        <p className="fw-bold">Total Users: {paging.totals}</p>
      </div>
      <DashboardCard>
        <div className="row">
          <div className="col-12 col-lg-6 d-flex">
          <SearchField
            title="Search Name..."
            params={params}
            onChange={(e) => handleSearch(e.target.value)}
          />
          <Reset params={params} handleFilter={handleFilter}/>
          </div>
          <div className="col-12 col-lg-6 d-flex justify-content-lg-end">
            <div>
            {userPermission?.find((per) => per.name === "create-user")
              ?.status === 1 && (
              <>
                <Button
                  style={{ backgroundColor: "#4085ad", color: "#ffff" }}
                  onClick={() => setShowAddUserModal(true)}
                >
                  Add User
                </Button>
              </>
            )}
            </div>
          </div>
        </div>

        <Box
          sx={{
            overflow: "auto",
            width: { xs: "300px", sm: "500px", md: "950px", lg: "1100px" },
          }}
        >
          <TableContainer>
            <Table
              aria-label="simple table"
              sx={{
                // whiteSpace: "nowrap",
                mt: 2,
              }}
            >
              <TableHeadUser />
              <TableBody>
                {currentUser.length > 0 ? (
                  renderedUserList
                ) : (
                  <TableRow>
                    <TableCell colSpan={8} className="text-center">
                      <Typography
                        sx={{
                          fontSize: "18px",
                          fontWeight: "600",
                        }}
                      >
                        User not found
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          {/* Pagination */}
          <GlobalPagination
            params={params}
            totalPage={totalPage}
            page={page}
            handleChange={handleChange}
            handleFilter={handleFilter}
          />
        </Box>
      </DashboardCard>
      {/* Add User Modal */}
      <AddUserModal
        show={showAddUserModal}
        handleClose={() => setShowAddUserModal(false)}
      />
      {/* Edit User Modal */}
      {userData && (
        <EditUserModal
          handleClose={() => setUserData(null)}
          userData={userData}
          onSubmit={handleEditSubmit}
        />
      )}
      {/* Delete User Modal */}
      
      <DeleteModal
        title="Are you sure want to delete this user?"
        open={userIdToDelete != null}
        onClose={() => dispatch(setUserIdToDelete(null))}
        confirmDelete={confirmDelete}
      />
    </Container>
  );
};

export default User;
