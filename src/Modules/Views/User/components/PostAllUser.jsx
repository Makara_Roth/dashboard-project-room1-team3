import React, { useState } from "react";
import {
  Chip,
  IconButton,
  Popover,
  TableCell,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import { ReactComponent as Delete } from "../../../../assets/images/trash.svg";
import { ReactComponent as Edit } from "../../../../assets/images/edit.svg"
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import { MoreVert } from "@mui/icons-material";
import { useSelector } from "react-redux";

const PostAllUser = ({
  id,
  name,
  email,
  gender,
  role,
  createdDate,
  status,
  onEdit,
  onDelete,
}) => {
  const { userPermission } = useSelector((state) => state.auth);
  const formattedCreatedDate = dayjs(createdDate).format("MM/DD/YYYY h:mm A"); // Example: April 2, 2024 11:21 AM
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const DU = open ? "simple-popover" : undefined;
  return (
    <TableRow>
      <TableCell>
        <Typography
         variant="subtitle2" fontWeight={600}
        >
          {id}
        </Typography>
      </TableCell>

      <TableCell>
        <Tooltip title="user detail">
          <Typography
            variant="subtitle2" fontWeight={600}
          >
            <Link
              style={{ textDecoration: "none", color: "#2A3547"}}
              to={`/user/detail/${id}`}
            >
              {name}
            </Link>
          </Typography>
        </Tooltip>
      </TableCell>

      <TableCell>
        <Typography
         variant="subtitle2" fontWeight={600}
        >
          {gender}
        </Typography>
      </TableCell>

      <TableCell>
        <Typography
         variant="subtitle2" fontWeight={600}
        >
          {email}
        </Typography>
      </TableCell>

      <TableCell>
        <Typography
         variant="subtitle2" fontWeight={600}
        >
          {role.name}
        </Typography>
      </TableCell>

      <TableCell>
        <Typography
         variant="subtitle2" fontWeight={600}
        >
          {formattedCreatedDate}
        </Typography>
      </TableCell>

      <TableCell>
        <Chip
          sx={{
            px: "4px",
            backgroundColor: status ? "#4085ad" : "#f44336",
            color: "#ffffff",
          }}
          size="small"
          label={status ? "Active" : "Inactive"}
        ></Chip>
      </TableCell>
      {(userPermission?.find((per) => per.name === "edit-user")?.status === 1 ||
        userPermission?.find((per) => per.name === "delete-user")?.status ===
          1) && (
        <TableCell align="center">
          <IconButton
            className=" text-secondary"
            aria-describedby={DU}
            onClick={handleClick}
          >
            <MoreVert />
          </IconButton>
          <Popover
            id={DU}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
          >
            <>
              {userPermission?.find((per) => per.name === "edit-user")
                ?.status === 1 && (
                <IconButton aria-label="edit" color="primary" onClick={onEdit}>
                  <Edit />
                </IconButton>
              )}
              {userPermission?.find((per) => per.name === "delete-user")
                ?.status === 1 && (
                <IconButton
                  onClick={onDelete}
                  aria-label="delete"
                  color="error"
                >
                  <Delete />
                </IconButton>
              )}
            </>
          </Popover>
        </TableCell>
      )}
    </TableRow>
  );
};

export default PostAllUser;
