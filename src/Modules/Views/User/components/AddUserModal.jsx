import {
  Modal,
  Box,
  Typography,
  Button,
  TextField,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import { useSelector } from "react-redux";
import useUser from "../core/Action";

const AddUserModal = ({ show, handleClose }) => {
  const { roles } = useSelector((state) => state.user);
  const [newUser, setNewUser] = useState({});

  useEffect(() => {
    const currentDataTime = new Date();
    const formattedDate = currentDataTime?.toISOString()?.slice(0, -5) + "Z";
    setNewUser((prevUserData) => ({
      ...prevUserData,
      hire_date: formattedDate,
      role_id: null,
    }));
  }, []);
  const handleInputChange = (field, value) => {
    setNewUser((prevUserData) => ({
      ...prevUserData,
      [field]: value,
    }));
  };
  const { addUser } = useUser();

  const handleSubmit = async () => {
    addUser(newUser);
    clearForm();
  };
  const clearForm = () => {
    setNewUser({
      name: "",
      username: "",
      gender: "",
      role_id: "",
      salary: "",
      phone: "",
      email: "",
      password: "",
      confrim_password: "",
    });
  };

  return (
    <Modal open={show} onClose={handleClose}>
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: 600,
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          borderRadius: "5px",
          // Responsive styles
          "@media (max-width: 600px)": {
            // xs and sm screens
            width: "90%", // Adjusted width
            maxWidth: "none", // No maximum width
          },
          "@media (min-width: 600px) and (max-width: 960px)": {
            // md screens
            width: "80%", // Adjusted width
            maxWidth: 500, // Maximum width
          },
          "@media (min-width: 960px)": {
            // lg screens
            width: 600, // Fixed width
            maxWidth: "none", // No maximum width
          },
        }}
      >
        <Form>
          <Typography
            id="modal-modal-title"
            variant="h4"
            component="h2"
            style={{ textAlign: "left" }}
          >
            Add User
          </Typography>
          <div className={"row"}>
            <div className={"col-md-6"}>
              <div className="form-group mt-2">
                <TextField
                  fullWidth
                  sx={{ borderRadius: 2 }}
                  label="Name"
                  type="text"
                  onChange={(e) => handleInputChange("name", e.target.value)}
                />
              </div>
            </div>
            <div className={"col-md-6"}>
              <div className="form-group mt-2">
                <TextField
                  fullWidth
                  sx={{ borderRadius: 2 }}
                  label="Username"
                  type="text"
                  onChange={(e) =>
                    handleInputChange("username", e.target.value)
                  }
                />
              </div>
            </div>
          </div>
          <div className={"row"}>
            <div className={"col-md-6"}>
              <div className="form-group mt-3">
                <FormControl fullWidth sx={{ borderRadius: 2 }}>
                  <InputLabel>Gender</InputLabel>
                  <Select
                    label="Gender"
                    onChange={(e) =>
                      handleInputChange("gender", e.target.value)
                    }
                  >
                    <MenuItem value="Male">Male</MenuItem>
                    <MenuItem value="Female">Female</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>
            <div className={"col-md-6"}>
              <div className="form-group mt-3">
                <FormControl fullWidth sx={{ borderRadius: 2 }}>
                  <InputLabel>Role</InputLabel>
                  <Select
                    label="Role"
                    onChange={(e) =>
                      handleInputChange("role_id", parseInt(e.target.value))
                    }
                  >
                    {roles?.map((role) => (
                      <MenuItem key={role.id} value={role.id}>
                        {role.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
            </div>
            <div className={"col-xl-6"}>
              <div className="form-group mt-3">
                <TextField
                  id="filled-basic"
                  variant="outlined"
                  fullWidth
                  sx={{ borderRadius: 2 }}
                  label="Enter salary"
                  type="text"
                  onChange={(e) => handleInputChange("salary", e.target.value)}
                />
              </div>
            </div>
            <div className={"col-xl-6"}>
              <div className="form-group mt-3">
                <TextField
                  fullWidth
                  sx={{ borderRadius: 2 }}
                  label="Enter Phone Number"
                  type="phone"
                  onChange={(e) => handleInputChange("phone", e.target.value)}
                />
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group mt-3">
                <TextField
                  fullWidth
                  sx={{ borderRadius: 2 }}
                  label="Enter email"
                  type="email"
                  onChange={(e) => handleInputChange("email", e.target.value)}
                />
              </div>
            </div>
            <div className={"col-md-6"}>
              <div className="form-group mt-3">
                <TextField
                  fullWidth
                  sx={{ borderRadius: 2 }}
                  label="Enter password"
                  type="password"
                  onChange={(e) =>
                    handleInputChange("password", e.target.value)
                  }
                />
              </div>
            </div>
            <div className={"col-md-6"}>
              <div className="form-group mt-3">
                <TextField
                  fullWidth
                  sx={{ borderRadius: 2 }}
                  label="Enter confirm password"
                  type="password"
                  onChange={(e) =>
                    handleInputChange("confirm_password", e.target.value)
                  }
                />
              </div>
            </div>
            <div className="mt-4 text-end">
              <Button
                style={{
                  backgroundColor: "#f1f1f1",
                  color: "",
                  marginRight: 3,
                }}
                onClick={handleClose}
              >
                Cancel
              </Button>
              <Button
                style={{
                  backgroundColor: "#4085ad",
                  color: "#ffff",
                }}
                onClick={handleSubmit}
              >
                Create User
              </Button>
            </div>
          </div>
        </Form>
      </Box>
    </Modal>
  );
};

export default AddUserModal;
