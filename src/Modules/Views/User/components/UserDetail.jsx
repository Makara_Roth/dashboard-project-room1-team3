import { useParams } from "react-router";
import {
  Avatar,
  Chip,
  Grid,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import DashboardCard from "../../../Components/shared/DashboardCard";
import { useSelector } from "react-redux";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import SkeletonUser from "../../../../Globals/helpers/components/SkeletonUser";
import useUser from "../core/Action";
import { ReactComponent as NoData } from "../../../../assets/images/report-search_1.svg";
import dayjs from "dayjs";

const UserDetail = () => {
  const { id } = useParams();
  const { user, loading } = useSelector((state) => state.user);
  const { getUserById } = useUser();
  const formattedDate = dayjs(user ? user.hireDate : "").format(
    "MM/DD/YYYY h:mm A"
  ); // Example: April 2, 2024 11:21 AM
  useEffect(() => {
    getUserById(id);
    // eslint-disable-next-line
  }, [id]);

  return (
    <>
      <div className="mb-3">
        <h4 className="fw-bold">User Detail</h4>
      </div>
      {loading ? (
        <SkeletonUser />
      ) : (
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <DashboardCard>
              <Link to="/user" style={{ color: "black" }}>
                <Tooltip title="back">
                  <ArrowBackIcon />
                </Tooltip>
              </Link>

              <div className="mb-4">
                <Avatar
                  src={user ? user.avatar : ""}
                  alt="Profile"
                  sx={{
                    width: 100,
                    height: 100,
                    margin: "auto",
                  }}
                />
              </div>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Typography
                    variant="h3"
                    component="div"
                    color="text.secondary"
                  >
                    Information
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    label="Name"
                    value={user ? user.name : ""}
                    InputProps={{ readOnly: true }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    label="Gender"
                    value={user ? user.gender : ""}
                    InputProps={{ readOnly: true }}
                  />
                </Grid>

                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    label="Role"
                    value={user ? user.role.name : ""}
                    InputProps={{ readOnly: true }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    label="Salary"
                    value={`${user ? user.salary : ""}$`}
                    InputProps={{ readOnly: true }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    label="HireDate"
                    value={formattedDate}
                    InputProps={{ readOnly: true }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    label="CreatedBy"
                    value={user ? user.createdBy.name : ""}
                    InputProps={{ readOnly: true }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Typography
                    variant="h3"
                    component="div"
                    color="text.secondary"
                  >
                    Contact
                  </Typography>
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    label="Phone"
                    value={user ? user.phone : ""}
                    InputProps={{ readOnly: true }}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    fullWidth
                    label="Email"
                    value={user ? user.email : ""}
                    InputProps={{ readOnly: true }}
                  />
                </Grid>
              </Grid>
            </DashboardCard>
          </Grid>
          <Grid item xs={12} md={6}>
            <DashboardCard>
              <Typography
                variant="h3"
                component="div"
                color="text.secondary"
                className="mb-2"
              >
                Order Summary
              </Typography>
              <hr className="one" />
              {user && user.orders && user.orders.length === 0 ? (
                <div className="text-center">
                  <NoData style={{ width: "75px", height: "75px" }} />
                  <h4 className="fw-bold">No Data!</h4>
                </div>
              ) : (
                user &&
                user.orders &&
                user.orders.map((order) => (
                  <div key={order.id}>
                    <div>Order ID: {order.id}</div>
                    <div>
                      Status:
                      <Chip
                        className="mx-2 my-2"
                        sx={{
                          px: "4px",
                          backgroundColor:
                            order.status === "Complete"
                              ? "complete.main"
                              : order.status === "Prepare"
                              ? "primary.dark"
                              : order.status === "Cooking"
                              ? "warning.main"
                              : "error.main",
                          color: "warning.contrastText",
                        }}
                        size="small"
                        label={order.status}
                      >
                        {order.status}
                      </Chip>
                    </div>
                    <div className="mb-2">
                      Payment Method: {order.paymentMethod}
                    </div>
                    <div>
                      Total Price:{" "}
                      <span className="text-danger">${order.totalPrice}</span>
                    </div>
                    <hr className="one" />
                  </div>
                ))
              )}
            </DashboardCard>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default UserDetail;
