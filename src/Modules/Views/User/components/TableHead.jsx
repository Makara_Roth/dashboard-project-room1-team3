import {
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
  Typography,
} from "@mui/material";
import React from "react";
import { useSelector } from "react-redux";
import useUser from "../core/Action";

const TableHeadUser = () => {
  const { params } = useSelector((state) => state.user);
  const {userPermission} = useSelector((state) => state.auth);
  const { handleSort } = useUser();
  const { sort, order } = params;
  return (
    <TableHead>
      <TableRow>
        <TableCell>
          <TableSortLabel
            active={sort === "id"}
            direction={order}
            onClick={() => handleSort("id")}
          >
            <Typography variant="subtitle2" fontWeight={600}>
              No
            </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
          <TableSortLabel
            active={sort === "name"}
            direction={order}
            onClick={() => handleSort("name")}
          >
            <Typography variant="subtitle2" fontWeight={600}>
              Name
            </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
          <TableSortLabel
            active={sort === "gender"}
            direction={order}
            onClick={() => handleSort("gender")}
          >
            <Typography variant="subtitle2" fontWeight={600}>
              Gender
            </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
        <TableSortLabel
            active={sort === "email"}
            direction={order}
            onClick={() => handleSort("email")}
          >
          <Typography variant="subtitle2" fontWeight={600}>
            Email
          </Typography>
          </TableSortLabel>
        </TableCell>

        <TableCell>
          <Typography variant="subtitle2" fontWeight={600}>
            Role
          </Typography>
        </TableCell>

        <TableCell>
          <Typography variant="subtitle2" fontWeight={600}>
            Created Date
          </Typography>
        </TableCell>
        <TableCell>
          <Typography variant="subtitle2" fontWeight={600}>
            Status
          </Typography>
        </TableCell>
        {
          ((userPermission?.find((per) => per.name === "edit-role"))?.status === 1 || (userPermission?.find((per) => per.name === "delete-role"))?.status === 1) && 
        <TableCell align="center">
          <Typography variant="subtitle2" fontWeight={600}>
            Action
          </Typography>
        </TableCell>
        }
      </TableRow>
    </TableHead>
  );
};

export default TableHeadUser;
