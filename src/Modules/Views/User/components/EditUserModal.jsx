import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import styles from "../../../../Globals/User/user.module.css";
import { Import } from "iconsax-react";
import useUser from "../core/Action";
import imageCompression from "browser-image-compression";
import {
  Box,
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  TextField,
  Typography,
} from "@mui/material";

const initialData = {
  name: "",
  username: "",
  gender: "",
  phone: "",
  avatar: "",
  role_id: "",
};

const EditUserModal = ({ handleClose, userData, onSubmit }) => {
  const [updatedUser, setUpdatedUser] = useState({
    ...initialData,
  });
  const [previewUrl, setPreviewUrl] = useState(null);
  const { editImage } = useUser();
  const { roles } = useSelector((state) => state.user);
  // const { image } = useSelector((state) => state.user);

  useEffect(() => {
    if (roles && userData) {
      setUpdatedUser((prevData) => ({
        ...prevData,
        ...userData,
        role_id: roles.find((r) => r.name === userData?.role?.name)?.id,
      }));
    }
  }, [userData, roles]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUpdatedUser({ ...updatedUser, [name]: value });
  };
  // console.log(userData?.role?.name?.id);
  const handleEdit = (e) => {
    e.preventDefault();
    onSubmit(updatedUser);

    setUpdatedUser({
      ...initialData,
    });
  };
  console.log(updatedUser)
  const handleFileInputChange = async (event) => {
    const file = event.target.files[0];
    const convertedFile = await convertImages(file);
    const fileReader = new FileReader();
    fileReader.onload = () => setPreviewUrl(fileReader.result);
    if (event.target.files[0].name) {
      fileReader.readAsDataURL(convertedFile);
    }
    const formData = new FormData();
    formData.append("file", convertedFile);

    if (!convertedFile) {
      toast.error(" Please select a file again ", {
        position: "top-center",
      });
    } else {
      try {
        toast.success("Profile has been updated");
        await editImage(updatedUser.id, formData);
      } catch (e) {
        console.log(e.message);
      }
    }
  };

  const convertImages = async (imageFile) => {
    if (imageFile.type === "image/jpeg") {
      return imageFile; // No conversion needed
    }

    const options = {
      maxSizeMB: 1,
      maxWidthOrHeight: 1920,
      useWebWorker: true,
    };

    try {
      const compressedFile = await imageCompression(imageFile, options);
      return new File([compressedFile], imageFile.name, { type: "image/jpeg" });
    } catch (error) {
      toast.error("Image conversion failed. Please try a different file.");
      console.error("Error compressing or converting file:", error);
      throw error;
    }
  };

  return (
    <Modal open={true} onClose={handleClose}>
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: 300,
          maxWidth: 400,
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          borderRadius: "5px",
          // Responsive styles
          "@media (max-width: 600px)": {
            // xs and sm screens
            width: "90%", // Adjusted width
            maxWidth: "none", // No maximum width
          },
          "@media (min-width: 600px) and (max-width: 960px)": {
            // md screens
            width: "80%", // Adjusted width
            maxWidth: 500, // Maximum width
          },
          "@media (min-width: 960px)": {
            // lg screens
            width: 400, // Fixed width
            maxWidth: "none", // No maximum width
          },
        }}
      >
        <Typography
          id="modal-modal-title"
          variant="h4"
          component="h2"
          style={{ textAlign: "left" }}
        >
          Edit User
        </Typography>
        <div className="d-flex justify-content-center align-content-center mb-3">
          <label
            htmlFor="user-profile"
            className={styles["user-profile-label"]}
          >
            {previewUrl ? (
              <img src={previewUrl} alt="Preview" />
            ) : (
              <img
                className={""}
                src={updatedUser.avatar}
                alt="Profile Avatar "
              />
            )}
            <div className={`${styles["profile-icon-box"]} bg-primary`}>
              <Import size="20" color="white" />
            </div>
            <input
              type="file"
              className="d-none"
              id="user-profile"
              onChange={(e) => handleFileInputChange(e)}
              // onChange={(e) => setvalues({...values, avatar: e.target.value})}
            />
          </label>
        </div>

        <Grid container spacing={2}>
          <Grid item xs={6}>
            <TextField
              className="mb-4"
              id="outlined-basic"
              label="Name"
              variant="outlined"
              value={updatedUser.name || ""}
              onChange={handleChange}
              name="name"
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              id="outlined-basic"
              label="UserName"
              variant="outlined"
              value={updatedUser.username || ""}
              onChange={handleChange}
              name="username"
            />
          </Grid>
          <Grid item xs={6}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Gender</InputLabel>
              <Select
                className="mb-4"
                label="Gender"
                onChange={handleChange}
                value={updatedUser.gender}
                name="gender"
              >
                <MenuItem value="Male">Male</MenuItem>
                <MenuItem value="Female">Female</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={6}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Role</InputLabel>
              <Select
                className="mb-4"
                label="Role"
                onChange={handleChange}
                name="role_id"
                defaultValue={""}
              >
                {roles?.map((role) => (
                  <MenuItem key={role.id} value={role?.id}>
                    {role?.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              id="outlined-basic"
              label="Phone"
              variant="outlined"
              value={updatedUser.phone || ""}
              onChange={handleChange}
              name="phone"
            />
          </Grid>
        </Grid>
        <div className="mt-4 text-end">
          <Button
            style={{
              backgroundColor: "#f1f1f1",
              color: "",
              marginRight: 3,
            }}
            onClick={handleClose}
          >
            Cancel
          </Button>
          <Button
            style={{
              backgroundColor: "#4085ad",
              color: "#ffff",
            }}
            onClick={handleEdit}
          >
            Save
          </Button>
        </div>
      </Box>
    </Modal>
  );
};

export default EditUserModal;
