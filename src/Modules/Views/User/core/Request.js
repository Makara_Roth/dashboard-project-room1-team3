import axios from "axios";
import { getAuth } from "../../../Auth/helper/authHelper";

const reqFetchUser = async (params) => {
  return await axios.get(`/api/user`, {
    params: params,
  });
};

const reqFetchUserById = async (id) => {
  return await axios.get(`/api/user/${id}`)
}

const reqEditUserById = (updatedUserData) => {
  return axios.put(`/api/user/${updatedUserData.id}`, updatedUserData);
}

const reqDeleteUserById = (id) => {
  return axios.delete(`/api/user/${id}`)
}

const reqCreateUser = ({ ...newUser }) => {
  return axios.post(
    "/api/user",
    {
      ...newUser,
    },
  );
};

const reqFetchRole = () => {
  return axios.get("/api/roles");
};

const reqEditUserAvatar = (id, avatar) => {
  return axios.post(`/api/user/${id}/profile-avatar`, avatar, {
    headers: {
      Authorization: `Bearer ${getAuth()}`,
      "Content-Type": "multipart/form-data",
    },
  });
};

export {
  reqFetchUser,
  reqFetchUserById,
  reqFetchRole,
  reqEditUserById,
  reqDeleteUserById,
  reqCreateUser,
  reqEditUserAvatar,
};
