import {
  reqEditUserAvatar,
  reqCreateUser,
  reqFetchRole,
  reqFetchUser,
  reqEditUserById,
  reqDeleteUserById,
  reqFetchUserById,
} from "./Request";
import { useDispatch, useSelector } from "react-redux";
import {
  setPaging,
  setRole,
  setMessage,
  setUsers,
  setImage,
  setLoading,
  setParams,
  setUserDetail,
  setShowModal,
} from "./Reducer";
import { toast } from "react-toastify";

const useUser = () => {
  const dispatch = useDispatch();
  const { params } = useSelector((state) => state.user);

  const getCurrentUser = () => {
    dispatch(setLoading(true));
    reqFetchUser(params).then((res) => {
      dispatch(setUsers(res.data));
      dispatch(setPaging(res.data.paging));
      dispatch(setLoading(false));
    });
  };

  const getUserById = (id) => {
    dispatch(setLoading(true));
    reqFetchUserById(id).then((res) => {
      dispatch(setUserDetail(res.data.data));
      dispatch(setLoading(false))
    });
  };

  const editUserById = (updatedUserData) => {
    return reqEditUserById(updatedUserData)
      .then((res) => {
        toast.success(res.data.data);
      })
      .catch(() => {
        toast.error("Failed to update user");
      });
  };

  const deleteUserById = (id) => {
    if (id === 1) {
      toast.error("You can't delete this user");
      return;
    }
    return reqDeleteUserById(id)
      .then((res) => {
        toast.success("User deleted successfully");
      })
      .catch((error) => {
        toast.error("Failed to delete user");
      });
  };

  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  const addUser = async (payload) => {
    const payloadKeys = Object.keys(payload);
    const payloadValues = Object.values(payload);
    const isAllFilled =
      payloadKeys.length === 10 && payloadValues.every((value) => value !== "");
    const isEmailValid = payload.email && emailRegex.test(payload.email);

    if (isAllFilled && isEmailValid) {
      try {
        await reqCreateUser(payload);
        toast.success("User has been Created Successfully");
        getCurrentUser();
        dispatch(setShowModal(false));
      } catch (error) {
        toast.error(error.response.data.message);
      }
    } else {
      if (!isAllFilled) {
        toast.error("Please fill in all fields");
      } else if (!isEmailValid) {
        toast.error("Please provide a valid email address");
      }
    }
  };

  const getRole = () => {
    reqFetchRole().then((res) => {
      dispatch(setRole(res.data.data));
    });
  };

  const editImage = async (id, avatar) => {
    try {
      reqEditUserAvatar(id, avatar).then((res) => {
        dispatch(setImage(res.data.data));
        // console.log(res.data.data);
      });
    } catch (err) {
      console.log(err.message);
      dispatch(setMessage(err.message));
    }
  };

  const handleSearch = (text) => {
    dispatch(setParams({ page: 1, query: text }));
  };

  const handlePage = (page) => dispatch(setParams({ page: page }));

  const handleSort = (field) => {
    const newOrder = params.order === "asc" ? "desc" : "asc";
    dispatch(setParams({ sort: field, order: newOrder }));
  };

  const handleFilter = (name, value) => {
    dispatch(setParams({ [name]: value }));
  };

  return {
    getCurrentUser,
    getUserById,
    editUserById,
    deleteUserById,
    getRole,
    addUser,
    editImage,
    handleSearch,
    handlePage,
    handleSort,
    handleFilter,
  };
};
export default useUser;
