import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "user",
  initialState: {
    currentUser: [],
    totalPage: 0,
    user: null,
    userIdToDelete: null,
    paging: {},
    message: {},
    image: undefined,
    loading: true,
    params: {
      size: 12,
      page: 1,
      query: "",
      sort: "",
      order: "",
    },
    showModal: false,
  },
  reducers: {
    setUsers: (state, action) => {
      state.currentUser = action.payload.data;
      state.totalPage = action.payload.paging.totalPage;
    },
    setUserDetail: (state, action) => {
      state.user = action.payload;
    },
    // editUser: (state, action) => {
    //   const { userId, updatedUserData } = action.payload;
    //   state.currentUser = state.currentUser.map((user) =>
    //     user.id === userId ? { ...user, ...updatedUserData } : user
    //   );
    // },
    setUserIdToDelete: (state, action) => {
      state.userIdToDelete = action.payload;
    },
    deleteUser: (state, action) => {
      const deletedId = action.payload;
      state.currentUser = state.currentUser.filter(
        (user) => user.id !== deletedId
      );
    },
    setImage: (state, action) => {
      state.image = action.payload;
    },
    setRole: (state, action) => {
      state.roles = action.payload;
    },
    setPaging: (state, action) => {
      state.paging = action.payload;
    },
    setMessage: (state, action) => {
      state.message = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setParams: (state, action) => {
      state.params = { ...state.params, ...action.payload };
    },
    setShowModal: (state, action) => {
      state.showModal = action.payload;
    },
  },
});
export const {
  setUsers,
  setUserDetail,
  // editUser,
  deleteUser,
  setUserIdToDelete,
  setPage,
  setImage,
  setRole,
  setPaging,
  setMessage,
  setLoading,
  setParams,
  setShowModal,
} = userSlice.actions;
export default userSlice.reducer;
