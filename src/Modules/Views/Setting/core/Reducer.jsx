import { createSlice } from "@reduxjs/toolkit";

const settingsSlice = createSlice({
  name: "Settings",
  initialState: {
    oldPassword: "",
    newPassword: "",
    cfPassword: "",
    values:{
      oldPassword: "",
      newPassword: "",
      cfPassword: "",
    },
    showModelEditProfile: false,
    userProfile: {},
    image: undefined,
    message: {},
  },
  reducers: {
    setUser: (state, action) => {
      state.userProfile = action.payload;
    },
    setImage: (state, action) => {
      state.image = action.payload;
    },
    setOldPassword: (state, action) => {
      state.oldPassword = action.payload;
    },
    setMessage: (state, action) => {
      state.message = action.payload;
    },
    setNewPassword: (state, action) => {
      state.newPassword = action.payload;
    },
    setCfPassword: (state, action) => {
      state.cfPassword = action.payload;
    },
    setValues: (state, action) => {
      state.values = action.payload;
    },
    setShowModelEditProfile: (state, action) => {
      state.showModelEditProfile = action.payload;
    }
  },
});

export const { setOldPassword, setMessage, setNewPassword, setCfPassword , setValues  , setShowModelEditProfile} =
  settingsSlice.actions;
export default settingsSlice.reducer;
