import axios from "axios";
import {getAuth} from "../../../Auth/helper/authHelper";

const reqUpdateProfile = ({ name, gender, phone, avatar }) => {
  return axios.put(
      "/api/user/profile",
      {
        name: name,
        gender: gender,
        phone: phone,
        avatar: avatar,
      }
  );
};
const reqUpdateProfileAvatarByToken  = (avatar) => {
  return axios.post("/api/user/profile-avatar/token", avatar ,{
        headers: {
          Authorization: `Bearer ${getAuth()}`,
          'Content-Type': 'multipart/form-data'
        },
      }
  )
}
const reqChangePassword = (id, password, cfpassword, username) => {
  return axios.patch(`/api/user/${id}/password?username=${username}`, {
    password: password,
    confirm_password: cfpassword,
  });
};
  const reqResetPassword = ( { oldPassword ,  newPassword, cfPassword } , username) => {
  return axios.patch(`/api/user/password/reset?username=${username}`, {
    old_password: oldPassword,
    password: newPassword,
    confirm_password: cfPassword,
  });
};

export { reqUpdateProfile, reqUpdateProfileAvatarByToken , reqChangePassword  , reqResetPassword};
