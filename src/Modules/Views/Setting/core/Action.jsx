import {reqUpdateProfile, reqUpdateProfileAvatarByToken , reqChangePassword  , reqResetPassword} from ".//Request";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import {setImage} from "../../User/core/Reducer";
import {setMessage} from "./Reducer";

const useSettings = () => {
  const dispatch = useDispatch();

  // change password field
  const change_Password = async (id, password, cfPassword) => {
    try {
     const res =  await reqChangePassword(id, password, cfPassword)
     dispatch(setMessage({success: res.data.message}))
     toast.success(`Password : Successfully changed ${res.data.message}`);

     console.log(res.data.message);
    }catch (error) {
      dispatch(setMessage({error: error.response.data.message}))
      toast.error(`Password : ${error.response.data.message}`);
      // dispatch(setMessage({}));
      // console.log(error.response.data.data);
    }
  };

  // reset password field
  // username
  const reset_Password = async ({...value} , username) => {
    try {
       const res =  await reqResetPassword({...value} , username)
      toast.success(`Password : Successfully changed ${res.data.message}`);
    }catch (e) {
      dispatch(setMessage({error: e.response.data.message}))
      toast.error(`Password : ${e.response.data.message}`);
    }
  }
    const getUpdateProfile = async ({ name, gender, phone, avatar }) => {
        try {
            await reqUpdateProfile({name, gender, phone, avatar}).then((response)=>{
                console.log(response.data.data.message);
                dispatch(setMessage(response.data.data.message));
            })
        }catch (err) {

            dispatch(setMessage(err.message));

        }
    };
    const updateImage = async (avatar) =>{
        try{
          reqUpdateProfileAvatarByToken(avatar).then((res)=>{
                dispatch(setImage(res.data.data))
                // console.log(res.data.data);
            })
        }catch (err) {
            console.log(err.message)
            dispatch(setMessage(err.message));
        }
    }
  return { change_Password , reset_Password , getUpdateProfile , updateImage};
};
export default useSettings;
