import React from 'react';
import {Navigate, Route, Routes} from "react-router-dom";
import Setting from "./component/Setting";
import ChangePassword from "./component/Change_Password";
import ProfileDetails from "./component/ProfileDetial";
import ResetPassword from "./component/Reset-Password";

function SettingPages() {
    return (
        <>
            <Routes>
                <Route element={<Setting/>}>
                <Route index element={<Navigate to="details"/>}/>
                    <Route path='details' element={<ProfileDetails/>}/>
                    <Route path={"change-password"} element={<ChangePassword/>}/>
                    <Route path={"reset-password"} element={<ResetPassword/>}/>
                </Route>
            </Routes>
        </>
    );
}

export default SettingPages;
