import React, { useEffect, useState } from 'react';
import { Icon } from "react-icons-kit";
import { eyeOff } from "react-icons-kit/feather/eyeOff";
import { eye } from "react-icons-kit/feather/eye";
import { setMessage, setValues } from '../core/Reducer';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Container, Box, Card, CardHeader, CardContent, Typography, TextField, IconButton } from '@mui/material';
import useSettings from '../core/Action';
import { toast } from "react-toastify";

function ResetPassword() {
    const dispatch = useDispatch();
    const { message, values } = useSelector((state) => state.settings);
    const { userProfile } = useSelector(state => state.profile);
    const { reset_Password } = useSettings();
    const [type, setType] = useState("password");
    const [icon, setIcon] = useState(eyeOff);
    const [errors, setErrors] = useState({});

    useEffect(() => {
        dispatch(setMessage({}));
        dispatch(setValues({
            oldPassword: "",
            newPassword: "",
            cfPassword: "",
        }));
        // eslint-disable-next-line
    }, []);

    const validate = () => {
        let tempErrors = {};
        if (!values.oldPassword) tempErrors.oldPassword = "Old password is required";
        if (!values.newPassword) tempErrors.newPassword = "New password is required";
        if (!values.cfPassword) tempErrors.cfPassword = "Confirm password is required";
        if (values.newPassword !== values.cfPassword) tempErrors.cfPassword = "Passwords do not match";
        setErrors(tempErrors);
        return Object.keys(tempErrors).length === 0;
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (validate()) {
            await reset_Password({ ...values }, userProfile.username);
            toast.success(`Password successfully changed`);
            dispatch(setMessage({}));
            dispatch(setValues({
                oldPassword: "",
                newPassword: "",
                cfPassword: "",
            }));
        }
    };

    const handleToggle = () => {
        if (type === "password") {
            setIcon(eye);
            setType("text");
        } else {
            setIcon(eyeOff);
            setType("password");
        }
    };

    return (
        <Container maxWidth="sm" sx={{ mt: 5 }}>
            <form onSubmit={handleSubmit}>
                <Card>
                    <CardHeader title="Reset Password" />
                    <CardContent>
                        <Box mb={2}>
                            <Typography>Old Password</Typography>
                            <TextField
                                type="password"
                                name="oldPassword"
                                placeholder="Old Password"
                                value={values.oldPassword}
                                onChange={(e) => dispatch(setValues({ ...values, oldPassword: e.target.value }))}
                                fullWidth
                                required
                                error={!!errors.oldPassword}
                                helperText={errors.oldPassword}
                            />
                        </Box>
                        <Box mb={2} sx={{ position: 'relative' }}>
                            <Typography>New Password</Typography>
                            <TextField
                                type={type}
                                name="newPassword"
                                placeholder="New Password"
                                value={values.newPassword}
                                onChange={(e) => dispatch(setValues({ ...values, newPassword: e.target.value }))}
                                fullWidth
                                required
                                error={!!errors.newPassword}
                                helperText={errors.newPassword}
                            />
                            <IconButton
                                onClick={handleToggle}
                                sx={{ position: 'absolute', top: '25%', right: '5%' }}
                            >
                                <Icon icon={icon} size={20} />
                            </IconButton>
                        </Box>
                        <Box mb={2} sx={{ position: 'relative' }}>
                            <Typography>Confirm Password</Typography>
                            <TextField
                                type={type}
                                name="cfPassword"
                                placeholder="Confirm Password"
                                value={values.cfPassword}
                                onChange={(e) => dispatch(setValues({ ...values, cfPassword: e.target.value }))}
                                fullWidth
                                required
                                error={!!errors.cfPassword}
                                helperText={errors.cfPassword}
                            />
                            <IconButton
                                onClick={handleToggle}
                                sx={{ position: 'absolute', top: '25%', right: '5%' }}
                            >
                                <Icon icon={icon} size={20} />
                            </IconButton>
                        </Box>
                    </CardContent>
                    <Typography color="error" textAlign="center">
                        {message.error}
                    </Typography>
                    <Box display="flex" justifyContent="center" my={2}>
                        <Button type="submit" variant="contained" color="primary" sx={{ width: '50%' }}>
                            Submit
                        </Button>
                    </Box>
                </Card>
            </form>
        </Container>
    );
}

export default ResetPassword;
