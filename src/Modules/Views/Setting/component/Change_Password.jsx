import React, { useState } from "react";
import {
  Container,
  Box,
  Card,
  CardHeader,
  CardContent,
  Typography,
  TextField,
  IconButton,
  Button
} from "@mui/material";
import { Icon } from "react-icons-kit";
import { eyeOff } from "react-icons-kit/feather/eyeOff";
import { eye } from "react-icons-kit/feather/eye";
import { useDispatch, useSelector } from "react-redux";
import { setNewPassword, setCfPassword, setMessage } from "../core/Reducer";
import useSettings from "../core/Action";
import { useEffect } from "react";



function ChangePassword() {
  const { message, newPassword, cfPassword } = useSelector((state) => state.settings);
  // const { user } = useSelector(state => state.login)
  const { userProfile } = useSelector(state => state.profile)
  const { change_Password } = useSettings();
  const [type, setType] = useState("password");
  const [icon, setIcon] = useState(eyeOff);
  const dispatch = useDispatch();

  useEffect(() => {

    //clear error message when compnent fist render

    dispatch(setNewPassword(""));
    dispatch(setCfPassword(""));
    dispatch(setMessage({}));

// eslint-disable-next-line
  }, []);

  // push data to api 
  const handleSubmit = (e) => {
    e.preventDefault();
    const id = userProfile.id;
     if (cfPassword !== newPassword){
      dispatch(setMessage({ error: 'Passwords do not match'}));
    } else {
      change_Password(id, newPassword, cfPassword)
      //clear error message
      dispatch(setNewPassword(""));
      dispatch(setCfPassword(""));
      dispatch(setMessage({}));
    }
  };

  
  const handleToggle = () => {
    if (type === "password") {
      setIcon(eye);
      setType("text");
    } else {
      setIcon(eyeOff);
      setType("password");
    }
  };

  return (
    <>
      <Container maxWidth="sm" sx={{ mt: 5 }}>
      <form onSubmit={handleSubmit}>
        <Card>
          <CardHeader title="Change Password" />
          <CardContent>
            <Box mb={2}>
              <Typography>Password</Typography>
              <Box sx={{ position: 'relative' }}>
                <TextField
                  type={type}
                  name="password"
                  placeholder="Password"
                  value={newPassword}
                  onChange={(e) => dispatch(setNewPassword(e.target.value))}
                  fullWidth
                  required
                />
                <IconButton
                  onClick={handleToggle}
                  sx={{ position: 'absolute', top: '5%', right: '5%' }}
                >
                  <Icon icon={icon} size={20} />
                </IconButton>
              </Box>
            </Box>
            <Box mb={2}>
              <Typography>Confirm Password</Typography>
              <Box sx={{ position: 'relative' }}>
                <TextField
                  type={type}
                  name="ConfirmPassword"
                  placeholder="Confirm Password"
                  value={cfPassword}
                  onChange={(e) => dispatch(setCfPassword(e.target.value))}
                  fullWidth
                  required
                />
                <IconButton
                  onClick={handleToggle}
                  sx={{ position: 'absolute', top: '5%', right: '5%' }}
                >
                  <Icon icon={icon} size={20} />
                </IconButton>
              </Box>
            </Box>
            <Typography color="error" textAlign="center">
              {message.error ? message.error : message.success}
            </Typography>
            <Box mt={4} display="flex" justifyContent="center">
              <Button type="submit" variant="contained" color="primary" sx={{ width: '50%' }}>
                Submit
              </Button>
            </Box>
          </CardContent>
        </Card>
      </form>
    </Container>
    </>
  );
}

export default ChangePassword;
