import React from 'react';
import {Outlet} from "react-router-dom";
import NavSetting from "./NavSetting";

function Setting() {
    return (
        <>
          <NavSetting/>
          <Outlet/>
        </>
    );
}

export default Setting;