import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import { Modal, Box, Container, Grid, TextField, Button, CircularProgress , Typography } from "@mui/material";
import styles from "../../../../Globals/Profile/Profile.module.css";
import { Import } from "iconsax-react";
import { toast } from "react-toastify";
import imageCompression from 'browser-image-compression';
import useSettings from "../core/Action";

const initialData = {
    name: "",
    gender: "",
    phone: "",
    avatar: "",
};

function EditProfile({ handleClose }) {
    const { userProfile } = useSelector((state) => state.profile);
    const { image } = useSelector((state) => state.settings);
    const { getUpdateProfile, updateImage } = useSettings();
    const [values, setValues] = useState({ ...initialData });
    const [previewUrl, setPreviewUrl] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (userProfile) {
            setValues((prevData) => ({
                ...prevData,
                ...userProfile,
            }));
        }
    }, [userProfile]);

    const handleSave = async (e) => {
        e.preventDefault();
        setLoading(true);
        setValues({ ...values, avatar: image });
        let isConfirm = window.confirm("Are you sure you want to Edit?");
        if (isConfirm === true) {
            try {
                await getUpdateProfile({ ...values });
                toast.success(" Update successfully ", {
                    position: "top-center",
                });
            } catch (error) {
                toast.error(" Update Failed ");
            } finally {
                setLoading(false);
            }
        }
    };

    const handleFileInputChange = async (event) => {
        const file = event.target.files[0];
        if (!file) return;

        const convertedFile = await convertImages(file);
        const fileReader = new FileReader();
        fileReader.onload = () => setPreviewUrl(fileReader.result);
        fileReader.readAsDataURL(convertedFile);

        const formData = new FormData();
        formData.append("file", convertedFile);

        try {
            await updateImage(formData);
        } catch (error) {
            console.log(error.message);
        }
    };

    const convertImages = async (imageFile) => {
        if (imageFile.type === 'image/jpeg') {
            return imageFile; // No conversion needed
        }

        const options = {
            maxSizeMB: 1,
            maxWidthOrHeight: 1920,
            useWebWorker: true,
        };

        try {
            const compressedFile = await imageCompression(imageFile, options);
            return new File([compressedFile], imageFile.name, { type: 'image/jpeg' });
        } catch (error) {
            toast.error("Image conversion failed. Please try a different file.");
            console.error('Error compressing or converting file:', error);
            throw error;
        }
    };

    return (
        <>
            <Modal open={true} onClose={handleClose}>
                <Box sx={{ 
                    position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate(-50%, -50%)',
                        width: '90%', // Adjusted width for smaller screens
                        maxWidth: 600, // Maximum width for larger screens
                        bgcolor: 'background.paper',
                        boxShadow: 24,
                        p: 4,}}>
                    <Typography variant="h4" component="h2" style={{ textAlign: "left" }}>
                        Edit UserProfile 
                    </Typography>
                    <Container maxWidth="sm">
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Box className={`${styles["user-profile"]} img-account-profile h-100 d-flex flex-column align-items-center border bg-white`}>
                                    <label htmlFor="user-profile" className={styles["user-profile-label"]}>
                                        {previewUrl ? <img src={previewUrl} alt="Preview" /> : <img className={""} src={values.avatar} alt="Profile Avatar " />}
                                        <Box className={`${styles["profile-icon-box"]} bg-primary`}>
                                            <Import size="20" color="white" />
                                        </Box>
                                        <input type="file" className="d-none" id="user-profile" onChange={handleFileInputChange} />
                                    </label>
                                </Box>
                            </Grid>
                            <Grid item xs={12} md={8}>
                                <TextField fullWidth label="Name" value={values.name} onChange={(e) => setValues({ ...values, name: e.target.value })} autoFocus />
                            </Grid>
                            <Grid item xs={12} md={4}>
                                <TextField fullWidth label="Gender" value={values.gender} onChange={(e) => setValues({ ...values, gender: e.target.value })} />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField fullWidth label="Tel" value={values.phone} onChange={(e) => setValues({ ...values, phone: e.target.value })} />
                            </Grid>
                        </Grid>
                        <Box mt={2} display="flex" justifyContent="space-between">
                            <Button variant="contained" onClick={ handleClose }>Close</Button>
                            <Button variant="contained" color="primary" onClick={handleSave} disabled={loading}  >
                                {loading ? <CircularProgress size={24} /> : "Save Changes"}
                            </Button>
                        </Box>
                    </Container>
                </Box>
            </Modal>
        </>
    );
}

export default EditProfile;
