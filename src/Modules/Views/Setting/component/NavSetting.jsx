import React, { useState } from "react";
import { Container, Grid, Breadcrumbs, Typography, Button, Box, Link } from "@mui/material";
import { NavLink } from "react-router-dom";
import { uniqueId } from "lodash";
import { useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import HomeIcon from '@mui/icons-material/Home';
import VpnKeyIcon from '@mui/icons-material/VpnKey';
import LockResetIcon from '@mui/icons-material/LockReset';

function NavSetting() {
    const [activeIndex, setActiveIndex] = useState(-1);

    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

    const navItems = [
        {
            id: uniqueId(),
            title: "Profile Details",
            to: "details",
            icon: <HomeIcon />
        },
        {
            id: uniqueId(),
            title: "Change Password",
            to: "change-password",
            icon: <VpnKeyIcon />
        },
        {
            id: uniqueId(),
            title: "Reset Password",
            to: "reset-password",
            icon: <LockResetIcon />
        },
    ];

    const handleNavLinkClick = (index) => {
        setActiveIndex(index);
    };
    return (
        <>
            <Container>
                <Grid container>
                    <Grid item xs={12}>
                        <Breadcrumbs aria-label="breadcrumb">
                            <Link underline="hover" color="inherit" href="/">
                                Dashboard
                            </Link>
                            <Typography color="text.primary">Settings</Typography>
                        </Breadcrumbs>
                    </Grid>
                    <Grid item xs={12} className={"w-100 mt-3"}>
                        <Box display="flex" flexWrap="wrap">
                            {navItems.map((item, index) => (
                                <Button
                                    key={item.id}
                                    component={NavLink}
                                    to={item.to}
                                    className={`me-2 mb-3 mt-3 ${activeIndex === index ? 'active' : ''}`}
                                    onClick={() => handleNavLinkClick(index)}
                                    variant="contained"
                                    color={activeIndex === index ? "secondary" : "primary"}
                                    startIcon={isMobile ? null : item.icon}
                                    sx={{ minWidth: isMobile ? 'auto' : '100px' }}
                                >
                                    {isMobile ? item.icon : item.title}
                                </Button>
                            ))}
                        </Box>
                    </Grid>
                </Grid>
            </Container>
        </>
    );
}

export default NavSetting;
