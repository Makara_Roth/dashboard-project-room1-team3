
import "../../../../Globals/Profile/Profile.css";
import styles from "../../../../Globals/Profile/Profile.module.css";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { Spinner } from "react-bootstrap";
import { Status } from "iconsax-react";
import Breadcrumb from "../../../../Globals/helpers/components/Breadcrumb";
import useProfile from '../../ProfilePages/core/Action';

const ProfileDetails = () => {
  const {
    userProfile,
  } = useSelector((state) => state.profile);
  const { getProfile } = useProfile();
  const navigate = useNavigate();

  useEffect(() => {
    getProfile();
    // eslint-disable-next-line
  }, []);

  const editHandler = (e) => {
    e.preventDefault();
    navigate("/user/profile/edit");
  };

  if (!userProfile) {
    return (
      <Spinner
        className={"d-flex justify-content-center align-items-center"}
        animation="border"
        role="status"
      >
        <span className="visually-hidden">Loading...</span>
      </Spinner>
    );
  }

  return (
    <>
      <div className={"container mt-3"}>
      <Breadcrumb title={"Dasborad "} subtitle={"Profile"}/>
        <div className="row mt-2">
          <div className="col-xl-4">
            {/*Profile picture card*/}
            <div className="card mb-xl-0 mb-4">
              <div className="card-header">Profile Picture</div>
              <div className="card-body text-center">
                <div
                  className={`${styles["user-profile"]} img-account-profile d-flex flex-column align-items-center border bg-white`}
                >
                  <label
                    htmlFor="user-profile"
                    className={styles["user-profile-label"]}
                  >
                    <img src={userProfile.avatar} alt="" />
                    <div className={`${styles["profile-icon-box"]} bg-primary`}>
                      {/* <Import size="20" color="white" /> */}
                      { <Status
                        size="20"
                        color=""
                      /> }
                    </div>
                  </label>
                </div>
                {/*Profile picture help block */}
                {/*<div className="small font-italic text-muted mb-4">JPG or PNG no larger than 5 MB</div>*/}
                {/*  Profile picture upload button */}
                {/*<button className="btn btn-primary" type="button"> Upload new image</button>*/}
              </div>
            </div>
          </div>
          <div className="col-xl-8">
            {/*Account details card*/}
            <div className="card mb-4">
              <div className="card-header">Account Details</div>
              <div className="card-body">
                <form>
                  {/* Form Group (username) */}
                  <div className="mb-3">
                    <label className="small mb-1" htmlFor="inputUsername">
                      Username (how your name will appear to other users on the
                      site)
                    </label>
                    <input
                      className="form-control"
                      id="inputUsername"
                      type="text"
                      // placeholder="Enter your username"
                      defaultValue={userProfile.username}
                      readOnly={true}
                    />
                  </div>
                  <div className="row gx-3 mb-3">
                    <div className="col-md-6">
                      <label className="small mb-1" htmlFor="inputFirstName">
                        Full name
                      </label>
                      <input
                        className="form-control"
                        id="inputFirstName"
                        type="text"
                        // placeholder="Enter your first name"
                         value={userProfile.name}
                        readOnly={true}
                      />
                    </div>
                    <div className="col-md-6">
                      <label className="small mb-1" htmlFor="inputLastName">
                        {" "}
                        Gender :{" "}
                      </label>
                      <input
                        className="form-control"
                        id="inputLastName"
                        type="text"
                        // placeholder="Enter your last name"
                        defaultValue={userProfile.gender}
                        readOnly={true}
                      />
                    </div>
                  </div>
                  <div className="row gx-3 mb-3">
                    <div className="col-md-6">
                      <label className="small mb-1" htmlFor="inputOrgName">
                        Salary
                      </label>
                      <input
                        className="form-control"
                        id="inputOrgName"
                        type="text"
                        // placeholder="Enter your organization name"
                        defaultValue={`${userProfile.salary}$`}
                        readOnly={true}
                      />
                    </div>
                    <div className="col-md-6">
                      <label className="small mb-1" htmlFor="inputLocation">
                        Hire Date
                      </label>
                      <input
                        className="form-control"
                        id="inputLocation"
                        type="text"
                        // placeholder="Enter your location"
                        defaultValue={userProfile.hireDate}
                        readOnly={true}
                      />
                    </div>
                  </div>
                  <div className="mb-3">
                    <label className="small mb-1" htmlFor="inputEmailAddress">
                      Email address
                    </label>
                    <input
                      className="form-control"
                      id="inputEmailAddress"
                      type="email"
                      // placeholder="Enter your email address"
                      defaultValue={userProfile.email}
                      readOnly={true}
                    />
                  </div>
                  <div className="row gx-3 mb-3">
                    <div className="col-md-6">
                      <label className="small mb-1" htmlFor="inputPhone">
                        Phone number
                      </label>
                      <input
                        className="form-control"
                        id="inputPhone"
                        type="tel"
                        placeholder="Enter your phone number"
                        value={userProfile.phone}
                        readOnly={true}
                      />
                    </div>
                  </div>
                  <button
                    className="btn btn-primary"
                    type="button"
                    onClick={editHandler}
                  >
                    Edit Information
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ProfileDetails;
