import "../../../../Globals/Profile/Profile.css";
import { Container, Card, CardHeader, CardContent, Grid, TextField, Typography, Button, CircularProgress, Box } from '@mui/material'; // Import necessary MUI components

import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import useProfile from '../../ProfilePages/core/Action';
import { setShowModelEditProfile } from "../core/Reducer";
import EditProfile from "./EditProfile";
import ProfileCard from '../../ProfilePages/components/ProfileCard';

const ProfileDetails = () => {
  const { userProfile , addresses } = useSelector((state) => state.profile);
  const { showModelEditProfile } = useSelector((state) => state.settings);
  const { getProfile } = useProfile();
  const dispatch = useDispatch();

  useEffect(() => {
    getProfile();
    // eslint-disable-next-line
  }, []);

  const editHandler = () => {
    dispatch(setShowModelEditProfile(true));
  };

  if (!userProfile) {
    return (
      <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
        <CircularProgress />
      </Box>
    );
  }

  return (
    <Box>
      <Container mt={3}>
        <Grid container mt={2} spacing={2}>
          <Grid item xs={12} md={4}>
            <ProfileCard  userProfile={userProfile} address={addresses} />
          </Grid>
          <Grid item xs={12} md={8}>
            <Card>
              <CardHeader title="Account Details" />
              <CardContent>
                <form>
                  <TextField
                    fullWidth
                    label="Username"
                    value={userProfile.username || ''}
                    InputProps={{ readOnly: true }}
                    margin="normal"
                  />
                  <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                      <TextField
                        fullWidth
                        label="Full Name"
                        value={userProfile.name || ''}
                        InputProps={{ readOnly: true }}
                        margin="normal"
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        fullWidth
                        label="Gender"
                        value={userProfile.gender || ''}
                        InputProps={{ readOnly: true }}
                        margin="normal"
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                      <TextField
                        fullWidth
                        label="Salary"
                        value={`${userProfile.salary || 0}$`}
                        InputProps={{ readOnly: true }}
                        margin="normal"
                      />
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <TextField
                        fullWidth
                        label="Hire Date"
                        value={userProfile.hireDate || ''}
                        InputProps={{ readOnly: true }}
                        margin="normal"
                      />
                    </Grid>
                  </Grid>
                  <TextField
                    fullWidth
                    label="Email Address"
                    type="email"
                    value={userProfile.email || ''}
                    InputProps={{ readOnly: true }}
                    margin="normal"
                  />
                  <TextField
                    fullWidth
                    label="Phone Number"
                    type="tel"
                    value={userProfile.phone || ''}
                    InputProps={{ readOnly: true }}
                    margin="normal"
                  />
                  <Box display="flex" justifyContent="flex-end" mt={2}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={editHandler}
                    >
                      Edit Information
                    </Button>
                  </Box>

                  {/* EditProfile Modal */}
                  {showModelEditProfile && (
                    <EditProfile
                      handleClose={() => dispatch(setShowModelEditProfile(false))}
                      // userData={userData}
                    />
                  )}
                </form>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default ProfileDetails;
