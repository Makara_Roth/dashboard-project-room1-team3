import DashboardOutlinedIcon from '@mui/icons-material/DashboardOutlined';
import TableRestaurantOutlinedIcon from '@mui/icons-material/TableRestaurantOutlined';
import ShoppingBagOutlinedIcon from '@mui/icons-material/ShoppingBagOutlined';
import PersonOutlinedIcon from '@mui/icons-material/PersonOutlined';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import { MdOutlineFastfood } from "react-icons/md";
import AssessmentIcon from '@mui/icons-material/Assessment';
import RestaurantIcon from '@mui/icons-material/Restaurant';

import { uniqueId } from "lodash";

const Menuitems = [
  {
    navlabel: true,
    subheader: "Home",
  },
  {
    id: uniqueId(),
    title: "Dashboard",
    icon: DashboardOutlinedIcon,
    href: "/dashboard",
  },
  {
    id: uniqueId(),
    title: "Table",
    icon: TableRestaurantOutlinedIcon,
    href: "/table",
  },
  {
    id: uniqueId(),
    title: "Order",
    icon: ShoppingBagOutlinedIcon,
    href: "/order",
  },
  {
    navlabel: true,
    subheader: "Report",
  },
  {
    id: uniqueId(),
    title: "Food Report",
    icon: AssessmentIcon,
    href: "/foodreport",
  },
  {
    navlabel: true,
    subheader: "Foods",
  },
  {
    id: uniqueId(),
    title: "Food",
    icon: MdOutlineFastfood,
    href: "/food",
  },
  {
    id: uniqueId(),
    title: "Food Category",
    icon: RestaurantIcon,
    href: "/foodCategorise",
  },
  {
    navlabel: true,
    subheader: "Administration",
    permission: 'list-user' || 'list-role'
  },
  {
    id: uniqueId(),
    title: "User Management",
    icon: PersonOutlinedIcon,
    href: "/user",
    permission: 'list-user'
  },
  {
    id: uniqueId(),
    title: "Role",
    icon: SettingsOutlinedIcon,
    href: "/role",
    permission: 'list-role'
  },

];

export default Menuitems;
