import React from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { Box, List } from '@mui/material';
import Menuitems from './MenuItems';
import NavGroup from './NavGroup/NavGroup';
import NavItem from './NavItem/Index';

const filterMenuItemsByPermission = (menuItems, user, userPermissions) => {
  return menuItems.filter(item => {
    if (item.permission) {
      return (
        user?.id === 1 || userPermissions?.some(per => per.name === item.permission && per.status === 1)
      );
    }
    return true;
  });
};

const SidebarItems = () => {
  const { pathname } = useLocation();
  const { user, userPermission } = useSelector(state => state.auth);

  const filteredMenuItems = filterMenuItemsByPermission(Menuitems, user, userPermission);

  return (
    <Box sx={{ px: 3 }}>
      <List sx={{ pt: 0 }} className="sidebarNav">
        {filteredMenuItems.map((item) => {
          // {/********SubHeader**********/}
          if (item.subheader) {
            return <NavGroup item={item} key={item.subheader} />;

            // {/********If Sub Menu**********/}
          } else {
            return <NavItem item={item} key={item.id} pathDirect={pathname} />;
          }
        })}
      </List>
    </Box>
  );
};

export default SidebarItems;
