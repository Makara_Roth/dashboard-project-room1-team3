import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Avatar,
  Box,
  Menu,
  Button,
  IconButton,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Badge,
} from "@mui/material";
import { IconListCheck, IconSettings, IconUser } from "@tabler/icons";
import useLogin from "../../../Auth/Login/Core/Action";
import { useSelector } from "react-redux";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({

  profileContainer: {
    backgroundColor: "#fff",
    position: "relative",
    padding: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
  statusIndicator: {
    position: "absolute",
    bottom: 0,
    right: 0,
  },
}));

const Profile = () => {
  const { user } = useSelector((state) => state.login);
  const { userProfile } = useSelector((state) => state.profile);

  const [anchorEl2, setAnchorEl2] = useState(null);
  const { logout } = useLogin();

  const classes = useStyles();

  const handleClick2 = (event) => {
    setAnchorEl2(event.currentTarget);
  };
  const handleClose2 = () => {
    setAnchorEl2(null);
  };

  return (
    <Box>
      <IconButton
        size="large"
        aria-label="show 11 new notifications"
        aria-controls="msgs-menu"
        onClick={handleClick2}
        sx={{
          color: typeof anchorEl2 === "object" && "primary.main",
          marginLeft: "40px",
          marginTop: "5px"
        }}
      >
        {(user || userProfile) && (
          <div
            sx={{
              backgroundColor: "#fff",
              position: "relative",
              padding: 2,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Badge
              overlap="circular"
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
              }}
              sx={{
                position: "absolute",
                bottom: 0,
                right: 0,
              }}
              badgeContent={
                <div
                  style={{
                    backgroundColor: userProfile?.status === true ? "green" : "red",
                    width: 10,
                    height: 10,
                    borderRadius: "50%",
                  }}
                />
              }
            >
              <Avatar
                src={
                  user?.avatar ??
                  userProfile?.avatar ??
                  "https://conflictresolutionmn.org/wp-content/uploads/2020/01/flat-business-man-user-profile-avatar-icon-vector-4333097-600x648.jpg"
                }
                alt="User Profile"
                sx={{ width: 35, height: 35, }} // Adjust the size as per your preference
              />
            </Badge>
          </div>
        )}
      </IconButton>
      {/* ------------------------------------------- */}
      {/* Message Dropdown */}
      {/* ------------------------------------------- */}
      <Menu
        id="msgs-menu"
        anchorEl={anchorEl2}
        keepMounted
        open={Boolean(anchorEl2)}
        onClose={handleClose2}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        sx={{
          "& .MuiMenu-paper": {
            width: "200px",
          },
        }}
      >
        <MenuItem>
          <ListItemIcon>
            <IconUser width={20} />
          </ListItemIcon>
          <ListItemText>
            <Link
              to={"/user/profile"}
              className={"text-decoration-none text-dark"}
            >
              My Profile{" "}
            </Link>
          </ListItemText>
        </MenuItem>
        <MenuItem>
          <ListItemIcon>
            <IconSettings width={20} />
          </ListItemIcon>
          <ListItemText>
            <Link
              to={"/user/settings"}
              className={"text-decoration-none text-dark"}
            >
              Setting
            </Link>
          </ListItemText>
        </MenuItem>
        <Box mt={1} py={1} px={2}>
          <Button
            // to="/auth/login"
            variant="outlined"
            color="primary"
            onClick={logout}
            // component={Link}
            fullWidth
          >
            Logout
          </Button>
        </Box>
      </Menu>
    </Box>
  );
};

export default Profile;
