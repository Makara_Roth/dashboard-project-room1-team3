import React, { memo } from 'react';
import kiloitLogo from '../../../assets/images/profile/kiloit-logo.svg';

const SplashScreen = () => {
  return (
    <div id="splash-screen" className="splash-screen">
      <img src={kiloitLogo} alt="KiloIT Logo" />
      <span>Loading ...</span>
    </div>
  );
};

export default memo(SplashScreen);
