import { Route, Routes } from "react-router"
import FullLayout from "../Layout/Full/FullLayout"
import routes from "./Router"

const PrivateRoutes = () => {

    return (
        <Routes>
            <Route element={<FullLayout />}>
                {
                    routes.map((route, i) => {
                        return <Route key={i} path={route.path} element={route.element} />
                    })
                }
            </Route>
        </Routes>
    )
}

export { PrivateRoutes }