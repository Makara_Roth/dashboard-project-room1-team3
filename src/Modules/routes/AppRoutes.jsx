import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import App from "../../App";
import { PrivateRoutes } from "./PrivateRoutes";
import Login from "../Auth/Login/Components/Login";
import { useSelector } from "react-redux";

const AppRoutes = () => {
  const { token } = useSelector((state) => state.auth);

  return (
    <BrowserRouter>
      <Routes>
        <Route element={<App />}>
          {token ? (
            <>
              <Route path="/*" element={<PrivateRoutes />} />
              <Route index element={<Navigate to="/dashboard" />} />
            </>
          ) : (
            <>
              {/*{Public Routh }*/}
              <Route path={"auth/login"} element={<Login />} />
              <Route path="*" element={<Navigate to="/auth/login" />}></Route>
            </>
          )}
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export {AppRoutes}
