import { Navigate } from "react-router";
import UserDetail from "../Views/User/components/UserDetail";
import { lazy } from "react";


const FoodReport = lazy(() => import("../Views/FoodReport/components/FoodReport"))
const Dashboard = lazy(() => import("../Views/Dashboard/Dashboard"))
const User = lazy(() => import("../Views/User/components/User"))
const Roles = lazy(() => import("../Views/Roles/components/Roles"))
const Food = lazy(() => import("../Views/food/components/Food"))
const FoodDetail = lazy(() => import("../Views/food/components/FoodDetail"))
const FoodsCategorise = lazy(() => import("../Views/foodCategory/components/FoodsCategorise"))
const TableBooking = lazy(() => import("../Views/Table/components/Table"))
const Order = lazy(() => import("../Views/Order/components/Order"))
const OrderDetail = lazy(() => import("../Views/Order/components/OrderDetail"))
const OrderFoodCart = lazy(() => import("../Views/Order/cart/OrderFoodCart"))
const SettingPages = lazy(() => import("../Views/Setting/SettingPages"))
const Profile = lazy(() => import("../Views/ProfilePages/components/Profile"))


const Router = [
  {
    path: "/",
    element: <Navigate to="/dashboard" />,
  },
  {
    path: "/dashboard",
    element: <Dashboard />,
  },
  {
    path: "/user",
    element: <User />,
  },
  {
    path: "/user/detail/:id",
    element: <UserDetail />
  },
  {
    path: "/user/profile",
    element: <Profile />
  },
  {
    path: "role",
    element: <Roles />
  },
  {
    path: "/user/settings/*",
    element: <SettingPages />
  },
  {
    path: "/food",
    element: <Food />
  },
  {
    path: "/food/detail/:id",
    element: <FoodDetail />
  },
  {
    path: "/foodCategorise",
    element: <FoodsCategorise />
  },
  {
    path: "/table",
    element: <TableBooking />
  },
  {
    path: "/order",
    element: <Order />
  },
  {
    path: "/order/orderdetail/:id",
    element: <OrderDetail />
  },
  {
    path: "/order/cart",
    element: <OrderFoodCart />
  },
  {
    path:"/foodreport",
    element: <FoodReport/>
  },
  {
    path: "*",
    element: <Navigate to="/" />
  },
];

export default Router;
